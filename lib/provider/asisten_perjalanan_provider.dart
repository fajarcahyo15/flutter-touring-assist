import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/model/grup_member.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_detail_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/res_detail_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/res_list_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_detail_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/grup_member_repo.dart';
import 'package:new_touring_assist_flutter/repository/grup_repo.dart';
import 'package:new_touring_assist_flutter/repository/grup_rute_repo.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';

class AsistenPerjalananProvider extends ChangeNotifier {
  GrupMemberRepo _grupMemberRepo = GrupMemberRepo();
  GrupRuteRepo _grupRuteRepo = GrupRuteRepo();
  GrupRepo _grupRepo = GrupRepo();

  bool _loadingScreen = false;
  String _toastMessage;
  GrupMember _grupMember = GrupMember();
  GrupRute _grupRute;
  Grup _grup;
  String _activeRouteOriDesString = "-";
  bool _haveActiveRoute = false;
  bool _anggotaTampil = false;

  bool get loadingScreen => _loadingScreen;
  String get toastMessage => _toastMessage;
  GrupMember get grupMember => _grupMember;
  GrupRute get grupRute => _grupRute;
  Grup get grup => _grup;
  String get activeRouteOriDesString => _activeRouteOriDesString;
  bool get haveActiveRoute => _haveActiveRoute;
  bool get anggotaTampil => _anggotaTampil;

  set loadingScreen(bool val) {
    _loadingScreen = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  set grupMember(GrupMember val) {
    _grupMember = val;
    notifyListeners();
  }

  set grupRute(GrupRute val) {
    _grupRute = val;
    notifyListeners();
  }

  set grup(Grup val) {
    _grup = val;
    notifyListeners();
  }

  set activeRouteOriDesString(String val) {
    _activeRouteOriDesString = val;
    notifyListeners();
  }

  set haveActiveRoute(bool val) {
    _haveActiveRoute = val;
    notifyListeners();
  }

  set anggotaTampil(bool val) {
    _anggotaTampil = val;
    notifyListeners();
  }

  void clearData() {
    loadingScreen = false;
    toastMessage = null;
    grupMember = GrupMember();
    grupRute = null;
    grup = null;
    activeRouteOriDesString = "-";
    haveActiveRoute = false;
    anggotaTampil = false;
  }

  Future<bool> getGrupMemberByIdGrup(String idGrup) async {
    bool out = false;
    var res = await _grupMemberRepo.getDetailByIdGrup(idGrup);

    if (res['data'] != null) {
      ResDetailGrupMember resDetailGrupMember = res['data'];
      grupMember = resDetailGrupMember.data.grupMember;
      out = true;
    } else {
      toastMessage = res['message'];
    }

    return out;
  }

  Future<bool> getGrupByIdGrup(String idGrup) async {
    bool out = false;
    var res = await _grupRepo.getById(idGrup);

    if (res['data'] != null) {
      ResDetailGrup resDetailGrup = res['data'];
      grup = resDetailGrup.data.grup;
      out = true;
    } else {
      toastMessage = res['message'];
    }

    return out;
  }

  Future<GrupRute> getFasePerjalananAktif(String idGrup) async {
    var res = await _grupRuteRepo.getAssistActive(idGrup);

    if (res['data'] != null) {
      ResDetailGrupRute resDetailGrupRute = res['data'];
      grupRute = resDetailGrupRute.data.grupRute;
      activeRouteOriDesString = "${resDetailGrupRute.data.grupRute.origin} -> ${resDetailGrupRute.data.grupRute.destination}";
      haveActiveRoute = true;
    } else {
      toastMessage = res['message'];
      activeRouteOriDesString = res['message'];
      haveActiveRoute = false;
    }

    return grupRute;
  }

  Future<ResListGrupMember> getMemberActiveAssist(String idGrup) async {
    ResListGrupMember resListGrupMember;
    loadingScreen = true;
    var res = await _grupMemberRepo.getAssistActive(idGrup);
    loadingScreen = false;

    if (res['data'] != null) {
      resListGrupMember = res['data'];
    } else {
      toastMessage = res['message'];
    }

    return resListGrupMember;
  }

  Future<bool> updateAssist(GrupRute grupRute) async {
    bool success = false;

    var res = await _grupMemberRepo.updateAssist(grupRute);

    if (res['data'] != null) {
      ResBase resBase = res['data'];
      success = resBase.status;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }

  Future<bool> startUpdateLocationService(String idGrup) async {
    bool success = false;
    Session session = Session();
    MethodChannel methodChannel = MethodChannel("org.presidentrocknroll.new_touring_assist_flutter.assist");

    var token = await session.getToken();
    try {
      String status = await methodChannel.invokeMethod("startService", {
        'token': token,
        'id_grup': idGrup,
        'id_grup_rute': grupRute.idGrupRute,
        'interval': grup.intervalLokasi,
      });
      toastMessage = status;
      success = true;
    } catch (e) {
      toastMessage = "Terjadi kesalahan sistem";
    }

    return success;
  }

  void stopUpdateLocationService() async {
    var methodChannel = MethodChannel("org.presidentrocknroll.new_touring_assist_flutter.assist");
    String status = await methodChannel.invokeMethod("stopService");
    toastMessage = status;
  }
}
