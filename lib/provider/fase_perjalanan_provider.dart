import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_list_grup_rute.dart';
import 'package:new_touring_assist_flutter/repository/grup_rute_repo.dart';

class FasePerjalananProvider extends ChangeNotifier {
  GrupRuteRepo _grupRuteRepo = GrupRuteRepo();

  bool _loadingPage = false;
  String _toastMessage = "";

  List<GrupRute> _listGrupRute = List();

  bool get loadingPage => _loadingPage;
  String get toastMessage => _toastMessage;
  List<GrupRute> get listGrupRute => _listGrupRute;

  set loadingPage(bool val) {
    _loadingPage = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  set listGrupRute(List<GrupRute> val) {
    _listGrupRute = val;
    notifyListeners();
  }

  void clearData() {
    loadingPage = false;
    toastMessage = "";
    listGrupRute = List();
  }

  Future<bool> getListGrupRute(String idGrup) async {
    bool have = true;
    var res = await _grupRuteRepo.getByIdGrup(idGrup);

    if (res['data'] != null) {
      ResListGrupRute resListGrupRute = res['data'];
      listGrupRute = resListGrupRute.data.listRute.toList();
    } else {
      toastMessage = res['message'];
      listGrupRute = List();
      have = false;
    }
    return have;
  }

  Future<bool> deleteGrupRute(String idGrupRute) async {
    bool success = false;
    var res = await _grupRuteRepo.delete(idGrupRute);

    if (res['data'] != null) {
      success = true;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }
}
