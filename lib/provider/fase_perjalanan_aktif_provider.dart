import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/data_list_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/res_list_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_list_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/grup_member_repo.dart';
import 'package:new_touring_assist_flutter/repository/grup_rute_repo.dart';

class FasePerjalananAktifProvider extends ChangeNotifier {
  GrupRuteRepo _grupRuteRepo = GrupRuteRepo();
  GrupMemberRepo _grupMemberRepo = GrupMemberRepo();

  bool _loadingScreen = false;
  String _toastMessage;
  List<GrupRute> _listGrupRute = List();
  List<DataListGrupMember> _listDataGrupMember = List();

  bool get loadingScreen => _loadingScreen;
  String get toastMessage => _toastMessage;
  List<GrupRute> get listGrupRute => _listGrupRute;
  List<DataListGrupMember> get listDataGrupMember => _listDataGrupMember;

  set loadingScreen(bool val) {
    _loadingScreen = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  set listGrupRute(List<GrupRute> val) {
    _listGrupRute = val;
    notifyListeners();
  }

  set listDataGrupMember(List<DataListGrupMember> val) {
    _listDataGrupMember = val;
    notifyListeners();
  }

  void clearData() {
    loadingScreen = false;
    toastMessage = null;
    listDataGrupMember = List();
  }

  Future<bool> getListGrupRute(String idGrup) async {
    bool out = true;
    var res = await _grupRuteRepo.getByIdGrup(idGrup);

    if (res['data'] != null) {
      ResListGrupRute resListGrupRute = res['data'];
      listGrupRute = resListGrupRute.data.listRute.toList();
    } else {
      toastMessage = res['message'];
      listGrupRute = List();
      out = false;
    }
    return out;
  }

  Future<bool> getListGrupMemberActive(String idGrup) async {
    bool out = true;
    var res = await _grupMemberRepo.getAssistActive(idGrup);

    if (res['data'] != null) {
      ResListGrupMember resListGrupMember = res['data'];
      listDataGrupMember = resListGrupMember.data.toList();
    } else {
      toastMessage = res['message'];
      listDataGrupMember = List();
      out = false;
    }

    return out;
  }

  Future<bool> updateAssist(GrupRute grupRute) async {
    bool success = false;

    var res = await _grupRuteRepo.updateAssist(grupRute);

    if (res['data'] != null) {
      ResBase resBase = res['data'];
      success = resBase.status;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }
}
