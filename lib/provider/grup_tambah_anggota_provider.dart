import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/response/anggota_grup/res_list_anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/anggota_repo.dart';
import 'package:new_touring_assist_flutter/repository/grup_member_repo.dart';

class GrupTambahAnggotaProvider extends ChangeNotifier {
  final AnggotaRepo _anggotaRepo = AnggotaRepo();
  final GrupMemberRepo _grupMemberRepo = GrupMemberRepo();

  String _toastMessage = "";
  bool _reqCari = false;
  bool _reqTambahAnggota = false;
  List<AnggotaGrup> _listAnggotaGrup = List();

  String get toastMessage => _toastMessage;
  bool get reqCari => _reqCari;
  bool get reqTambahAnggota => _reqTambahAnggota;
  List<AnggotaGrup> get listAnggotaGrup => _listAnggotaGrup;

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  set reqCari(bool val) {
    _reqCari = val;
    notifyListeners();
  }

  set reqTambahAnggota(bool val) {
    _reqCari = val;
    notifyListeners();
  }

  set listAnggotaGrup(List<AnggotaGrup> val) {
    _listAnggotaGrup = val;
    notifyListeners();
  }

  void clearData() {
    toastMessage = "";
    reqCari = false;
    reqTambahAnggota = false;
    listAnggotaGrup = List();
  }

  Future<bool> getCariAnggota(String idGrup, String cari) async {
    bool success = false;

    reqCari = true;
    var res = await _anggotaRepo.cariTambahAnggota(idGrup, cari);
    reqCari = false;

    if (res['data'] != null) {
      ResListAnggotaGrup resListAnggotaGrup = res['data'];
      listAnggotaGrup = resListAnggotaGrup.data.listAnggotaGrup.toList();
      if (listAnggotaGrup.length > 0) {
        success = true;
      } else {
        toastMessage = "Akun tidak ditemukan";
      }
    } else {
      toastMessage = res['message'];
    }

    return success;
  }

  Future<bool> tambahAnggota(String idGrup, String idAnggota) async {
    bool success = false;

    reqTambahAnggota = true;
    var res = await _grupMemberRepo.addMember(idGrup, idAnggota);
    reqTambahAnggota = false;

    if (res['data'] != null) {
      ResBase resBase = res['data'];
      toastMessage = resBase.message;
      success = true;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }
}
