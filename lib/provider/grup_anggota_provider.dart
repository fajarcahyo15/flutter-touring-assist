import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/response/anggota_grup/res_list_anggota_grup.dart';
import 'package:new_touring_assist_flutter/repository/grup_repo.dart';

class GrupAnggotaProvider extends ChangeNotifier {
  GrupRepo _grupRepo = GrupRepo();

  String _toastMessage = "";
  String _centerScreenMessage = "Tidak ada anggota perjalanan";
  bool _reqAnggota = false;
  List<AnggotaGrup> _listAnggotaGrup = List();

  String get toastMessage => _toastMessage;
  String get centerScreenMessage => _centerScreenMessage;
  bool get reqAnggota => _reqAnggota;
  List<AnggotaGrup> get listAnggotaGrup => _listAnggotaGrup;

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  set centerScreenMessage(String val) {
    _centerScreenMessage = val;
    notifyListeners();
  }

  set reqAnggota(bool val) {
    _reqAnggota = val;
    notifyListeners();
  }

  set listAnggotaGrup(List<AnggotaGrup> val) {
    _listAnggotaGrup = val;
    notifyListeners();
  }

  void clearData() {
    toastMessage = "";
    centerScreenMessage = "Tidak ada anggota perjalanan";
    reqAnggota = false;
  }

  Future getListAnggotaGrup(String idGrup) async {
    reqAnggota = true;
    var res = await _grupRepo.getListMember(idGrup);
    reqAnggota = false;

    if (res['data'] != null) {
      ResListAnggotaGrup resListAnggotaGrup = res['data'];
      listAnggotaGrup = resListAnggotaGrup.data.listAnggotaGrup.toList();
      if (listAnggotaGrup.length > 0) {
        centerScreenMessage = null;
      } else {
        centerScreenMessage = "Tidak ada anggota perjalanan";
        toastMessage = res['message'];
      }
    } else {
      toastMessage = res['message'];
    }
  }
}
