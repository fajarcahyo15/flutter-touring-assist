import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/repository/grup_repo.dart';

class BuatGrupProvider extends ChangeNotifier {
  GrupRepo _grupRepo = GrupRepo();

  bool _loadingButton = false;
  String _toastMessage = "";

  bool get loadingButton => _loadingButton;
  String get toastMessage => _toastMessage;

  set loadingButton(bool val) {
    _loadingButton = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  Future<bool> createGrup(Grup grup) async {
    loadingButton = true;

    Map<String, dynamic> map = await _grupRepo.create(grup);

    if (map['data'] == null) {
      toastMessage = map['message'];
      loadingButton = false;
      return false;
    } else {
      return true;
    }
  }
}
