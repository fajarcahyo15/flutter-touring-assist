import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_list_grup.dart';
import 'package:new_touring_assist_flutter/repository/grup_repo.dart';

class MainProvider extends ChangeNotifier {
  GrupRepo _grupRepo = GrupRepo();

  String _centerScreenMessage = "";
  bool _loadingScreenCenter = false;
  List<Grup> _listGrup = List();

  String get centerScreenMessage => _centerScreenMessage;
  bool get loadingScreenCenter => _loadingScreenCenter;
  List<Grup> get listGrup => _listGrup;

  set centerScreenMessage(String val) {
    _centerScreenMessage = val;
    notifyListeners();
  }

  set loadingScreenCenter(bool val) {
    _loadingScreenCenter = val;
    notifyListeners();
  }

  set listGrup(List<Grup> val) {
    _listGrup = val;
    notifyListeners();
  }

  void clearData() {
    centerScreenMessage = "";
    loadingScreenCenter = false;
    listGrup = List();
  }

  dynamic fotoDisplayer(String foto) {
    dynamic fotoBytes = AssetImage("assets/images/background-login.jpg");

    if ((foto != null) || (foto != "")) {
      fotoBytes = MemoryImage(base64Decode(foto));
    }

    return fotoBytes;
  }

  void getListGrup() async {
    loadingScreenCenter = true;
    Map<String, dynamic> map = await _grupRepo.getByIdMember();
    loadingScreenCenter = false;

    if (map['data'] == null) {
      centerScreenMessage = map['message'];
      return;
    }

    ResListGrup resListGrup = map['data'];
    listGrup = resListGrup.data.listGrup.toList();
  }
}
