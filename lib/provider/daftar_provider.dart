import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/akun_repo.dart';

class DaftarProvider extends ChangeNotifier {
  AkunRepo _akunRepo = AkunRepo();

  bool _reqDaftar = false;
  String _toastMessage = "";

  bool get reqDaftar => _reqDaftar;
  String get toastMessage => _toastMessage;

  set reqDaftar(bool val) {
    _reqDaftar = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  void clearData() {
    reqDaftar = false;
    toastMessage = "";
  }

  Future<bool> daftarAkun(Map<String, dynamic> input) async {
    bool success = false;

    reqDaftar = true;
    var res = await _akunRepo.daftarAkun(input);

    if (res['data'] != null) {
      ResBase resBase = res['data'];
      toastMessage = resBase.message;
      success = true;
    } else {
      toastMessage = res['message'];
      success = false;
    }

    reqDaftar = false;
    return success;
  }
}
