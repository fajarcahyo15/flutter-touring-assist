import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_detail_grup_rute.dart';
import 'package:new_touring_assist_flutter/repository/grup_rute_repo.dart';

class LihatFasePerjalananProvider extends ChangeNotifier {
  GrupRuteRepo _grupRuteRepo = GrupRuteRepo();

  bool _loadingScreen = false;
  String _toastMessage = "";

  bool get loadingScreen => _loadingScreen;
  String get toastMessage => _toastMessage;

  set loadingScreen(bool val) {
    _loadingScreen = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  void clearData() {
    loadingScreen = false;
    toastMessage = "";
  }

  Future<GrupRute> getGrupRute(int idGrupRute) async {
    GrupRute grupRute;
    var res = await _grupRuteRepo.getByIdGrupRute(idGrupRute);

    if (res['data'] != null) {
      ResDetailGrupRute resDetailGrupRute = res['data'];
      grupRute = resDetailGrupRute.data.grupRute;
    } else {
      toastMessage = res['message'];
    }

    return grupRute;
  }
}
