import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/direction/direction.dart';
import 'package:new_touring_assist_flutter/model/response/direction/route.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/forecast.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/weather_forecast_owm.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_detail_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_buat_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_last_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/weather/res_bad_weather.dart';
import 'package:new_touring_assist_flutter/repository/google_repo.dart';
import 'package:new_touring_assist_flutter/repository/grup_repo.dart';
import 'package:new_touring_assist_flutter/repository/grup_rute_repo.dart';
import 'package:new_touring_assist_flutter/repository/owm_repo.dart';
import 'package:new_touring_assist_flutter/repository/weather_repo.dart';

class TambahFasePerjalananProvider extends ChangeNotifier {
  final GoogleRepo _googleRepo = GoogleRepo();
  final OwmRepo _owmRepo = OwmRepo();
  final GrupRepo _grupRepo = GrupRepo();
  final GrupRuteRepo _grupRuteRepo = GrupRuteRepo();
  final WeatherRepo _weatherRepo = WeatherRepo();

  bool _loadingBtnLihatRute = false;
  String _toastMessage;
  List<Map<String, dynamic>> _listDetailRute = List();
  int _currentRouteIndex = 0;

  bool get loadingBtnLihatRute => _loadingBtnLihatRute;
  String get toastMessage => _toastMessage;
  List<Map<String, dynamic>> get listDetailRute => _listDetailRute;
  int get currentRouteIndex => _currentRouteIndex;

  set loadingBtnLihatRute(bool val) {
    _loadingBtnLihatRute = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  set listDetailRute(List<Map<String, dynamic>> val) {
    _listDetailRute = val;
    notifyListeners();
  }

  set currentRouteIndex(int val) {
    _currentRouteIndex = val;
    notifyListeners();
  }

  void clearData() {
    loadingBtnLihatRute = false;
    toastMessage = null;
    listDetailRute = List();
    currentRouteIndex = 0;
  }

  Future<int> getTimestampTglBerangkat(String idGrup) async {
    int tglBerangkat;

    var res = await _grupRepo.getById(idGrup);

    if (res['data'] != null) {
      ResDetailGrup resDetailGrup = res['data'];
      DateTime dt = DateFormat("yyyy-MM-dd hh:mm:ss").parse(resDetailGrup.data.grup.tglBerangkat);
      tglBerangkat = (dt.millisecondsSinceEpoch / 1000).round();
    } else {
      toastMessage = res['message'];
    }

    return tglBerangkat;
  }

  Future<GrupRute> getRuteTerakhir(String idGrup) async {
    GrupRute grupRute;

    var res = await _grupRuteRepo.getLastRouteByIdGrup(idGrup);

    if (res['data'] != null) {
      try {
        ResLastGrupRute resLastGrupRute = res['data'];
        grupRute = resLastGrupRute.data.grupRute;
      } catch (_) {}
    } else {
      toastMessage = res['message'];
    }

    return grupRute;
  }

  Future<List<Map<String, dynamic>>> getListDetailRute(List<Route> routes, int tsWaktuBerangkat) async {
    List<Map<String, dynamic>> mListDetailRute = List();
    List<Map<String, dynamic>> mListWeather = List();
    ResBadWeather resBadWeather;
    int waktuTempuh = 0;
    int jarak = 0;

    Map<String, dynamic> resListBadWeather = await _weatherRepo.getListBadWeather();
    if (resListBadWeather['message'] == null) {
      resBadWeather = resListBadWeather['data'];
    }

    for (var i = 0; i < routes.length; i++) {
      Route route = routes[i];
      bool badWeather = false;

      for (var leg in route.legs) {
        Map<String, int> map = Map();
        map['waktuTempuh'] = leg.duration.value;
        map['jarak'] = leg.distance.value;

        for (var step in leg.steps) {
          waktuTempuh += step.duration.value;
          jarak += step.distance.value;

          if (jarak >= 5000) {
            LatLng latLng = LatLng(step.endLocation.lat, step.endLocation.lng);
            Map<String, dynamic> resForecast = await _owmRepo.forecast5d3h(latLng);

            if (resForecast['data'] != null) {
              Forecast forecast = resForecast['data'];
              int tsWaktuSampai = tsWaktuBerangkat + waktuTempuh;

              int min = 9999999999999;
              WeatherForecastOwm weatherForecastOwm;
              for (var data in forecast.list) {
                // mendapatkan selisih timestamp terkecil
                int minCandidates = (data.dt - tsWaktuSampai);

                if (minCandidates < 0) minCandidates *= -1;

                if (minCandidates < min) {
                  min = minCandidates;
                  weatherForecastOwm = data;
                }
              }

              // tambah kondisi cuaca dimasing2 koordinat
              mListWeather.add({
                'weather': weatherForecastOwm.weather[0],
                'latlng': LatLng(step.endLocation.lat, step.endLocation.lng),
              });

              // TODO: cek rute perlu dihindari
              if (resBadWeather != null) {
                for (var j = 0; j < resBadWeather.data.listBadWeather.length; j++) {
                  if (weatherForecastOwm.weather[0].id == resBadWeather.data.listBadWeather[j].id) {
                    badWeather = true;
                    break;
                  }
                }
              }

              jarak = 0;
            }
          }
        }

        mListDetailRute.add({
          'route': route,
          'listWeather': mListWeather,
          'badWeather': badWeather,
          'jarak': map['jarak'],
          'waktuTempuh': map['waktuTempuh'],
          'rekomendasi': false,
        });
      }
    }

    // TODO: tentukan rute paling direkomendasikan (ada cuaca yang tidak buruk)
    var jarakTerpendek = 999999999999999;
    for (var i = 0; i < mListDetailRute.length; i++) {
      if (!mListDetailRute[i]['badWeather']) {
        if (mListDetailRute[i]['jarak'] < jarakTerpendek) {
          for (var detailRute in mListDetailRute) {
            detailRute['rekomendasi'] = false;
          }
          mListDetailRute[i]['rekomendasi'] = true;
          jarakTerpendek = mListDetailRute[i][jarak];
        }
      }
    }

    // TODO: tentukan rute paling direkomendasikan (seluruh rute cuacanya buruk)
    bool haveRecommendRoute = false;
    for (var detailRute in mListDetailRute) {
      if (detailRute['rekomendasi']) {
        haveRecommendRoute = true;
      }
    }
    if (!haveRecommendRoute) {
      for (var i = 0; i < mListDetailRute.length; i++) {
        if (mListDetailRute[i]['jarak'] < jarakTerpendek) {
          for (var detailRute in mListDetailRute) {
            detailRute['rekomendasi'] = false;
          }
          mListDetailRute[i]['rekomendasi'] = true;
          jarakTerpendek = mListDetailRute[i][jarak];
        }
      }
    }

    listDetailRute = mListDetailRute;
    return mListDetailRute;
  }

  Future<Direction> getRoute(String from, String destination, String idGrup) async {
    Direction direction;

    Map<String, dynamic> resDirection = await _googleRepo.direction(from.replaceAll(" ", "+"), destination.replaceAll(" ", "+"));

    if (resDirection['data'] != null) {
      direction = resDirection['data'];
    } else {
      toastMessage = "Terjadi kesalahan sistem";
    }
    return direction;
  }

  Future<LatLng> getCurrentLocation() async {
    LatLng currentLocation;
    var location = Location();

    try {
      LocationData res = await location.getLocation();
      currentLocation = LatLng(res.latitude, res.longitude);
    } on PlatformException catch (e) {
      if (e.code == "PERMISSION_DENIED") {
        toastMessage = "Izin untuk mengetahui lokasi tidak didapatkan";
      }
      currentLocation = null;
    }

    return currentLocation;
  }

  Future<bool> create(GrupRute input) async {
    bool success = false;
    Map<String, dynamic> res = await _grupRuteRepo.create(input);

    if (res['data'] != null) {
      ResBuatGrupRute resBuatGrupRute = res['data'];
      success = resBuatGrupRute.status;
      toastMessage = resBuatGrupRute.message;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }
}
