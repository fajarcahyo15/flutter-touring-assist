import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/repository/akun_repo.dart';
import 'package:new_touring_assist_flutter/repository/auth_repo.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';
import 'package:new_touring_assist_flutter/singleton/fcm_singleton.dart';

class LoginProvider extends ChangeNotifier {
  AuthRepo _authRepo = AuthRepo();
  AkunRepo _akunRepo = AkunRepo();
  Session _session = Session();

  String _errLoginMessage;
  bool _requestLogin;

  String get errLoginMessage => _errLoginMessage;
  bool get requestLogin => _requestLogin;

  set errLoginMessage(String val) {
    _errLoginMessage = val;
    notifyListeners();
  }

  set requestLogin(bool val) {
    _requestLogin = val;
    notifyListeners();
  }

  void clearData() {
    errLoginMessage = null;
    requestLogin = null;
  }

  Future<bool> login(String username, String password) async {
    errLoginMessage = null;
    requestLogin = true;

    Map<String, dynamic> resLogin = await _authRepo.login(username, password);
    if (resLogin['status']) {
      await _session.setToken(resLogin['token']);
      return true;
    } else {
      errLoginMessage = resLogin['message'];
    }

    requestLogin = false;
    return false;
  }

  Future<bool> updateFcmToken() async {
    bool success = false;
    String fcmToken = await FcmSingleton.instance.firebaseMessaging.getToken();

    Map<String, dynamic> res = await _akunRepo.updateFcmToken(fcmToken);
    if (res['data'] != null) {
      success = true;
    } else {
      _session.setToken(null).then((_) {
        errLoginMessage = res['message'];
      });
    }

    return success;
  }

  Future<String> cekToken() async {
    return _session.getToken();
  }
}
