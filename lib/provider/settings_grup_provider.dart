import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_detail_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_edit_grup.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/grup_member_repo.dart';
import 'package:new_touring_assist_flutter/repository/grup_repo.dart';
import 'package:new_touring_assist_flutter/repository/grup_rute_repo.dart';

class SettingsGrupProvider extends ChangeNotifier {
  final GrupRepo _grupRepo = GrupRepo();
  final GrupRuteRepo _grupRuteRepo = GrupRuteRepo();
  final GrupMemberRepo _grupMemberRepo = GrupMemberRepo();

  String _toastMessage = "";
  bool _loadingScreen = false;
  Grup _grup = Grup();
  String _oriDes = "-";
  Uint8List _fotoBytes;

  String get toastMessage => _toastMessage;
  bool get loadingScreen => _loadingScreen;
  Grup get grup => _grup;
  String get oriDes => _oriDes;
  Uint8List get fotoBytes => _fotoBytes;

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  set loadingScreen(bool val) {
    _loadingScreen = val;
    notifyListeners();
  }

  set grup(Grup val) {
    _grup = val;
    notifyListeners();
  }

  set oriDes(String val) {
    _oriDes = val;
    notifyListeners();
  }

  set fotoBytes(Uint8List val) {
    _fotoBytes = val;
    notifyListeners();
  }

  void clearData() {
    toastMessage = "";
    loadingScreen = false;
    grup = Grup();
    oriDes = "-";
    fotoBytes = null;
  }

  Future<bool> getInfoGrup(String idGrup) async {
    bool success = true;
    var res = await _grupRepo.getById(idGrup);
    var resOriDes = await _grupRuteRepo.getOriDesByIdGrup(idGrup);

    if (res['data'] == null) {
      toastMessage = res['messsage'];
      success = false;
    }

    if (resOriDes['data'] == null) {
      toastMessage = "Terjadi kesalahan dalam memuat data";
      success = false;
    }

    ResDetailGrup resDetailGrup = res['data'];
    grup = resDetailGrup.data.grup;
    oriDes = resOriDes['data'];

    if ((grup?.fotoGrup != "") && (grup?.fotoGrup != null)) {
      fotoBytes = base64Decode(grup.fotoGrup);
    }

    return success;
  }

  Future<bool> updateInfo(Grup grupUpdate) async {
    bool success = true;
    loadingScreen = true;

    var res = await _grupRepo.update(grupUpdate);

    if (res['data'] == null) {
      toastMessage = res['message'];
      success = false;
    }

    ResEditGrup resEditGrup = res['data'];
    grup = resEditGrup.data.grup;
    loadingScreen = false;

    return success;
  }

  Future<bool> updateFoto(String idGrup, File foto) async {
    bool success = false;

    String fotoString = base64Encode(foto.readAsBytesSync());
    var res = await _grupRepo.updateFoto(idGrup, fotoString);
    if (res['data'] != null) {
      ResBase resBase = res['data'];
      getInfoGrup(idGrup);
      toastMessage = resBase.message;
      success = true;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }

  Future<bool> deleteGrup(String idGrup) async {
    bool success = false;

    loadingScreen = true;
    var res = await _grupRepo.delete(idGrup);
    loadingScreen = false;

    if (res['data'] != null) {
      success = true;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }

  Future<bool> keluarGrup(String idGrup) async {
    bool success = false;

    loadingScreen = true;
    var res = await _grupMemberRepo.keluarGrup(idGrup);
    loadingScreen = false;

    if (res['data'] != null) {
      success = true;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }

  String tglKirimFormatter(String tgl) {
    initializeDateFormatting();
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm");
    DateTime dateTime = dateFormat.parse(tgl);
    DateFormat transform = DateFormat("yyyy-MM-dd", "id");
    String tglBerangkat = transform.format(dateTime);

    return tglBerangkat;
  }

  String tampilanTglBerangkat(String tgl) {
    String tglBerangkat = "-";

    if (tgl != null) {
      initializeDateFormatting();
      DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm");
      DateTime dateTime = dateFormat.parse(tgl);
      DateFormat transform = DateFormat("dd MMMM yyyy", "id");
      tglBerangkat = transform.format(dateTime);
    }

    return tglBerangkat;
  }

  String tampilanJamBerangkat(String tgl) {
    String jamBerangkat = "-";

    if (tgl != null) {
      initializeDateFormatting();
      DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm");
      DateTime dateTime = dateFormat.parse(tgl);
      DateFormat transform = DateFormat("HH:mm", "id");
      jamBerangkat = transform.format(dateTime);
    }

    return jamBerangkat;
  }
}
