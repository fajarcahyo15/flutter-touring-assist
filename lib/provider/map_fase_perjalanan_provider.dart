import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_list_grup_rute.dart';
import 'package:new_touring_assist_flutter/repository/grup_rute_repo.dart';

class MapFasePerjalananProvider extends ChangeNotifier {
  GrupRuteRepo _grupRuteRepo = GrupRuteRepo();

  bool _loadingScreen = false;
  String _toastMessage = "";

  bool get loadingScreen => _loadingScreen;
  String get toastMessage => _toastMessage;

  set loadingScreen(bool val) {
    _loadingScreen = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  Future<List<GrupRute>> getListGrupRute(String idGrup) async {
    List<GrupRute> listGrupRute;
    var res = await _grupRuteRepo.getByIdGrup(idGrup);

    if (res['data'] != null) {
      ResListGrupRute resListGrupRute = res['data'];
      listGrupRute = resListGrupRute.data.listRute.toList();
    } else {
      toastMessage = res['message'];
    }

    return listGrupRute;
  }
}
