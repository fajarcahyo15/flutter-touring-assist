import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/anggota.dart';
import 'package:new_touring_assist_flutter/model/response/anggota/res_detail_anggota.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/akun_repo.dart';
import 'package:new_touring_assist_flutter/repository/anggota_repo.dart';
import 'package:new_touring_assist_flutter/repository/auth_repo.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';

class ProfilProvider extends ChangeNotifier {
  final Session _session = Session();
  final AuthRepo _authRepo = AuthRepo();
  final AnggotaRepo _anggotaRepo = AnggotaRepo();
  final AkunRepo _akunRepo = AkunRepo();

  bool _loadingPage = false;
  String _toastMessage = "";
  Anggota _profil;
  Uint8List _fotoBytes;

  bool get loadingPage => _loadingPage;
  String get toastMessage => _toastMessage;
  Anggota get profil => _profil;
  Uint8List get fotoBytes => _fotoBytes;

  set loadingPage(bool val) {
    _loadingPage = val;
    notifyListeners();
  }

  set toastMessage(String val) {
    _toastMessage = val;
    notifyListeners();
  }

  set profil(Anggota val) {
    _profil = val;
    notifyListeners();
  }

  set fotoBytes(Uint8List val) {
    _fotoBytes = val;
    notifyListeners();
  }

  void clearData() {
    loadingPage = false;
    toastMessage = "";
    profil = null;
    fotoBytes = null;
  }

  Future<Anggota> getData() async {
    Anggota anggota;

    var res = await _anggotaRepo.getProfil();
    if (res['data'] != null) {
      ResDetailAnggota resDetailAnggota = res['data'];
      anggota = resDetailAnggota.data.anggota;
      profil = anggota;

      if ((profil?.foto != "") && (profil?.foto != null)) {
        fotoBytes = base64Decode(profil.foto);
      }
    } else {
      toastMessage = res['message'];
    }

    return anggota;
  }

  Future<bool> updateProfil(Anggota profil) async {
    bool success = false;

    var res = await _anggotaRepo.updateProfil(profil);
    if (res['data'] != null) {
      ResBase resBase = res['data'];
      success = true;
      getData();
      toastMessage = resBase.message;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }

  Future<bool> updateFoto(File foto) async {
    bool success = false;

    String stringFoto = base64Encode(foto.readAsBytesSync());
    var res = await _anggotaRepo.updateFoto(stringFoto);
    if (res['data'] != null) {
      ResBase resBase = res['data'];
      success = true;
      getData();
      toastMessage = resBase.message;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }

  Future<bool> updatePassword(String passwordLama, String passwordBaru) async {
    bool success = false;

    var res = await _akunRepo.updatePassword(passwordLama, passwordBaru);
    if (res['data'] != null) {
      ResBase resBase = res['data'];
      success = true;
      toastMessage = resBase.message;
    } else {
      toastMessage = res['message'];
    }

    return success;
  }

  Future<bool> logout() async {
    bool success = false;
    loadingPage = true;

    var res = await _authRepo.logout();
    if (res['data'] != null) {
      await _session.setToken(null);
      success = true;
    } else {
      toastMessage = res['message'];
    }

    loadingPage = false;
    return success;
  }
}
