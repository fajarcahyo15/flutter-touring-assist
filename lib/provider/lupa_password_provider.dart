import 'package:flutter/foundation.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/akun_repo.dart';

class LupaPasswordProvider extends ChangeNotifier {
  AkunRepo _akunRepo = AkunRepo();

  bool _reqLupa = false;
  String _alertMessage = "";

  bool get reqLupa => _reqLupa;
  String get alertMessage => _alertMessage;

  set reqLupa(bool val) {
    _reqLupa = val;
    notifyListeners();
  }

  set alertMessage(String val) {
    _alertMessage = val;
    notifyListeners();
  }

  void clearData() {
    reqLupa = false;
    alertMessage = "";
  }

  Future<bool> lupaPassword(String email) async {
    bool success = false;
    reqLupa = true;

    var res = await _akunRepo.lupaPassword(email);
    if (res['data'] != null) {
      ResBase resBase = res['data'];
      alertMessage = resBase.message;
      success = true;
    } else {
      alertMessage = res['message'];
      success = false;
    }

    reqLupa = false;
    return success;
  }
}
