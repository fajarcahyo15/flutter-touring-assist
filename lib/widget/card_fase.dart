import 'package:flutter/material.dart';

class CardFase extends StatelessWidget {
  final GestureTapCallback onTap;
  final GestureLongPressCallback onLongPress;
  final String title;
  final String detail;
  final bool delete;
  final VoidCallback onDelete;

  const CardFase({
    Key key,
    this.onTap,
    this.onLongPress,
    @required this.title,
    @required this.detail,
    this.delete = false,
    this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: Theme.of(context).cardTheme.elevation,
      child: InkWell(
        onTap: onTap,
        onLongPress: onLongPress,
        child: Container(
          padding: EdgeInsets.all(4),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: Theme.of(context).textTheme.title,
                    ),
                    Text(
                      detail,
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  ],
                ),
              ),
              (delete)
                  ? IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Colors.grey,
                      ),
                      onPressed: onDelete,
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
