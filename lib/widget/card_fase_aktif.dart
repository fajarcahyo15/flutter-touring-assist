import 'package:flutter/material.dart';

class CardFaseAktif extends StatelessWidget {
  final String title;
  final String detail;
  final bool active;
  final GestureTapCallback onTap;
  final VoidCallback onActivate;
  final VoidCallback onDeactivate;

  const CardFaseAktif({
    Key key,
    @required this.title,
    @required this.detail,
    @required this.active,
    this.onTap,
    @required this.onActivate,
    @required this.onDeactivate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: Theme.of(context).cardTheme.elevation,
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(4),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: Theme.of(context).textTheme.title,
                    ),
                    Text(
                      detail,
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  ],
                ),
              ),
              (active)
                  ? IconButton(
                      icon: Icon(
                        Icons.check,
                        color: Colors.green,
                      ),
                      onPressed: onDeactivate,
                    )
                  : IconButton(
                      icon: Icon(
                        Icons.close,
                        color: Colors.red,
                      ),
                      onPressed: onActivate,
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
