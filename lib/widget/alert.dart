import 'package:flutter/material.dart';

class Alert {
  static void loading({
    @required BuildContext context,
    String content: "Loading...",
  }) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          content: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CircularProgressIndicator(),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Text(content),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  static void inform({
    @required BuildContext context,
    String title: "Informasi",
    @required String content,
    VoidCallback onYes,
  }) {
    onYes = onYes ?? () => Navigator.pop(context);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: onYes,
            ),
          ],
        );
      },
    );
  }

  static void confirm({
    @required BuildContext context,
    String title: "Konfirmasi",
    @required String content,
    @required VoidCallback onYes,
    VoidCallback onCancel,
  }) {
    onCancel = onCancel ?? () => Navigator.pop(context);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text("BATAL"),
              onPressed: onCancel,
            ),
            FlatButton(
              child: Text("OK"),
              onPressed: onYes,
            ),
          ],
        );
      },
    );
  }

  static void pickImage({
    @required BuildContext context,
    String title: "Ambil Gambar",
    String textCamera: "Kamera",
    String textGallery: "Galeri",
    @required VoidCallback onFromCamera,
    @required VoidCallback onFromGallery,
  }) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RaisedButton(
                color: Theme.of(context).primaryColor,
                child: Text(
                  textGallery,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: onFromGallery,
              ),
              RaisedButton(
                color: Theme.of(context).primaryColor,
                child: Text(
                  textCamera,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: onFromCamera,
              ),
            ],
          ),
        );
      },
    );
  }
}
