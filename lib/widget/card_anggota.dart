import 'package:flutter/material.dart';

class CardAnggota extends StatelessWidget {
  final GestureTapCallback onTap;
  final ImageProvider<dynamic> image;
  final String title;
  final String noHp;
  final String status;

  const CardAnggota({
    Key key,
    this.onTap,
    this.image,
    @required this.title,
    @required this.noHp,
    this.status,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            CircleAvatar(
              backgroundImage: (image == null)
                  ? AssetImage(
                      "assets/images/background-login.jpg",
                    )
                  : image,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Text(
                        noHp,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            (status != null)
                ? Container(
                    padding: EdgeInsets.all(1.5),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.green,
                      ),
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Text(
                      status,
                      style: TextStyle(
                        color: Colors.green,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
