import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ImageViewer extends StatelessWidget {
  final ImageProvider imageProvider;
  final String title;

  const ImageViewer({
    Key key,
    this.title,
    @required this.imageProvider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text((title != null) ? title : "Lihat Foto"),
        elevation: 0,
      ),
      body: Container(
        child: PhotoView(
          imageProvider: imageProvider,
          basePosition: Alignment.center,
        ),
      ),
    );
  }
}
