import 'package:flutter/material.dart';

class CardLoading extends StatelessWidget {
  final String loadingMessage;

  const CardLoading({
    Key key,
    this.loadingMessage = "Memuat data...",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Wrap(
          children: <Widget>[
            SizedBox(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(loadingMessage),
            ),
          ],
        ),
      ),
    );
  }
}
