import 'package:flutter/material.dart';

class CardGrup extends StatelessWidget {
  final GestureTapCallback onTap;
  final ImageProvider<dynamic> image;
  final String title;
  final String chat;

  const CardGrup({
    Key key,
    @required this.title,
    @required this.onTap,
    this.image,
    this.chat = "Belum ada percakapan",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              CircleAvatar(
                backgroundImage: (image == null)
                    ? AssetImage(
                        "assets/images/background-login.jpg",
                      )
                    : image,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        title,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          chat,
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
