import 'package:flutter/material.dart';

class IndicatorLoadingButton extends StatelessWidget {
  final String loadingText;

  const IndicatorLoadingButton({
    Key key,
    @required this.loadingText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        CircularProgressIndicator(),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 10),
            child: Text("$loadingText"),
          ),
        )
      ],
    );
  }
}
