import 'package:flutter/material.dart';

class TFFBordered extends StatelessWidget {
  final TextEditingController controller;
  final String labelText;
  final TextInputType keyboardType;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;
  final bool obscureText;
  final int maxLines;
  final bool enabled;
  final String prefixText;

  const TFFBordered({
    Key key,
    @required this.labelText,
    this.keyboardType = TextInputType.text,
    this.validator,
    this.onSaved,
    this.obscureText = false,
    this.maxLines = 1,
    this.controller,
    this.enabled = true,
    this.prefixText = "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: key,
      controller: controller,
      enabled: enabled,
      decoration: InputDecoration(
        labelText: labelText,
        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
        prefixText: prefixText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
      ),
      keyboardType: keyboardType,
      validator: validator,
      onSaved: onSaved,
      obscureText: obscureText,
      maxLines: maxLines,
    );
  }
}
