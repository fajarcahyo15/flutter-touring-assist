import 'package:flutter/material.dart';

class CardMenu extends StatelessWidget {
  final GestureTapCallback onTap;
  final String title;
  final Icon icon;

  const CardMenu({
    Key key,
    this.onTap,
    @required this.title,
    @required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: Theme.of(context).cardTheme.elevation,
      child: InkWell(
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 8.0,
              ),
              child: icon,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: Theme.of(context).textTheme.title,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        onTap: onTap,
      ),
    );
  }
}
