import 'package:flutter/material.dart';

class ItemSettingsInCardWithButton extends StatelessWidget {
  final String title;
  final String detail;
  final GestureTapCallback onTap;

  const ItemSettingsInCardWithButton({
    Key key,
    @required this.title,
    @required this.detail,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                detail,
                style: TextStyle(
                  fontStyle: FontStyle.italic,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
        InkWell(
          child: Text(
            "Ganti",
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).primaryColor,
            ),
          ),
          onTap: onTap,
        ),
      ],
    );
  }
}
