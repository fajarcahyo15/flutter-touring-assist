import 'package:flutter/material.dart';

class TFFPassword extends StatefulWidget {
  final String labelText;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;
  final TextEditingController controller;

  const TFFPassword({
    Key key,
    @required this.labelText,
    this.validator,
    this.onSaved,
    this.controller,
  }) : super(key: key);

  @override
  _TFFPasswordState createState() => _TFFPasswordState();
}

class _TFFPasswordState extends State<TFFPassword> {
  bool _passwordHidden;
  Icon _tffPasswordIcon;

  @override
  void initState() {
    _passwordHidden = true;
    _tffPasswordIcon = Icon(Icons.visibility_off);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: widget.key,
      controller: widget.controller,
      decoration: InputDecoration(
        labelText: widget.labelText,
        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
        suffixIcon: IconButton(
          icon: _tffPasswordIcon,
          onPressed: () {
            if (_passwordHidden)
              _showPassword();
            else
              _hidePassword();
          },
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
      ),
      obscureText: _passwordHidden,
      validator: widget.validator,
      onSaved: widget.onSaved,
    );
  }

  void _showPassword() {
    setState(() {
      _passwordHidden = false;
      _tffPasswordIcon = Icon(Icons.visibility);
    });
  }

  void _hidePassword() {
    setState(() {
      _passwordHidden = true;
      _tffPasswordIcon = Icon(Icons.visibility_off);
    });
  }
}
