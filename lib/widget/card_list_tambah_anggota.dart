import 'package:flutter/material.dart';

class CardListTambahAnggota extends StatelessWidget {
  final GestureTapCallback onTap;
  final VoidCallback onAddPressed;
  final ImageProvider<dynamic> image;
  final String title;
  final String noHp;

  const CardListTambahAnggota({
    Key key,
    @required this.title,
    this.onTap,
    this.onAddPressed,
    this.image,
    this.noHp = "-",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GestureTapCallback _onTap = (this.onTap == null) ? () {} : this.onTap;
    VoidCallback _onAddPressed = (this.onAddPressed == null) ? () {} : this.onAddPressed;

    return Card(
      child: InkWell(
        onTap: _onTap,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              CircleAvatar(
                backgroundImage: (image == null)
                    ? AssetImage(
                        "assets/images/background-login.jpg",
                      )
                    : image,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        title,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4),
                        child: Text(
                          noHp,
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.add,
                  color: Colors.green,
                ),
                onPressed: _onAddPressed,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
