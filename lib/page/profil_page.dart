import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:new_touring_assist_flutter/helper/form_validation.dart';
import 'package:new_touring_assist_flutter/model/anggota.dart';
import 'package:new_touring_assist_flutter/page/login_page.dart';
import 'package:new_touring_assist_flutter/provider/profil_provider.dart';
import 'package:new_touring_assist_flutter/widget/alert.dart';
import 'package:new_touring_assist_flutter/widget/button_circular.dart';
import 'package:new_touring_assist_flutter/widget/card_menu.dart';
import 'package:new_touring_assist_flutter/widget/image_viewer.dart';
import 'package:new_touring_assist_flutter/widget/item_settings_in_card_with_button.dart';
import 'package:new_touring_assist_flutter/widget/tff_password.dart';
import 'package:provider/provider.dart';

class ProfilPage extends StatefulWidget {
  @override
  _ProfilPageState createState() => _ProfilPageState();
}

class _ProfilPageState extends State<ProfilPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormNoHp = GlobalKey<FormState>();
  final _keyFormAlamat = GlobalKey<FormState>();
  final _keyFormPassword = GlobalKey<FormState>();

  final _txtNoHP = TextEditingController();
  final _txtAlamat = TextEditingController();
  final _txtPasswordLama = TextEditingController();
  final _txtPasswordBaru = TextEditingController();
  final _txtPasswordKonfirmasi = TextEditingController();

  ProfilProvider _profilProvider;

  String _textAppBar = "Akun Saya";
  Icon _iconAppBar = Icon(Icons.arrow_back);
  AssetImage _dummyProfilImage = AssetImage("assets/images/dummy-profil.jpg");

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _profilProvider.clearData();
      _profilProvider.getData();
    });
  }

  @override
  void dispose() {
    super.dispose();

    _txtNoHP.dispose();
    _txtAlamat.dispose();
    _txtPasswordLama.dispose();
    _txtPasswordBaru.dispose();
    _txtPasswordKonfirmasi.dispose();
  }

  void _onGantiFotoPressed() async {
    Alert.pickImage(
      context: context,
      title: "Ambil Foto Dari",
      onFromCamera: () async {
        Navigator.pop(context);
        File foto = await ImagePicker.pickImage(source: ImageSource.camera);
        if (foto != null) {
          Alert.loading(context: context);
          _profilProvider.updateFoto(foto).then((success) {
            Navigator.pop(context);
            if (!success) {
              Alert.inform(context: context, content: _profilProvider.toastMessage);
            }
          });
        }
      },
      onFromGallery: () async {
        Navigator.pop(context);
        File foto = await ImagePicker.pickImage(source: ImageSource.gallery);
        if (foto != null) {
          Alert.loading(context: context);
          _profilProvider.updateFoto(foto).then((success) {
            Navigator.pop(context);
            if (!success) {
              Alert.inform(context: context, content: _profilProvider.toastMessage);
            }
          });
        }
      },
    );
  }

  void _onGantiNoHPPressed() {
    _keyScaffold.currentState.showBottomSheet(
      (BuildContext ctx) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          setState(() {
            _textAppBar = "Ganti Nomor HP";
            _iconAppBar = Icon(Icons.close);
          });
        });

        _txtNoHP.text = (_profilProvider.profil?.noHp != null) ? _profilProvider.profil?.noHp : "-";

        return Form(
          key: _keyFormNoHp,
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(32),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text("Pastikan nomor hp pengganti merupakan nomor hp yang masih aktif."),
                    SizedBox(
                      height: 16,
                    ),
                    TextFormField(
                      controller: _txtNoHP,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: "Nomor HP",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      validator: (value) {
                        return FormValidation.textBetween(
                          value: value,
                          min: 11,
                          max: 13,
                          label: "Nomor HP",
                        );
                      },
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    ButtonCircular(
                      text: "Ganti",
                      onPressed: () {
                        if (_keyFormNoHp.currentState.validate()) {
                          Anggota profil = _profilProvider.profil.rebuild((b) => b..noHp = _txtNoHP.text.trim());
                          _profilProvider.updateProfil(profil).then(
                            (success) {
                              if (success) {
                                Alert.inform(
                                  context: context,
                                  content: _profilProvider.toastMessage,
                                  onYes: () {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  },
                                );
                              } else {
                                Alert.inform(context: context, content: _profilProvider.toastMessage);
                              }
                            },
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _onGantiAlamatPressed() {
    _keyScaffold.currentState.showBottomSheet(
      (BuildContext ctx) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          setState(() {
            _textAppBar = "Ganti Alamat";
            _iconAppBar = Icon(Icons.close);
          });
        });

        _txtAlamat.text = (_profilProvider.profil?.alamatLengkap != null) ? _profilProvider.profil?.alamatLengkap : "-";

        return Form(
          key: _keyFormAlamat,
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(32),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text("Pastikan alamat yang anda masukkan benar dan lengkap."),
                    SizedBox(
                      height: 16,
                    ),
                    TextFormField(
                      controller: _txtAlamat,
                      keyboardType: TextInputType.multiline,
                      maxLines: 3,
                      decoration: InputDecoration(
                        labelText: "Alamat",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      validator: (value) {
                        return FormValidation.textBetween(
                          value: value,
                          min: 5,
                          max: 255,
                          label: "Alamat",
                        );
                      },
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    ButtonCircular(
                      text: "Ganti",
                      onPressed: () async {
                        if (_keyFormAlamat.currentState.validate()) {
                          Anggota profil = _profilProvider.profil.rebuild((b) => b..alamatLengkap = _txtAlamat.text.trim());
                          _profilProvider.updateProfil(profil).then(
                            (success) {
                              if (success) {
                                Alert.inform(
                                  context: context,
                                  content: _profilProvider.toastMessage,
                                  onYes: () {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  },
                                );
                              } else {
                                Alert.inform(context: context, content: _profilProvider.toastMessage);
                              }
                            },
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _onGantiPasswordPressed() {
    _keyScaffold.currentState.showBottomSheet(
      (context) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          setState(() {
            _textAppBar = "Ganti Password";
            _iconAppBar = Icon(Icons.close);
          });
        });

        _txtPasswordLama.text = "";
        _txtPasswordBaru.text = "";
        _txtPasswordKonfirmasi.text = "";

        return Form(
          key: _keyFormPassword,
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(32),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text("Perbarui password secara berkala untuk keamanan akun yang lebih baik."),
                    SizedBox(
                      height: 16,
                    ),
                    TFFPassword(
                      controller: _txtPasswordLama,
                      labelText: "Password Lama",
                      validator: (value) {
                        return FormValidation.textRequired(
                          value: value,
                          label: "Password Lama",
                        );
                      },
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TFFPassword(
                      controller: _txtPasswordBaru,
                      labelText: "Password Baru",
                      validator: (value) => FormValidation.textBetween(value: value, min: 8, max: 15, label: "Password Baru"),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    TFFPassword(
                      controller: _txtPasswordKonfirmasi,
                      labelText: "Konfirmasi Password",
                      validator: (value) {
                        if (value.trim() != _txtPasswordBaru.text.trim()) {
                          return "Konfirmasi Password tidak sama dengan Password Baru";
                        } else {
                          return null;
                        }
                      },
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    ButtonCircular(
                      text: "Ganti Password",
                      onPressed: () async {
                        if (_keyFormPassword.currentState.validate()) {
                          Alert.loading(context: context, content: "Mengubah Password...");
                          _profilProvider.updatePassword(_txtPasswordLama.text.trim(), _txtPasswordBaru.text.trim()).then((success) {
                            Navigator.pop(context);
                            Alert.inform(
                              context: context,
                              content: _profilProvider.toastMessage,
                              onYes: () {
                                Navigator.pop(context);
                                if (success) {
                                  Navigator.pop(context);
                                }
                              },
                            );
                          });
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _onKeluarPressed(BuildContext context) {
    Alert.confirm(
      context: context,
      title: "Konfirmasi",
      content: "Apakah Anda yakin ingin keluar?",
      onYes: () async {
        bool success = await _profilProvider.logout();

        if (success) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => LoginPage()),
            (Route<dynamic> route) => false,
          );
        }
      },
      onCancel: () => Navigator.pop(context),
    );
  }

  @override
  Widget build(BuildContext context) {
    _profilProvider = Provider.of<ProfilProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text(_textAppBar),
        leading: IconButton(
          icon: _iconAppBar,
          onPressed: () {
            setState(() {
              _textAppBar = "Akun Saya";
              _iconAppBar = Icon(Icons.arrow_back);
            });
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(4),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 4, left: 4),
                  child: Text(
                    "Foto Profil dan Nama",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 2),
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              InkWell(
                                child: CircleAvatar(
                                  backgroundImage: (_profilProvider.fotoBytes != null) ? MemoryImage(_profilProvider.fotoBytes) : _dummyProfilImage,
                                ),
                                onTap: () {
                                  if (_profilProvider.fotoBytes != null) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ImageViewer(
                                          title: "Foto Profil",
                                          imageProvider: MemoryImage(_profilProvider.fotoBytes),
                                        ),
                                      ),
                                    );
                                  }
                                },
                              ),
                              Positioned(
                                right: 0,
                                bottom: 0,
                                child: SizedBox(
                                  width: 15.0,
                                  height: 15.0,
                                  child: FloatingActionButton(
                                    elevation: 2.0,
                                    child: Icon(
                                      Icons.camera_alt,
                                      size: 10.0,
                                    ),
                                    onPressed: _onGantiFotoPressed,
                                  ),
                                ),
                              )
                            ],
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(left: 16),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "${(_profilProvider.profil?.namaLengkap != null) ? _profilProvider.profil?.namaLengkap : "-"}",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    "${(_profilProvider.profil?.email != null) ? _profilProvider.profil?.email : "-"}",
                                    style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 4, left: 4),
                  child: Text(
                    "Informasi Akun",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 2),
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ItemSettingsInCardWithButton(
                            title: "Nomor HP",
                            detail: (_profilProvider.profil?.noHp != null) ? _profilProvider.profil?.noHp : "-",
                            onTap: _onGantiNoHPPressed,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4, bottom: 4),
                            child: Divider(
                              color: Colors.black,
                              height: 0,
                            ),
                          ),
                          ItemSettingsInCardWithButton(
                            title: "Alamat",
                            detail: (_profilProvider.profil?.alamatLengkap != null) ? _profilProvider.profil?.alamatLengkap : "-",
                            onTap: _onGantiAlamatPressed,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                CardMenu(
                  icon: Icon(Icons.settings),
                  title: "Pengaturan",
                  onTap: () => {},
                ),
                CardMenu(
                  icon: Icon(Icons.vpn_key),
                  title: "Ganti Password",
                  onTap: _onGantiPasswordPressed,
                ),
                CardMenu(
                  icon: Icon(Icons.power_settings_new),
                  title: "Keluar",
                  onTap: () => _onKeluarPressed(context),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
