import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/provider/grup_tambah_anggota_provider.dart';
import 'package:new_touring_assist_flutter/widget/alert.dart';
import 'package:new_touring_assist_flutter/widget/card_list_tambah_anggota.dart';
import 'package:new_touring_assist_flutter/widget/card_loading.dart';
import 'package:provider/provider.dart';

class GrupTambahAnggotaPage extends StatefulWidget {
  final Grup grup;

  const GrupTambahAnggotaPage({
    Key key,
    @required this.grup,
  }) : super(key: key);

  @override
  _GrupTambahAnggotaPageState createState() => _GrupTambahAnggotaPageState();
}

class _GrupTambahAnggotaPageState extends State<GrupTambahAnggotaPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();

  final _txtCari = TextEditingController();

  GrupTambahAnggotaProvider _grupTambahAnggotaProvider;
  bool _searchState = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _grupTambahAnggotaProvider.clearData();
    });
  }

  @override
  void dispose() {
    _txtCari.dispose();
    super.dispose();
  }

  void _onIconSearchPressed() {
    setState(() {
      _searchState = (_searchState) ? false : true;
    });
  }

  void _aksiCariAnggota(String cari) async {
    if (cari != "") {
      var success = await _grupTambahAnggotaProvider.getCariAnggota(widget.grup.idGrup, cari);
      if (!success) {
        _keyScaffold.currentState.showSnackBar(SnackBar(
          content: Text(_grupTambahAnggotaProvider.toastMessage),
        ));
      }
    } else {
      _grupTambahAnggotaProvider.listAnggotaGrup = List();
    }
  }

  void _aksiTambahAnggota(String idAnggota) async {
    Alert.confirm(
      context: context,
      content: "Apakah Anda yakin ingin menambahkan akun kedalam grup?",
      onYes: () async {
        Navigator.pop(context);
        Alert.loading(context: context);
        bool success = await _grupTambahAnggotaProvider.tambahAnggota(widget.grup.idGrup, idAnggota);
        Navigator.pop(context);
        if (success) {
          Alert.inform(
            context: context,
            content: "Berhasil menambah anggota baru",
            onYes: () {
              Navigator.pop(context);
              _aksiCariAnggota(_txtCari.text.trim());
            },
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _grupTambahAnggotaProvider = Provider.of<GrupTambahAnggotaProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: _appBarTitle(),
        actions: <Widget>[
          _widgetSearch(),
        ],
      ),
      body: _displayContent(),
    );
  }

  Widget _displayContent() {
    if (_grupTambahAnggotaProvider.listAnggotaGrup.length > 0) {
      return Stack(
        children: <Widget>[
          ListView.builder(
            itemCount: _grupTambahAnggotaProvider.listAnggotaGrup.length,
            itemBuilder: (context, i) {
              return CardListTambahAnggota(
                title: _grupTambahAnggotaProvider.listAnggotaGrup[i].namaLengkap,
                noHp: "Nomor HP: ${(_grupTambahAnggotaProvider.listAnggotaGrup[i].noHp != null) ? _grupTambahAnggotaProvider.listAnggotaGrup[i].noHp : '-'}",
                onAddPressed: () => _aksiTambahAnggota(_grupTambahAnggotaProvider.listAnggotaGrup[i].idAnggota),
              );
            },
          ),
          (_grupTambahAnggotaProvider.reqCari)
              ? Center(
                  child: CardLoading(),
                )
              : Container(),
        ],
      );
    } else {
      if (_grupTambahAnggotaProvider.reqCari) {
        return Center(
          child: CardLoading(),
        );
      } else {
        return Center(
          child: Text("Tidak ada anggota yang dapat ditampilkan"),
        );
      }
    }
  }

  Widget _appBarTitle() {
    if (_searchState) {
      return TextFormField(
        controller: _txtCari,
        cursorColor: Colors.white,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
          hintText: "Cari...",
          hintStyle: TextStyle(color: Colors.grey),
          suffixIcon: Wrap(
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
                onPressed: () {
                  _txtCari.text = "";
                  _grupTambahAnggotaProvider.listAnggotaGrup = List();
                  _onIconSearchPressed();
                },
              ),
              IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                onPressed: () => _aksiCariAnggota(_txtCari.text.trim()),
              ),
            ],
          ),
        ),
        onFieldSubmitted: (value) => _aksiCariAnggota(value.trim()),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 6),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.grup.namaGrup,
            ),
            Text(
              'Tambah Anggota Perjalanan',
              style: TextStyle(fontSize: 14),
            ),
          ],
        ),
      );
    }
  }

  Widget _widgetSearch() {
    if (!_searchState) {
      return IconButton(
        icon: Icon(Icons.search),
        onPressed: _onIconSearchPressed,
      );
    } else
      return Container();
  }
}
