import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/page/daftar_page.dart';
import 'package:new_touring_assist_flutter/page/lupa_password_page.dart';
import 'package:new_touring_assist_flutter/provider/login_provider.dart';
import 'package:new_touring_assist_flutter/widget/button_circular.dart';
import 'package:new_touring_assist_flutter/widget/indicator_loading_button.dart';
import 'package:new_touring_assist_flutter/widget/tff_bordered.dart';
import 'package:new_touring_assist_flutter/widget/tff_password.dart';
import 'package:provider/provider.dart';

import 'main_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormLogin = GlobalKey<FormState>();

  final _txtEmail = TextEditingController();
  final _txtPassword = TextEditingController();

  LoginProvider _loginProvider;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loginProvider.clearData();
    });
  }

  @override
  void dispose() {
    _txtEmail.dispose();
    _txtPassword.dispose();
    super.dispose();
  }

  void _onLblDaftarPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DaftarPage(),
      ),
    );
  }

  void _onLblLupaPasswordPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LupaPasswordPage(),
      ),
    );
  }

  void _onBtnLoginPressed(BuildContext context) async {
    setState(() {
      _keyFormLogin.currentState.save();
    });

    var email = _txtEmail.text.trim();
    var password = _txtPassword.text.trim();

    bool successAuth = await _loginProvider.login(email, password);
    if (successAuth) {
      bool successUpdateFcmToken = await _loginProvider.updateFcmToken();
      if (successUpdateFcmToken) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => MainPage(),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    _loginProvider = Provider.of<LoginProvider>(context);

    _loginProvider.cekToken().then((token) {
      if ((token != null) && (token != "")) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => MainPage(),
          ),
        );
      }
    });

    return Scaffold(
      key: _keyScaffold,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        decoration: BoxDecoration(
          color: Colors.indigo,
        ),
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Center(
                child: Card(
                  elevation: 15,
                  child: Container(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          "Bikers Touring Assistance",
                          style: Theme.of(context).textTheme.title.copyWith(fontSize: 20.0),
                        ),
                        (_loginProvider.errLoginMessage != null)
                            ? Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.all(6),
                                        decoration: BoxDecoration(color: Colors.red[200]),
                                        child: Text(_loginProvider.errLoginMessage),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : Container(),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Form(
                            key: _keyFormLogin,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                TFFBordered(
                                  controller: _txtEmail,
                                  labelText: "Email",
                                  keyboardType: TextInputType.emailAddress,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 8),
                                  child: TFFPassword(
                                    controller: _txtPassword,
                                    labelText: "Password",
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 16),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      InkWell(
                                        child: Text("Daftar"),
                                        onTap: () => _onLblDaftarPressed(context),
                                      ),
                                      InkWell(
                                        child: Text("Lupa password?"),
                                        onTap: () => _onLblLupaPasswordPressed(context),
                                      ),
                                    ],
                                  ),
                                ),
                                (_loginProvider.requestLogin == true)
                                    ? Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: IndicatorLoadingButton(
                                          loadingText: "Melakukan autetikasi...",
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: ButtonCircular(
                                          text: "Login",
                                          onPressed: () => _onBtnLoginPressed(context),
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
