import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/page/map_fase_perjalanan_page.dart';
import 'package:new_touring_assist_flutter/page/form_fase_perjalanan/tambah_fase_perjalanan_page.dart';
import 'package:new_touring_assist_flutter/provider/fase_perjalanan_provider.dart';
import 'package:new_touring_assist_flutter/widget/alert.dart';
import 'package:new_touring_assist_flutter/widget/card_fase.dart';
import 'package:new_touring_assist_flutter/widget/card_loading.dart';
import 'package:provider/provider.dart';

import 'form_fase_perjalanan/lihat_fase_perjalanan_page.dart';

class FasePerjalananPage extends StatefulWidget {
  final String idGrup;

  const FasePerjalananPage({
    Key key,
    @required this.idGrup,
  }) : super(key: key);

  @override
  _FasePerjalananPageState createState() => _FasePerjalananPageState();
}

class _FasePerjalananPageState extends State<FasePerjalananPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();

  FasePerjalananProvider _fasePerjalananProvider;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _fasePerjalananProvider.clearData();
      _getListData();
    });
  }

  void _getListData() async {
    bool have = await _fasePerjalananProvider.getListGrupRute(widget.idGrup);
    if (have != true) {
      _keyScaffold.currentState.showSnackBar(
        (SnackBar(
          content: Text(_fasePerjalananProvider.toastMessage),
        )),
      );
    }
  }

  void _onMapPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MapFasePerjalananPage(
          idGrup: widget.idGrup,
        ),
      ),
    ).then((_) {
      _getListData();
    });
  }

  void _onItemPressed(BuildContext context, int idGrupRute) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LihatFasePerjalananPage(
          idGrupRute: idGrupRute,
        ),
      ),
    ).then((_) {
      _getListData();
    });
  }

  void _onItemDeleteFase(BuildContext context, String title, String idGrupRute) {
    Alert.confirm(
      context: context,
      title: "Konfirmasi",
      content: "Apakah Anda yakin ingin menghapus \"$title\"?",
      onYes: () async {
        bool success = await _fasePerjalananProvider.deleteGrupRute(idGrupRute);
        if (success) {
          _getListData();
        } else {
          _keyScaffold.currentState.showSnackBar(
            SnackBar(
              content: Text(_fasePerjalananProvider.toastMessage),
            ),
          );
        }
        Navigator.pop(context);
      },
      onCancel: () => Navigator.pop(context),
    );
  }

  void _onBtnAddPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => TambahFasePerjalananPage(
          idGrup: widget.idGrup,
        ),
      ),
    ).then((_) {
      _getListData();
    });
  }

  @override
  Widget build(BuildContext context) {
    _fasePerjalananProvider = Provider.of<FasePerjalananProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text("Fase Perjalanan"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.map),
            tooltip: "Lihat Seluruh Rute Perjalanan",
            onPressed: () => _onMapPressed(context),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: "Buat Fase Perjalanan",
        mini: true,
        onPressed: () => _onBtnAddPressed(context),
      ),
      body: Container(
        padding: EdgeInsets.all(4),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 4),
                    child: Text("Daftar Fase Perjalanan"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 4),
                  child: InkWell(
                    child: Icon(Icons.refresh),
                    onTap: () => _fasePerjalananProvider.getListGrupRute(widget.idGrup),
                  ),
                )
              ],
            ),
            Expanded(
              child: (_fasePerjalananProvider.listGrupRute.length > 0)
                  ? ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: _fasePerjalananProvider.listGrupRute.length,
                      itemBuilder: (context, i) {
                        bool delete = false;
                        if (_fasePerjalananProvider.listGrupRute.length == (i + 1)) {
                          delete = true;
                        }

                        return CardFase(
                          title: "Fase Perjalanan ${i + 1}",
                          detail: "${_fasePerjalananProvider.listGrupRute[i].origin} -> ${_fasePerjalananProvider.listGrupRute[i].destination}",
                          delete: delete,
                          onTap: () => _onItemPressed(context, _fasePerjalananProvider.listGrupRute[i].idGrupRute),
                          onDelete: () => _onItemDeleteFase(context, "Fase Perjalanan ${i + 1}", _fasePerjalananProvider.listGrupRute[i].idGrupRute.toString()),
                        );
                      },
                    )
                  : (_fasePerjalananProvider.loadingPage)
                      ? Center(
                          child: CardLoading(),
                        )
                      : Center(
                          child: Text("Belum ada fase perjalanan"),
                        ),
            ),
          ],
        ),
      ),
    );
  }
}
