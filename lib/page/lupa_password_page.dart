import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/helper/form_validation.dart';
import 'package:new_touring_assist_flutter/provider/lupa_password_provider.dart';
import 'package:new_touring_assist_flutter/widget/alert.dart';
import 'package:new_touring_assist_flutter/widget/button_circular.dart';
import 'package:new_touring_assist_flutter/widget/indicator_loading_button.dart';
import 'package:new_touring_assist_flutter/widget/tff_bordered.dart';
import 'package:provider/provider.dart';

class LupaPasswordPage extends StatefulWidget {
  @override
  _LupaPasswordPageState createState() => _LupaPasswordPageState();
}

class _LupaPasswordPageState extends State<LupaPasswordPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormLupaPassword = GlobalKey<FormState>();

  final _txtEmail = TextEditingController();

  LupaPasswordProvider _lupaPasswordProvider;

  bool _autoValidate = false;

  @override
  void dispose() {
    _txtEmail.dispose();
    super.dispose();
  }

  void _onBtnKirimPressed() async {
    if (_keyFormLupaPassword.currentState.validate()) {
      bool success = await _lupaPasswordProvider.lupaPassword(_txtEmail.text.trim());
      Alert.inform(
        context: context,
        title: (success) ? 'Sukses!' : 'Terjadi Kesalahan!',
        content: _lupaPasswordProvider.alertMessage,
      );
    }

    setState(() => _autoValidate = true);
  }

  @override
  Widget build(BuildContext context) {
    _lupaPasswordProvider = Provider.of<LupaPasswordProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text("Lupa Password"),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        child: Form(
          key: _keyFormLupaPassword,
          autovalidate: _autoValidate,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 32, horizontal: 32),
                child: Text(
                  "Masukkan email yang telah didaftarkan. Kami akan mengirimkan link untuk mereset password Anda ke email yang dimasukkan.",
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 32),
                child: Column(
                  children: <Widget>[
                    TFFBordered(
                      controller: _txtEmail,
                      labelText: "Email",
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        return FormValidation.email(
                          value: value,
                          label: "Email",
                        );
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: SizedBox(
                        width: double.infinity,
                        child: (!_lupaPasswordProvider.reqLupa)
                            ? ButtonCircular(
                                text: "Kirim",
                                onPressed: () => _onBtnKirimPressed(),
                              )
                            : IndicatorLoadingButton(loadingText: 'Mengirim permohonan pemulihan password'),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
