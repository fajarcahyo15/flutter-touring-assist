import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/page/buat_grup_page.dart';
import 'package:new_touring_assist_flutter/page/grup_page.dart';
import 'package:new_touring_assist_flutter/page/profil_page.dart';
import 'package:new_touring_assist_flutter/provider/main_provider.dart';
import 'package:new_touring_assist_flutter/widget/card_grup.dart';
import 'package:new_touring_assist_flutter/widget/card_loading.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();

  MainProvider _mainProvider;

  DateTime _currentBackPressTime;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _mainProvider.clearData();
      _mainProvider.getListGrup();
    });
  }

  void _onMenuProfilPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProfilPage(),
      ),
    );
  }

  void _onBuatGrupPressed(BuildContext buildContext) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BuatGrupPage(),
      ),
    ).then((returnValue) {
      _mainProvider.getListGrup();
    });
  }

  void _onGrupListPressed(BuildContext context, Grup grup) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GrupPage(
          grup: grup,
        ),
      ),
    ).then((returnValue) {
      _mainProvider.getListGrup();
    });
  }

  @override
  Widget build(BuildContext context) {
    _mainProvider = Provider.of<MainProvider>(context);

    return WillPopScope(
      onWillPop: () async {
        DateTime now = DateTime.now();

        if (_currentBackPressTime == null || now.difference(_currentBackPressTime) > Duration(seconds: 2)) {
          _currentBackPressTime = now;
          _keyScaffold.currentState.showSnackBar(
            SnackBar(
              content: Text(
                "Tekan kembali sekali lagi untuk keluar",
              ),
            ),
          );
          return false;
        }

        return true;
      },
      child: Scaffold(
        key: _keyScaffold,
        appBar: AppBar(
          title: Text("Bikers Touring Assistance"),
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.account_circle),
              onPressed: () => _onMenuProfilPressed(context),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          mini: true,
          tooltip: "Buat Grup Perjalanan",
          child: Icon(Icons.add),
          onPressed: () => _onBuatGrupPressed(context),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 4, left: 8),
                  child: Text(
                    "Daftar Grup Perjalanan",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 4, right: 8),
                  child: InkWell(
                    child: Icon(Icons.refresh),
                    onTap: () => _mainProvider.getListGrup(),
                  ),
                )
              ],
            ),
            Expanded(
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8),
                    child: ListView.builder(
                      itemCount: _mainProvider.listGrup.length,
                      itemBuilder: (BuildContext context, int i) {
                        return CardGrup(
                          title: _mainProvider.listGrup[i].namaGrup,
                          onTap: () => _onGrupListPressed(context, _mainProvider.listGrup[i]),
                          image: _mainProvider.fotoDisplayer(_mainProvider.listGrup[i].fotoGrup),
                        );
                      },
                    ),
                  ),
                  (_mainProvider.loadingScreenCenter) ? Center(child: CardLoading()) : Container(),
                  (((_mainProvider.listGrup.length <= 0) || (_mainProvider.listGrup == null)) && !_mainProvider.loadingScreenCenter)
                      ? Padding(
                          padding: EdgeInsets.all(16),
                          child: Center(
                            child: Text("Anda belum memiliki grup perjalanan."),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
