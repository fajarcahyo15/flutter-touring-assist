import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:new_touring_assist_flutter/helper/coordinates_converter.dart';
import 'package:new_touring_assist_flutter/helper/image_assets_helper.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/data_list_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/res_list_grup_member.dart';
import 'package:new_touring_assist_flutter/page/fase_perjalanan_aktif_page.dart';
import 'package:new_touring_assist_flutter/provider/asisten_perjalanan_provider.dart';
import 'package:new_touring_assist_flutter/widget/card_loading.dart';
import 'package:provider/provider.dart';

class AsistenPerjalananPage extends StatefulWidget {
  final String idGrup;

  const AsistenPerjalananPage({
    Key key,
    @required this.idGrup,
  }) : super(key: key);

  @override
  _AsistenPerjalananPageState createState() => _AsistenPerjalananPageState();
}

class _AsistenPerjalananPageState extends State<AsistenPerjalananPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();

  final Set<Marker> _markers = {};
  final Set<Polyline> _polylines = {};

  AsistenPerjalananProvider _asistenPerjalananProvider;

  GoogleMapController _googleMapController;

  BitmapDescriptor originMarkerIcon, destinationMarkerIcon, motorcycleMarkerIcon;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _asistenPerjalananProvider.clearData();
      _getGrupMemberByIdGrup();
    });

    ImageAssetsHelper.getBytesFromAsset('assets/images/origin.png', 64).then((icon) {
      setState(() {
        originMarkerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });

    ImageAssetsHelper.getBytesFromAsset('assets/images/destination.png', 64).then((icon) {
      setState(() {
        destinationMarkerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });

    ImageAssetsHelper.getBytesFromAsset('assets/images/motorcycle.png', 64).then((icon) {
      setState(() {
        motorcycleMarkerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });
  }

  void _onTampilLokasiAnggotaLainPressed() async {
    if (!_asistenPerjalananProvider.anggotaTampil) {
      ResListGrupMember resListGrupMember = await _asistenPerjalananProvider.getMemberActiveAssist(widget.idGrup);

      if ((resListGrupMember != null) && (resListGrupMember.data.length > 0)) {
        _asistenPerjalananProvider.anggotaTampil = true;
        _drawRoute(resListGrupMember.data.toList());
      } else {
        _keyScaffold.currentState.showSnackBar(
          SnackBar(
            content: Text("Tidak ada anggota yang dapat ditampilkan"),
          ),
        );
      }
    } else {
      _drawRoute(null);
      _asistenPerjalananProvider.anggotaTampil = false;
    }
  }

  void _getGrupMemberByIdGrup() async {
    _asistenPerjalananProvider.loadingScreen = true;
    bool success = await _asistenPerjalananProvider.getGrupMemberByIdGrup(widget.idGrup);
    _asistenPerjalananProvider.loadingScreen = false;

    if (!success) {
      _keyScaffold.currentState.showSnackBar(SnackBar(
        content: Text(
          _asistenPerjalananProvider.toastMessage,
        ),
      ));
    } else {
      _asistenPerjalananProvider.loadingScreen = true;
      bool success = await _asistenPerjalananProvider.getGrupByIdGrup(widget.idGrup);
      _asistenPerjalananProvider.loadingScreen = false;

      if (!success) {
        _keyScaffold.currentState.showSnackBar(SnackBar(
          content: Text(
            _asistenPerjalananProvider.toastMessage,
          ),
        ));
      }
    }
  }

  void _drawRoute(List<DataListGrupMember> listAnggota) async {
    _markers.clear();
    _polylines.clear();
    _asistenPerjalananProvider.grupRute = null;

    _asistenPerjalananProvider.loadingScreen = true;
    GrupRute grupRute = await _asistenPerjalananProvider.getFasePerjalananAktif(widget.idGrup);
    if (grupRute == null) {
      _onMyLocationPressed();
      _asistenPerjalananProvider.loadingScreen = false;
      return;
    }

    List<Placemark> origin = await Geolocator().placemarkFromAddress(grupRute.origin);
    List<Placemark> destination = await Geolocator().placemarkFromAddress(grupRute.destination);
    _asistenPerjalananProvider.loadingScreen = false;

    setState(() {
      _markers.add(
        Marker(
          markerId: MarkerId("origin"),
          position: LatLng(origin[0].position.latitude, origin[0].position.longitude),
          icon: originMarkerIcon,
          infoWindow: InfoWindow(
            title: "Titik Pemberangkatan",
            snippet: grupRute.origin,
          ),
        ),
      );

      _markers.add(
        Marker(
          markerId: MarkerId("destination"),
          position: LatLng(destination[0].position.latitude, destination[0].position.longitude),
          icon: destinationMarkerIcon,
          infoWindow: InfoWindow(
            title: "Titik Pemberhentian",
            snippet: grupRute.destination,
          ),
        ),
      );

      if (listAnggota != null) {
        for (var data in listAnggota) {
          _markers.add(
            Marker(
              markerId: MarkerId(data.grupMember.idGrupMember),
              position: LatLng(double.parse(data.grupMember.latitude), double.parse(data.grupMember.longitude)),
              icon: motorcycleMarkerIcon,
              infoWindow: InfoWindow(
                title: data.anggota.namaLengkap,
              ),
            ),
          );
        }
      }
    });

    List<PointLatLng> listPointLatLng = PolylinePoints().decodePolyline(grupRute.polyline);
    List<LatLng> listLatLng = CoordinatesConverter.listPointLatLngToListLatLng(listPointLatLng);

    setState(() {
      _polylines.add(
        Polyline(
          polylineId: PolylineId("polyline"),
          color: Colors.blueAccent,
          points: listLatLng,
          width: 4,
        ),
      );
    });

    _googleMapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(origin[0].position.latitude, origin[0].position.longitude),
          zoom: 12,
        ),
      ),
    );
  }

  void _onMapCreated(GoogleMapController googleMapController) async {
    setState(() => _googleMapController = googleMapController);
    _asistenPerjalananProvider.anggotaTampil = false;
    _drawRoute(null);
  }

  void _onInfoPressed() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FasePerjalananAktifPage(
          idGrup: widget.idGrup,
        ),
      ),
    ).then((_) {
      _asistenPerjalananProvider.anggotaTampil = false;
      _getGrupMemberByIdGrup();
      _drawRoute(null);
    });
  }

  void _onMyLocationPressed() async {
    var location = Location();
    LatLng myLocation;

    try {
      LocationData res = await location.getLocation();
      myLocation = LatLng(res.latitude, res.longitude);
    } on PlatformException catch (e) {
      var toastMessage = "Terjadi kesalahan sistem";
      if (e.code == "PERMISSION_DENIED") {
        toastMessage = "Izin untuk mengetahui lokasi tidak didapatkan";
      }
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text(
            toastMessage,
          ),
        ),
      );
      return;
    }

    _googleMapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: myLocation,
      zoom: 16,
    )));
  }

  void _onUpdateAssistPressed(bool assist) async {
    if (_asistenPerjalananProvider.grupRute != null) {
      GrupRute grupRute = GrupRute((b) => b
        ..idGrupRute = _asistenPerjalananProvider.grupRute.idGrupRute
        ..assistRute = assist);
      bool success = await _asistenPerjalananProvider.updateAssist(grupRute);
      if (success) {
        if (assist) {
          _asistenPerjalananProvider.startUpdateLocationService(widget.idGrup);
        } else {
          _asistenPerjalananProvider.stopUpdateLocationService();
        }

        _getGrupMemberByIdGrup();
      } else {
        _keyScaffold.currentState.showSnackBar(SnackBar(
          content: Text(_asistenPerjalananProvider.toastMessage),
        ));
      }
    } else {
      _keyScaffold.currentState.showSnackBar(SnackBar(
        content: Text("Tidak ada fase perjalanan yang aktif"),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    _asistenPerjalananProvider = Provider.of<AsistenPerjalananProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.symmetric(vertical: 6),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Asisten Perjalanan",
              ),
              Text(
                'Nama Grup Perjalanan',
                style: TextStyle(fontSize: 14),
              ),
            ],
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.info),
            onPressed: _onInfoPressed,
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              target: LatLng(-6.397430, 106.804530),
              zoom: 12,
            ),
            onMapCreated: _onMapCreated,
            markers: _markers,
            polylines: _polylines,
            myLocationEnabled: true,
          ),
          Positioned(
            top: 4,
            left: 4,
            right: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "FASE PERJALANAN AKTIF",
                                style: Theme.of(context).textTheme.title,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 4),
                                child: Text(
                                  _asistenPerjalananProvider.activeRouteOriDesString,
                                  style: Theme.of(context).textTheme.subtitle.copyWith(fontStyle: FontStyle.italic),
                                ),
                              ),
                            ],
                          ),
                        ),
                        (_asistenPerjalananProvider.grupMember.assistMember == true)
                            ? Tooltip(
                                message: "Non-aktifkan asisten perjalanan",
                                child: IconButton(
                                  icon: Icon(Icons.pause),
                                  onPressed: () => _onUpdateAssistPressed(false),
                                ),
                              )
                            : Tooltip(
                                message: "Aktifkan asisten perjalanan",
                                child: IconButton(
                                  icon: Icon(Icons.play_arrow),
                                  onPressed: () => _onUpdateAssistPressed(true),
                                ),
                              ),
                      ],
                    ),
                  ),
                ),
                (_asistenPerjalananProvider.haveActiveRoute)
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: RaisedButton(
                          color: Theme.of(context).primaryColor,
                          child: Text(
                            (_asistenPerjalananProvider.anggotaTampil) ? "Sembunyikan Lokasi Anggota Lain" : "Tampilkan Lokasi Anggota Lain",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          onPressed: _onTampilLokasiAnggotaLainPressed,
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
          Positioned(
            bottom: 4,
            right: 4,
            child: Card(
              color: Theme.of(context).primaryColor,
              child: Tooltip(
                message: "Ke Lokasi Saya",
                child: InkWell(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Icon(
                      Icons.my_location,
                      color: Theme.of(context).primaryIconTheme.color,
                    ),
                  ),
                  onTap: _onMyLocationPressed,
                ),
              ),
            ),
          ),
          (_asistenPerjalananProvider.loadingScreen)
              ? Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CardLoading(),
                    ],
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
