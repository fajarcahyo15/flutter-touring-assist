import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:new_touring_assist_flutter/helper/coordinates_converter.dart';
import 'package:new_touring_assist_flutter/helper/image_assets_helper.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/provider/map_fase_perjalanan_provider.dart';
import 'package:provider/provider.dart';

class MapFasePerjalananPage extends StatefulWidget {
  final String idGrup;

  const MapFasePerjalananPage({
    Key key,
    @required this.idGrup,
  }) : super(key: key);

  @override
  _MapFasePerjalananPageState createState() => _MapFasePerjalananPageState();
}

class _MapFasePerjalananPageState extends State<MapFasePerjalananPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();

  final Set<Marker> _markers = {};
  final Set<Polyline> _polylines = {};

  GoogleMapController _googleMapController;

  MapFasePerjalananProvider _mapFasePerjalananProvider;

  BitmapDescriptor originMarkerIcon, destinationMakrerIcon;

  @override
  void initState() {
    super.initState();

    ImageAssetsHelper.getBytesFromAsset('assets/images/origin.png', 64).then((icon) {
      setState(() {
        originMarkerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });

    ImageAssetsHelper.getBytesFromAsset('assets/images/destination.png', 64).then((icon) {
      setState(() {
        destinationMakrerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });
  }

  void _onMapCreated(GoogleMapController controller) async {
    _mapFasePerjalananProvider.loadingScreen = true;
    setState(() {
      _googleMapController = controller;
    });

    _markers.clear();
    _polylines.clear();

    // tampilkan rute dan marker
    List<GrupRute> listGrupRute = await _mapFasePerjalananProvider.getListGrupRute(widget.idGrup);

    if (listGrupRute != null) {
      int i = 0;
      LatLng titikKeberangkatan;

      listGrupRute.forEach((val) async {
        List<Placemark> origin;
        if (i == 0) {
          origin = await Geolocator().placemarkFromAddress(val.origin);
        }
        List<Placemark> destination = await Geolocator().placemarkFromAddress(val.destination);

        setState(() {
          if (i == 0) {
            titikKeberangkatan = LatLng(origin[0].position.latitude, origin[0].position.longitude);
            _markers.add(
              Marker(
                markerId: MarkerId("ormark-$i"),
                position: LatLng(origin[0].position.latitude, origin[0].position.longitude),
                icon: originMarkerIcon,
                infoWindow: InfoWindow(
                  title: "Titik Pemberangkatan",
                  snippet: val.origin,
                ),
              ),
            );
          }

          _markers.add(
            Marker(
              markerId: MarkerId("desmark-$i"),
              position: LatLng(destination[0].position.latitude, destination[0].position.longitude),
              icon: destinationMakrerIcon,
              infoWindow: InfoWindow(
                title: "Titik Pemberhentian",
                snippet: val.destination,
              ),
            ),
          );
        });

        // buat polyline
        List<PointLatLng> listPointLatLng = PolylinePoints().decodePolyline(val.polyline);
        List<LatLng> listLatLng = CoordinatesConverter.listPointLatLngToListLatLng(listPointLatLng);

        setState(() {
          _polylines.add(
            Polyline(
              polylineId: PolylineId("poly-$i"),
              color: Colors.blueAccent,
              points: listLatLng,
              width: 4,
            ),
          );
        });

        _googleMapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(target: titikKeberangkatan, zoom: 12),
          ),
        );

        i++;
        if (i == listGrupRute.length) _mapFasePerjalananProvider.loadingScreen = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _mapFasePerjalananProvider = Provider.of<MapFasePerjalananProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text("Map Fase Perjalanan"),
      ),
      body: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              target: LatLng(-6.9032739, 107.573117),
              zoom: 12,
            ),
            myLocationEnabled: true,
            onMapCreated: _onMapCreated,
            markers: _markers,
            polylines: _polylines,
          ),
          (_mapFasePerjalananProvider.loadingScreen)
              ? Center(
                  child: Card(
                    elevation: 10,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SizedBox(
                            width: 25,
                            height: 25,
                            child: CircularProgressIndicator(),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 8),
                            child: Text("Memuat Fase Perjalanan..."),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
