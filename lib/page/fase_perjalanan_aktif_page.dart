import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/data_list_grup_member.dart';
import 'package:new_touring_assist_flutter/provider/fase_perjalanan_aktif_provider.dart';
import 'package:new_touring_assist_flutter/widget/card_fase_aktif.dart';
import 'package:new_touring_assist_flutter/widget/card_loading.dart';
import 'package:provider/provider.dart';

import 'form_fase_perjalanan/lihat_fase_perjalanan_page.dart';

class FasePerjalananAktifPage extends StatefulWidget {
  final String idGrup;

  const FasePerjalananAktifPage({
    Key key,
    @required this.idGrup,
  }) : super(key: key);

  @override
  _FasePerjalananAktifPageState createState() => _FasePerjalananAktifPageState();
}

class _FasePerjalananAktifPageState extends State<FasePerjalananAktifPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();

  FasePerjalananAktifProvider _fasePerjalananAktifProvider;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _fasePerjalananAktifProvider.clearData();
      _getListGrupRute();
    });
  }

  void _onItemPressed(BuildContext context, int idGrupRute) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LihatFasePerjalananPage(
          idGrupRute: idGrupRute,
        ),
      ),
    ).then((_) {
      _getListGrupRute();
    });
  }

  void _onFaseActivate(GrupRute grupRute) async {
    GrupRute gr = GrupRute((b) => b
      ..idGrupRute = grupRute.idGrupRute
      ..assistRute = true);

    bool success = await _fasePerjalananAktifProvider.updateAssist(gr);

    if (!success) {
      _keyScaffold.currentState.showSnackBar(
        (SnackBar(
          content: Text(_fasePerjalananAktifProvider.toastMessage),
        )),
      );
    } else {
      _getListGrupRute();
      _getListDataGrupMember();
    }
  }

  void _onFaseDeactivate(GrupRute grupRute) async {
    GrupRute gr = GrupRute((b) => b
      ..idGrupRute = grupRute.idGrupRute
      ..assistRute = false);

    bool success = await _fasePerjalananAktifProvider.updateAssist(gr);

    if (!success) {
      _keyScaffold.currentState.showSnackBar(
        (SnackBar(
          content: Text(_fasePerjalananAktifProvider.toastMessage),
        )),
      );
    } else {
      _getListGrupRute();
    }
  }

  void _getListGrupRute() async {
    bool have = await _fasePerjalananAktifProvider.getListGrupRute(widget.idGrup);
    if (have != true) {
      _keyScaffold.currentState.showSnackBar(
        (SnackBar(
          content: Text(_fasePerjalananAktifProvider.toastMessage),
        )),
      );
    }
  }

  void _getListDataGrupMember() async {
    bool have = await _fasePerjalananAktifProvider.getListGrupMemberActive(widget.idGrup);
    if (have != true) {
      _keyScaffold.currentState.showSnackBar(
        (SnackBar(
          content: Text(_fasePerjalananAktifProvider.toastMessage),
        )),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    _fasePerjalananAktifProvider = Provider.of<FasePerjalananAktifProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.symmetric(vertical: 6),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Info Asisten Perjalanan",
              ),
              Text(
                'Daftar Fase Perjalanan',
                style: TextStyle(fontSize: 14),
              ),
            ],
          ),
        ),
      ),
      body: PageView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(4),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 4),
                        child: Text("Fase Perjalanan Aktif"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 4),
                      child: InkWell(
                        child: Icon(Icons.refresh),
                        onTap: _getListGrupRute,
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: (_fasePerjalananAktifProvider.listGrupRute.length > 0)
                      ? ListView.builder(
                          itemCount: _fasePerjalananAktifProvider.listGrupRute.length,
                          itemBuilder: (context, i) {
                            GrupRute gr = _fasePerjalananAktifProvider.listGrupRute[i];
                            return CardFaseAktif(
                              title: "Fase Perjalanan ${i + 1}",
                              detail: "${gr.origin} -> ${gr.destination}",
                              active: gr.assistRute,
                              onTap: () => _onItemPressed(context, gr.idGrupRute),
                              onActivate: () => _onFaseActivate(gr),
                              onDeactivate: () => _onFaseDeactivate(gr),
                            );
                          },
                        )
                      : (_fasePerjalananAktifProvider.loadingScreen)
                          ? Center(
                              child: CardLoading(),
                            )
                          : Center(
                              child: Text("Tidak ada fase perjalanan"),
                            ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(4),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 4),
                        child: Text("Anggota Perjalanan Aktif"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 4),
                      child: InkWell(
                        child: Icon(Icons.refresh),
                        onTap: _getListDataGrupMember,
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: (_fasePerjalananAktifProvider.listDataGrupMember.length > 0)
                      ? ListView.builder(
                          itemCount: _fasePerjalananAktifProvider.listDataGrupMember.length,
                          itemBuilder: (context, i) {
                            DataListGrupMember dataListGrupMember = _fasePerjalananAktifProvider.listDataGrupMember[i];
                            return CardFaseAktif(
                              title: dataListGrupMember.anggota.namaLengkap,
                              detail: dataListGrupMember.anggota.idAnggota,
                              active: true,
                              onTap: () {},
                              onActivate: () {},
                              onDeactivate: () {},
                            );
                          },
                        )
                      : (_fasePerjalananAktifProvider.loadingScreen)
                          ? Center(
                              child: CardLoading(),
                            )
                          : Center(
                              child: Text("Tidak ada anggota yang mengaktifkan asisten"),
                            ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
