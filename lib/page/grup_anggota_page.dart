import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/page/grup_tambah_anggota_page.dart';
import 'package:new_touring_assist_flutter/provider/grup_anggota_provider.dart';
import 'package:new_touring_assist_flutter/widget/card_anggota.dart';
import 'package:new_touring_assist_flutter/widget/card_loading.dart';
import 'package:provider/provider.dart';

class GrupAnggotaPage extends StatefulWidget {
  final Grup grup;

  const GrupAnggotaPage({
    Key key,
    @required this.grup,
  }) : super(key: key);

  @override
  _GrupAnggotaPageState createState() => _GrupAnggotaPageState();
}

class _GrupAnggotaPageState extends State<GrupAnggotaPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();

  GrupAnggotaProvider _grupAnggotaProvider;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _grupAnggotaProvider.clearData();
      _grupAnggotaProvider.getListAnggotaGrup(widget.grup.idGrup);
    });
  }

  void _onFabTambahAnggotaPressed() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GrupTambahAnggotaPage(
          grup: widget.grup,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _grupAnggotaProvider = Provider.of<GrupAnggotaProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.symmetric(vertical: 6),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                widget.grup.namaGrup,
              ),
              Text(
                'Anggota',
                style: TextStyle(fontSize: 14),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        mini: true,
        child: Icon(Icons.add),
        tooltip: "Tambah Anggota Baru",
        onPressed: _onFabTambahAnggotaPressed,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 4, left: 8),
                child: Text(
                  "Daftar Anggota Grup Perjalanan",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 4, right: 8),
                child: InkWell(
                  child: Icon(Icons.refresh),
                  onTap: () => _grupAnggotaProvider.getListAnggotaGrup(widget.grup.idGrup),
                ),
              )
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: _displayContent(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _displayContent() {
    if (_grupAnggotaProvider.listAnggotaGrup.length > 0) {
      return Stack(
        children: <Widget>[
          ListView.separated(
            itemCount: _grupAnggotaProvider.listAnggotaGrup.length,
            separatorBuilder: (context, i) => Divider(thickness: 2),
            itemBuilder: (context, i) {
              return CardAnggota(
                title: _grupAnggotaProvider.listAnggotaGrup[i].namaLengkap,
                noHp: "Nomor HP: ${(_grupAnggotaProvider.listAnggotaGrup[i].noHp != null) ? _grupAnggotaProvider.listAnggotaGrup[i].noHp : '-'}",
                status: "Koordinator",
                onTap: () {},
              );
            },
          ),
          (_grupAnggotaProvider.reqAnggota)
              ? Center(
                  child: CardLoading(),
                )
              : Container(),
        ],
      );
    } else {
      if (_grupAnggotaProvider.reqAnggota) {
        return Center(
          child: CardLoading(),
        );
      } else {
        return Center(
          child: Text(_grupAnggotaProvider.centerScreenMessage),
        );
      }
    }
  }
}
