import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:new_touring_assist_flutter/helper/form_validation.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/provider/buat_grup_provider.dart';
import 'package:new_touring_assist_flutter/widget/button_circular.dart';
import 'package:new_touring_assist_flutter/widget/indicator_loading_button.dart';
import 'package:new_touring_assist_flutter/widget/tff_bordered.dart';
import 'package:provider/provider.dart';

class BuatGrupPage extends StatefulWidget {
  @override
  _BuatGrupPageState createState() => _BuatGrupPageState();
}

class _BuatGrupPageState extends State<BuatGrupPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormBuatGrup = GlobalKey<FormState>();

  final _txtNamaGrupPerjalanan = TextEditingController();
  final _txtTglBerangkat = TextEditingController();
  final _txtJamBerangkat = TextEditingController();
  final _txtJarakMaksimal = TextEditingController();
  final _txtIntervalLokasi = TextEditingController();
  final _txtDeskripsi = TextEditingController();

  BuatGrupProvider _buatGrupProvider;

  bool _autoValidate = false;

  @override
  void dispose() {
    _txtNamaGrupPerjalanan.dispose();
    _txtTglBerangkat.dispose();
    _txtJamBerangkat.dispose();
    _txtJarakMaksimal.dispose();
    _txtIntervalLokasi.dispose();
    _txtDeskripsi.dispose();
    super.dispose();
  }

  void _onBtnSimpanPressed(BuildContext context) async {
    if (_keyFormBuatGrup.currentState.validate()) {
      Grup grup = Grup((b) => b
        ..namaGrup = _txtNamaGrupPerjalanan.text.trim()
        ..tglBerangkat = "${_txtTglBerangkat.text.trim()} ${_txtJamBerangkat.text.trim()}:00"
        ..jarakMaksimal = _txtJarakMaksimal.text.trim()
        ..intervalLokasi = _txtIntervalLokasi.text.trim()
        ..deskripsi = _txtDeskripsi.text.trim());

      bool success = await _buatGrupProvider.createGrup(grup);

      if (success) {
        Navigator.pop(context);
      } else {
        _keyScaffold.currentState.showSnackBar(
          SnackBar(
            content: Text(_buatGrupProvider.toastMessage),
          ),
        );
      }
    }

    setState(() {
      _autoValidate = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    _buatGrupProvider = Provider.of<BuatGrupProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text("Buat Grup Perjalanan"),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(32),
            child: Form(
              key: _keyFormBuatGrup,
              autovalidate: _autoValidate,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  TFFBordered(
                    controller: _txtNamaGrupPerjalanan,
                    labelText: "Nama Grup Perjalanan",
                    validator: (value) {
                      return FormValidation.textRequired(value: value, label: "Nama Grup Perjalanan");
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: TextFormField(
                      controller: _txtTglBerangkat,
                      readOnly: true,
                      decoration: InputDecoration(
                        labelText: "Tanggal Berangkat",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.date_range),
                          onPressed: () {
                            DatePicker.showDatePicker(
                              context,
                              showTitleActions: true,
                              minTime: DateTime.now(),
                              maxTime: DateTime.now().add(Duration(days: 365)),
                              onConfirm: (dateTime) {
                                setState(() {
                                  _txtTglBerangkat.text = formatDate(dateTime, [yyyy, '-', mm, '-', dd]);
                                });
                              },
                              currentTime: DateTime.now(),
                              locale: LocaleType.id,
                            );
                          },
                        ),
                      ),
                      validator: (value) {
                        return FormValidation.textRequired(
                          value: value,
                          label: "Tanggal Berangkat",
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: TextFormField(
                      controller: _txtJamBerangkat,
                      readOnly: true,
                      decoration: InputDecoration(
                        labelText: "Jam Berangkat",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.timer),
                          onPressed: () {
                            DatePicker.showTimePicker(
                              context,
                              showTitleActions: true,
                              onConfirm: (dateTime) {
                                setState(() {
                                  _txtJamBerangkat.text = formatDate(dateTime, [HH, ':', nn]);
                                });
                              },
                              locale: LocaleType.id,
                            );
                          },
                        ),
                      ),
                      validator: (value) {
                        return FormValidation.textRequired(
                          value: value,
                          label: "Jam Berangkat",
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: TextFormField(
                      controller: _txtJarakMaksimal,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: "Jarak Maksimal",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        suffixText: "Meter",
                      ),
                      validator: (value) {
                        return FormValidation.numberBetween(
                          value: value,
                          min: 1000,
                          max: 10000,
                          label: "Jarak Maksimal",
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: TextFormField(
                      controller: _txtIntervalLokasi,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: "Interval Kirim Lokasi",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        suffixText: "Menit",
                      ),
                      validator: (value) {
                        return FormValidation.numberBetween(
                          value: value,
                          min: 1,
                          max: 10,
                          label: "Interval Kirim Lokasi",
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: TFFBordered(
                      controller: _txtDeskripsi,
                      labelText: "Deskripsi",
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                    ),
                  ),
                  (!_buatGrupProvider.loadingButton)
                      ? Padding(
                          padding: EdgeInsets.only(top: 8),
                          child: ButtonCircular(
                            text: "Simpan",
                            onPressed: () {
                              _onBtnSimpanPressed(context);
                            },
                          ),
                        )
                      : Padding(
                          padding: EdgeInsets.only(top: 8),
                          child: IndicatorLoadingButton(
                            loadingText: "Menyimpan data...",
                          ),
                        ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
