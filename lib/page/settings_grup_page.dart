// import 'package:date_format/date_format.dart';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:new_touring_assist_flutter/helper/form_validation.dart';
import 'package:new_touring_assist_flutter/helper/string_date_formatter.dart';
import 'package:new_touring_assist_flutter/helper/time_converter.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/page/fase_perjalanan_page.dart';
import 'package:new_touring_assist_flutter/page/grup_anggota_page.dart';
import 'package:new_touring_assist_flutter/page/main_page.dart';
import 'package:new_touring_assist_flutter/provider/settings_grup_provider.dart';
import 'package:new_touring_assist_flutter/widget/alert.dart';
import 'package:new_touring_assist_flutter/widget/button_circular.dart';
import 'package:new_touring_assist_flutter/widget/card_menu.dart';
import 'package:new_touring_assist_flutter/widget/item_settings_in_card_with_button.dart';
import 'package:provider/provider.dart';

class SettingsGrupPage extends StatefulWidget {
  final Grup grup;

  const SettingsGrupPage({
    Key key,
    @required this.grup,
  }) : super(key: key);

  @override
  _SettingsGrupPageState createState() => _SettingsGrupPageState();
}

class _SettingsGrupPageState extends State<SettingsGrupPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormJarakMaksimal = GlobalKey<FormState>();
  final _keyFormIntervalUpdate = GlobalKey<FormState>();

  final _txtJarakMaksimal = TextEditingController();
  final _txtIntervalUpdate = TextEditingController();

  SettingsGrupProvider _settingsGrupProvider;

  AssetImage _dummyGrupImage = AssetImage("assets/images/background-login.jpg");

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _settingsGrupProvider.clearData();
      _settingsGrupProvider.getInfoGrup(widget.grup.idGrup);
    });
  }

  @override
  void dispose() {
    _txtJarakMaksimal.dispose();
    _txtIntervalUpdate.dispose();
    super.dispose();
  }

  void _onGantiFotoPressed() {
    Alert.pickImage(
      context: context,
      onFromCamera: () async {
        Navigator.pop(context);
        File foto = await ImagePicker.pickImage(source: ImageSource.camera);
        if (foto != null) {
          Alert.loading(context: context);
          _settingsGrupProvider.updateFoto(widget.grup.idGrup, foto).then((success) {
            Navigator.pop(context);
            if (!success) {
              Alert.inform(context: context, content: _settingsGrupProvider.toastMessage);
            }
          });
        }
      },
      onFromGallery: () async {
        Navigator.pop(context);
        File foto = await ImagePicker.pickImage(source: ImageSource.gallery);
        if (foto != null) {
          Alert.loading(context: context);
          _settingsGrupProvider.updateFoto(widget.grup.idGrup, foto).then((success) {
            Navigator.pop(context);
            if (!success) {
              Alert.inform(context: context, content: _settingsGrupProvider.toastMessage);
            }
          });
        }
      },
    );
  }

  void _onGantiTglBerangkatPressed(BuildContext context) async {
    DateTime initialDate = DateTime.now().add(Duration(minutes: 1));
    if (_settingsGrupProvider.grup.tglBerangkat != null) {
      if (initialDate.isBefore(DateTime.parse(_settingsGrupProvider.grup.tglBerangkat))) {
        initialDate = DateTime.parse(_settingsGrupProvider.grup.tglBerangkat);
      }
    }

    var tglBerangkat = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(Duration(days: 365)),
      locale: Locale('id'),
    );

    if (tglBerangkat != null) {
      Grup grupUpdate = _settingsGrupProvider.grup;
      var tgl = StringDateFormatter.format(from: DateFormatEnum.yyyyMMddHHmm, to: DateFormatEnum.yyyyMMdd, date: tglBerangkat.toString());
      tgl += " ${_settingsGrupProvider.tampilanJamBerangkat(_settingsGrupProvider.grup.tglBerangkat)}";
      grupUpdate = grupUpdate.rebuild((b) => b..tglBerangkat = tgl);
      _settingsGrupProvider.updateInfo(grupUpdate).then((success) {
        if (!success) {
          _keyScaffold.currentState.showSnackBar(
            SnackBar(
              content: Text(_settingsGrupProvider.toastMessage),
            ),
          );
        }
      });
    }
  }

  void _onGantiJamBerangkatPressed(BuildContext context) async {
    var jamBerangkat = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (jamBerangkat != null) {
      Grup grupUpdate = _settingsGrupProvider.grup;
      String tgl = StringDateFormatter.format(from: DateFormatEnum.yyyyMMddHHmm, to: DateFormatEnum.yyyyMMdd, date: grupUpdate.tglBerangkat);
      String jam = (jamBerangkat.hour < 10) ? "0${jamBerangkat.hour}" : jamBerangkat.hour.toString();
      String menit = (jamBerangkat.minute < 10) ? "0${jamBerangkat.minute}" : jamBerangkat.minute.toString();
      tgl += " $jam:$menit";
      grupUpdate = grupUpdate.rebuild((b) => b..tglBerangkat = tgl);
      _settingsGrupProvider.updateInfo(grupUpdate).then((success) {
        if (!success) {
          _keyScaffold.currentState.showSnackBar(
            SnackBar(
              content: Text(_settingsGrupProvider.toastMessage),
            ),
          );
        }
      });
    }
  }

  void _onGantiJarakMaksimal(BuildContext context) {
    _txtJarakMaksimal.text = _settingsGrupProvider.grup.jarakMaksimal.toString();

    _keyScaffold.currentState.showBottomSheet((context) {
      return Form(
        key: _keyFormJarakMaksimal,
        autovalidate: true,
        child: Container(
          padding: EdgeInsets.all(8),
          height: 180,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text("Atur jarak maksimal antara anggota terdepan dengan anggota paling belakang"),
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: TextFormField(
                  controller: _txtJarakMaksimal,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: "Jarak Maksimal",
                    contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    suffixText: "Meter",
                  ),
                  validator: (value) {
                    return FormValidation.numberBetween(
                      value: value,
                      min: 1000,
                      max: 10000,
                      label: "Jarak Maksimal",
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: ButtonCircular(
                  text: "Ganti",
                  onPressed: () {
                    if (_keyFormJarakMaksimal.currentState.validate()) {
                      Grup grupUpdate = _settingsGrupProvider.grup;
                      grupUpdate = grupUpdate.rebuild((b) => b..jarakMaksimal = _txtJarakMaksimal.text.trim());
                      _settingsGrupProvider.updateInfo(grupUpdate).then((success) {
                        if (!success) {
                          _keyScaffold.currentState.showSnackBar(
                            SnackBar(
                              content: Text(_settingsGrupProvider.toastMessage),
                            ),
                          );
                        } else {
                          Navigator.pop(context);
                        }
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  void _onGantiIntervalUpdateLokasi(BuildContext context) {
    _txtIntervalUpdate.text = TimeConverter.secondsToMinutes(int.parse(_settingsGrupProvider.grup.intervalLokasi)).toString();

    _keyScaffold.currentState.showBottomSheet((context) {
      return Form(
        key: _keyFormIntervalUpdate,
        autovalidate: true,
        child: Container(
          padding: EdgeInsets.all(8),
          height: 180,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text("Atur jangka waktu pengiriman lokasi bagi setiap anggota"),
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: TextFormField(
                  controller: _txtIntervalUpdate,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: "Interval Update Lokasi",
                    contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    suffixText: "Menit",
                  ),
                  validator: (value) {
                    return FormValidation.numberBetween(
                      value: value,
                      min: 1,
                      max: 10,
                      label: "Interval Update Lokasi",
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: ButtonCircular(
                  text: "Ganti",
                  onPressed: () {
                    if (_keyFormIntervalUpdate.currentState.validate()) {
                      Grup grupUpdate = _settingsGrupProvider.grup;
                      String time = TimeConverter.minutesToSeconds(int.parse(_txtIntervalUpdate.text.trim())).toString();
                      grupUpdate = grupUpdate.rebuild((b) => b..intervalLokasi = time);
                      _settingsGrupProvider.updateInfo(grupUpdate).then(
                        (success) {
                          if (!success) {
                            _keyScaffold.currentState.showSnackBar(
                              SnackBar(
                                content: Text(_settingsGrupProvider.toastMessage),
                              ),
                            );
                          } else {
                            Navigator.pop(context);
                          }
                        },
                      );
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  void _onGantiFasePerjalananPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FasePerjalananPage(
          idGrup: widget.grup.idGrup,
        ),
      ),
    ).then((_) {
      _settingsGrupProvider.getInfoGrup(widget.grup.idGrup);
    });
  }

  void _onBtnKeluarGrupPressed() {
    Alert.confirm(
      context: context,
      content: "Apakah Anda yakin ingin keluar dari grup ini?",
      onYes: () async {
        Navigator.pop(context);
        Alert.loading(context: context);
        bool success = await _settingsGrupProvider.keluarGrup(widget.grup.idGrup);
        Navigator.pop(context);

        if (success) {
          Alert.inform(
              context: context,
              content: "Anda telah keluar dari grup",
              onYes: () {
                Navigator.pop(context);
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MainPage(),
                  ),
                );
              });
        } else {
          Alert.inform(
            context: context,
            content: _settingsGrupProvider.toastMessage,
          );
        }
      },
    );
  }

  void _onBtnHapusGrupPressed() {
    Alert.confirm(
      context: context,
      content: "Apakah Anda yakin ingin menghapus grup ini?",
      onYes: () async {
        Navigator.pop(context);
        Alert.loading(context: context);
        bool success = await _settingsGrupProvider.deleteGrup(widget.grup.idGrup);
        Navigator.pop(context);

        if (success) {
          Alert.inform(
              context: context,
              content: "Grup telah berhasil dihapus",
              onYes: () {
                Navigator.pop(context);
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MainPage(),
                  ),
                );
              });
        } else {
          Alert.inform(
            context: context,
            content: _settingsGrupProvider.toastMessage,
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _settingsGrupProvider = Provider.of<SettingsGrupProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.symmetric(vertical: 6),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Pengaturan",
              ),
              Text(
                _settingsGrupProvider.grup.namaGrup ?? '-',
                style: TextStyle(fontSize: 14),
              ),
            ],
          ),
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(4, 4, 4, 0),
              child: Card(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          CircleAvatar(
                            backgroundImage: (_settingsGrupProvider.fotoBytes != null) ? MemoryImage(_settingsGrupProvider.fotoBytes) : _dummyGrupImage,
                          ),
                          Positioned(
                            right: 0,
                            bottom: 0,
                            child: SizedBox(
                              width: 15.0,
                              height: 15.0,
                              child: FloatingActionButton(
                                elevation: 2.0,
                                child: Icon(
                                  Icons.camera_alt,
                                  size: 10.0,
                                ),
                                onPressed: _onGantiFotoPressed,
                              ),
                            ),
                          )
                        ],
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                _settingsGrupProvider.grup.namaGrup ?? "-",
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(_settingsGrupProvider.grup.deskripsi ?? "-"),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4, 4, 4, 0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                      child: Text(
                        "INFORMASI",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Divider(
                      color: Colors.black,
                      height: 0,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ItemSettingsInCardWithButton(
                            title: "Tanggal Berangkat",
                            detail: _settingsGrupProvider.tampilanTglBerangkat(_settingsGrupProvider.grup.tglBerangkat),
                            onTap: () => _onGantiTglBerangkatPressed(context),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4, bottom: 4),
                            child: Divider(
                              color: Colors.black,
                              height: 0,
                            ),
                          ),
                          ItemSettingsInCardWithButton(
                            title: "Jam Keberangkatan",
                            detail: _settingsGrupProvider.tampilanJamBerangkat(_settingsGrupProvider.grup.tglBerangkat),
                            onTap: () => _onGantiJamBerangkatPressed(context),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4, 4, 4, 0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                      child: Text(
                        "ASISTEN PERJALANAN",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Divider(
                      color: Colors.black,
                      height: 0,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ItemSettingsInCardWithButton(
                            title: "Jarak Maksimal",
                            detail: "${_settingsGrupProvider.grup.jarakMaksimal ?? '-'} Meter",
                            onTap: () => _onGantiJarakMaksimal(context),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4, bottom: 4),
                            child: Divider(
                              color: Colors.black,
                              height: 0,
                            ),
                          ),
                          ItemSettingsInCardWithButton(
                            title: "Interval Update Lokasi",
                            detail: "${TimeConverter.secondsToMinutes(int.parse(_settingsGrupProvider.grup.intervalLokasi ?? "0"))} Menit",
                            onTap: () => _onGantiIntervalUpdateLokasi(context),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4, bottom: 4),
                            child: Divider(
                              color: Colors.black,
                              height: 0,
                            ),
                          ),
                          ItemSettingsInCardWithButton(
                            title: "Tujuan Perjalanan",
                            detail: _settingsGrupProvider.oriDes,
                            onTap: () => _onGantiFasePerjalananPressed(context),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4, 4, 4, 0),
              child: CardMenu(
                title: "Anggota Grup",
                icon: Icon(Icons.people),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => GrupAnggotaPage(
                        grup: widget.grup,
                      ),
                    ),
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4, 4, 4, 0),
              child: CardMenu(
                title: "Keluar Grup",
                icon: Icon(Icons.backspace),
                onTap: _onBtnKeluarGrupPressed,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4, 4, 4, 0),
              child: CardMenu(
                title: "Hapus Grup",
                icon: Icon(Icons.delete),
                onTap: _onBtnHapusGrupPressed,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
