import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:new_touring_assist_flutter/helper/coordinates_converter.dart';
import 'package:new_touring_assist_flutter/helper/form_validation.dart';
import 'package:new_touring_assist_flutter/helper/image_assets_helper.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/direction/direction.dart';
import 'package:new_touring_assist_flutter/provider/form_fase_perjalanan/tambah_fase_perjalanan_provider.dart';
import 'package:new_touring_assist_flutter/widget/button_circular.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;

class TambahFasePerjalananPage extends StatefulWidget {
  final String idGrup;

  const TambahFasePerjalananPage({
    Key key,
    @required this.idGrup,
  }) : super(key: key);

  @override
  _TambahFasePerjalananPageState createState() => _TambahFasePerjalananPageState();
}

class _TambahFasePerjalananPageState extends State<TambahFasePerjalananPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormRoute = GlobalKey<FormState>();

  final Set<Marker> _markers = {};
  final Set<Polyline> _polylines = {};

  final _txtDari = TextEditingController();
  final _txtTujuan = TextEditingController();

  TambahFasePerjalananProvider _tambahFasePerjalananProvider;
  GoogleMapController _googleMapController;

  bool _autoValidate = false;
  bool _txtDariEnabled = true;

  LatLng _latLngFrom;
  LatLng _latLngDestination;

  BitmapDescriptor originMarkerIcon, destinationMakrerIcon;

  GrupRute _grupRute;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _tambahFasePerjalananProvider.clearData();
      _setDefaultDari();
    });

    ImageAssetsHelper.getBytesFromAsset('assets/images/origin.png', 64).then((icon) {
      setState(() {
        originMarkerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });

    ImageAssetsHelper.getBytesFromAsset('assets/images/destination.png', 64).then((icon) {
      setState(() {
        destinationMakrerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });
  }

  @override
  void dispose() {
    _txtDari.dispose();
    _txtTujuan.dispose();
    super.dispose();
  }

  void _setDefaultDari() async {
    GrupRute grupRute = await _tambahFasePerjalananProvider.getRuteTerakhir(widget.idGrup);

    if (grupRute != null) {
      _txtDari.text = grupRute.destination;
      setState(() => _txtDariEnabled = false);
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _googleMapController = controller;
    });

    _onMyLocationPressed();
  }

  void _onLihatRutePressed(BuildContext context) async {
    _tambahFasePerjalananProvider.loadingBtnLihatRute = true;
    setState(() {
      _autoValidate = true;
    });

    if (!_keyFormRoute.currentState.validate()) {
      return;
    }

    String from = _txtDari.text.trim();
    String destination = _txtTujuan.text.trim();

    Direction direction = await _tambahFasePerjalananProvider.getRoute(from, destination, widget.idGrup);
    if (direction == null) {
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text(_tambahFasePerjalananProvider.toastMessage),
        ),
      );
      _tambahFasePerjalananProvider.loadingBtnLihatRute = false;
      return;
    }

    int tsWaktuBerangkat = await _tambahFasePerjalananProvider.getTimestampTglBerangkat(widget.idGrup);
    if (tsWaktuBerangkat == null) {
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text(_tambahFasePerjalananProvider.toastMessage),
        ),
      );
      _tambahFasePerjalananProvider.loadingBtnLihatRute = false;
      return;
    }

    // mendapatkan rute yang direkomendasikan
    await _tambahFasePerjalananProvider.getListDetailRute(direction.routes.toList(), tsWaktuBerangkat);
    List<Map<String, dynamic>> listDetailRute = _tambahFasePerjalananProvider.listDetailRute;
    int indexRuteRekomendasi;
    for (var i = 0; i < listDetailRute.length; i++) {
      if (listDetailRute[i]['rekomendasi']) {
        indexRuteRekomendasi = i;
        break;
      }
    }

    if (indexRuteRekomendasi == null) {
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text(_tambahFasePerjalananProvider.toastMessage),
        ),
      );
      _tambahFasePerjalananProvider.loadingBtnLihatRute = false;
      return;
    }

    _tambahFasePerjalananProvider.currentRouteIndex = indexRuteRekomendasi;

    // tampilkan rute yang paling direkomendasikan
    await _drawRouteToMap(from, destination);

    _tambahFasePerjalananProvider.loadingBtnLihatRute = false;
  }

  void _onSimpanRutePressed() async {
    _tambahFasePerjalananProvider.loadingBtnLihatRute = true;

    bool success = await _tambahFasePerjalananProvider.create(_grupRute);
    if (!success) {
      _keyScaffold.currentState.showSnackBar(
        SnackBar(
          content: Text(_tambahFasePerjalananProvider.toastMessage),
        ),
      );
      _tambahFasePerjalananProvider.loadingBtnLihatRute = false;
      return;
    }

    _tambahFasePerjalananProvider.loadingBtnLihatRute = false;
    Navigator.pop(context);
  }

  void _onMyLocationPressed() async {
    LatLng myLocation = await _tambahFasePerjalananProvider.getCurrentLocation();

    if (myLocation == null) {
      _keyScaffold.currentState.showSnackBar(SnackBar(
        content: Text(_tambahFasePerjalananProvider.toastMessage),
      ));
      return;
    }

    _googleMapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: myLocation,
      zoom: 16,
    )));
  }

  Future _drawRouteToMap(String from, String destination) async {
    int indexRoute = _tambahFasePerjalananProvider.currentRouteIndex;
    var route = _tambahFasePerjalananProvider.listDetailRute[indexRoute]['route'];
    List<Map<String, dynamic>> listWeatherMarkerData = List();

    List<PointLatLng> listPointLatLng = PolylinePoints().decodePolyline(route.overviewPolyline.points);
    List<LatLng> listLatLng = CoordinatesConverter.listPointLatLngToListLatLng(listPointLatLng);

    // setting data untuk marker cuaca
    for (var weather in _tambahFasePerjalananProvider.listDetailRute[indexRoute]['listWeather']) {
      var uuid = Uuid();
      var iconWeatherRes = await http.get("http://openweathermap.org/img/wn/${weather['weather'].icon}@2x.png");
      listWeatherMarkerData.add({
        'markerId': uuid.v4(),
        'icon': BitmapDescriptor.fromBytes(iconWeatherRes.bodyBytes),
        'position': weather['latlng'],
        'title': weather['weather'].main,
        'snippet': weather['weather'].description,
      });
    }

    setState(() {
      _markers.clear();
      _polylines.clear();

      _latLngFrom = LatLng(route.legs[0].startLocation.lat, route.legs[0].startLocation.lng);
      _latLngDestination = LatLng(route.legs[0].endLocation.lat, route.legs[0].endLocation.lng);

      _markers.add(
        Marker(
          markerId: MarkerId("From Marker"),
          position: _latLngFrom,
          icon: originMarkerIcon,
          infoWindow: InfoWindow(
            title: "Titik Keberangkatan",
            snippet: from,
          ),
        ),
      );

      _markers.add(
        Marker(
          markerId: MarkerId("Destination Marker"),
          position: _latLngDestination,
          icon: originMarkerIcon,
          infoWindow: InfoWindow(
            title: "Tujuan",
            snippet: destination,
          ),
        ),
      );

      // menampilkan marker kondisi cuaca
      for (var weather in listWeatherMarkerData) {
        _markers.add(
          Marker(
            markerId: MarkerId(weather['markerId']),
            position: weather['position'],
            icon: weather['icon'],
            infoWindow: InfoWindow(
              title: weather['title'],
              snippet: weather['snippet'],
            ),
          ),
        );
      }

      _polylines.add(
        Polyline(
          polylineId: PolylineId("polyId"),
          color: Colors.blueAccent,
          points: listLatLng,
          width: 4,
        ),
      );

      _grupRute = GrupRute(
        (b) => b
          ..origin = from
          ..destination = destination
          ..polyline = route.overviewPolyline.points
          ..waktuTempuh = route.legs[0].duration.value
          ..jarak = route.legs[0].distance.value
          ..idGrup = widget.idGrup,
      );
    });

    _googleMapController.animateCamera(
      CameraUpdate.newLatLngBounds(
        LatLngBounds(
          northeast: LatLng(route.bounds.northeast.lat, route.bounds.northeast.lng),
          southwest: LatLng(route.bounds.southwest.lat, route.bounds.southwest.lng),
        ),
        30,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _tambahFasePerjalananProvider = Provider.of<TambahFasePerjalananProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text("Tambah Fase Perjalanan"),
      ),
      body: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              target: LatLng(-6.397430, 106.804530),
              zoom: 12,
            ),
            onMapCreated: _onMapCreated,
            markers: _markers,
            polylines: _polylines,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _keyFormRoute,
              autovalidate: _autoValidate,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Card(
                    child: TextFormField(
                      controller: _txtDari,
                      enabled: _txtDariEnabled,
                      decoration: InputDecoration(
                        labelText: "Dari",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      validator: (value) {
                        return FormValidation.textRequired(
                          value: value,
                          label: "Dari",
                        );
                      },
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: _txtTujuan,
                      decoration: InputDecoration(
                        labelText: "Tujuan",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      validator: (value) {
                        return FormValidation.textRequired(
                          value: value,
                          label: "Tujuan",
                        );
                      },
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(),
                      ),
                      (!_tambahFasePerjalananProvider.loadingBtnLihatRute)
                          ? Padding(
                              padding: EdgeInsets.only(top: 2),
                              child: ButtonCircular(
                                text: "Lihat Rute",
                                onPressed: () => _onLihatRutePressed(context),
                              ),
                            )
                          : Padding(
                              padding: EdgeInsets.only(top: 2),
                              child: Card(
                                color: Theme.of(context).primaryColor,
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 20,
                                        width: 20,
                                        child: CircularProgressIndicator(
                                          backgroundColor: Theme.of(context).primaryIconTheme.color,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 8),
                                        child: Text(
                                          "Mendapatkan rute terbaik...",
                                          style: Theme.of(context).textTheme.button,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 8,
            right: 8,
            child: Card(
              color: Theme.of(context).primaryColor,
              child: Tooltip(
                message: "Ke Lokasi Saya",
                child: InkWell(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Icon(
                      Icons.my_location,
                      color: Theme.of(context).primaryIconTheme.color,
                    ),
                  ),
                  onTap: _onMyLocationPressed,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 8,
            left: 8,
            child: Card(
              color: Theme.of(context).primaryColor,
              child: InkWell(
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Icon(
                        Icons.save,
                        color: Theme.of(context).primaryIconTheme.color,
                      ),
                      Text(
                        " Simpan Rute",
                        style: Theme.of(context).textTheme.button,
                      ),
                    ],
                  ),
                ),
                onTap: _onSimpanRutePressed,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
