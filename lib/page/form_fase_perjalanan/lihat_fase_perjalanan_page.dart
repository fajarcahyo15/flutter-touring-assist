import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:new_touring_assist_flutter/helper/coordinates_converter.dart';
import 'package:new_touring_assist_flutter/helper/form_validation.dart';
import 'package:new_touring_assist_flutter/helper/image_assets_helper.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/provider/form_fase_perjalanan/lihat_fase_perjalanan_provider.dart';
import 'package:new_touring_assist_flutter/widget/card_loading.dart';
import 'package:provider/provider.dart';

class LihatFasePerjalananPage extends StatefulWidget {
  final int idGrupRute;

  const LihatFasePerjalananPage({
    Key key,
    @required this.idGrupRute,
  }) : super(key: key);

  @override
  _LihatFasePerjalananPageState createState() => _LihatFasePerjalananPageState();
}

class _LihatFasePerjalananPageState extends State<LihatFasePerjalananPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormRoute = GlobalKey<FormState>();

  final Set<Marker> _markers = {};
  final Set<Polyline> _polylines = {};

  final _txtDari = TextEditingController();
  final _txtTujuan = TextEditingController();

  LihatFasePerjalananProvider _lihatFasePerjalananProvider;
  GoogleMapController _googleMapController;

  BitmapDescriptor originMarkerIcon, destinationMakrerIcon;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _lihatFasePerjalananProvider.clearData();
    });

    ImageAssetsHelper.getBytesFromAsset('assets/images/origin.png', 64).then((icon) {
      setState(() {
        originMarkerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });

    ImageAssetsHelper.getBytesFromAsset('assets/images/destination.png', 64).then((icon) {
      setState(() {
        destinationMakrerIcon = BitmapDescriptor.fromBytes(icon);
      });
    });
  }

  @override
  void dispose() {
    _txtDari.dispose();
    _txtTujuan.dispose();
    super.dispose();
  }

  void _onMapCreated(GoogleMapController controller) async {
    _lihatFasePerjalananProvider.loadingScreen = true;
    GrupRute grupRute = await _lihatFasePerjalananProvider.getGrupRute(widget.idGrupRute);

    setState(() {
      _googleMapController = controller;
    });

    if (grupRute == null) {
      _keyScaffold.currentState.showSnackBar(SnackBar(
        content: Text(_lihatFasePerjalananProvider.toastMessage),
      ));
      _lihatFasePerjalananProvider.loadingScreen = false;
      return;
    }

    _markers.clear();
    _polylines.clear();

    List<Placemark> origin = await Geolocator().placemarkFromAddress(grupRute.origin);
    List<Placemark> destination = await Geolocator().placemarkFromAddress(grupRute.destination);

    setState(() {
      _markers.add(
        Marker(
          markerId: MarkerId("origin"),
          position: LatLng(origin[0].position.latitude, origin[0].position.longitude),
          icon: originMarkerIcon,
          infoWindow: InfoWindow(
            title: "Titik Pemberangkatan",
            snippet: grupRute.origin,
          ),
        ),
      );

      _markers.add(
        Marker(
          markerId: MarkerId("destination"),
          position: LatLng(destination[0].position.latitude, destination[0].position.longitude),
          icon: destinationMakrerIcon,
          infoWindow: InfoWindow(
            title: "Titik Pemberhentian",
            snippet: grupRute.destination,
          ),
        ),
      );
    });

    List<PointLatLng> listPointLatLng = PolylinePoints().decodePolyline(grupRute.polyline);
    List<LatLng> listLatLng = CoordinatesConverter.listPointLatLngToListLatLng(listPointLatLng);

    setState(() {
      _polylines.add(
        Polyline(
          polylineId: PolylineId("polyline"),
          color: Colors.blueAccent,
          points: listLatLng,
          width: 4,
        ),
      );
    });

    _googleMapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(origin[0].position.latitude, origin[0].position.longitude),
          zoom: 12,
        ),
      ),
    );

    _txtDari.text = grupRute.origin;
    _txtTujuan.text = grupRute.destination;
    _lihatFasePerjalananProvider.loadingScreen = false;
  }

  @override
  Widget build(BuildContext context) {
    _lihatFasePerjalananProvider = Provider.of<LihatFasePerjalananProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text("Lihat Fase Perjalanan"),
      ),
      body: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              target: LatLng(-6.397430, 106.804530),
              zoom: 12,
            ),
            onMapCreated: _onMapCreated,
            markers: _markers,
            polylines: _polylines,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _keyFormRoute,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Card(
                    child: TextFormField(
                      controller: _txtDari,
                      enabled: false,
                      // initialValue: _grupRute.origin ?? "",
                      decoration: InputDecoration(
                        labelText: "Dari",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      validator: (value) {
                        return FormValidation.textRequired(
                          value: value,
                          label: "Dari",
                        );
                      },
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: _txtTujuan,
                      enabled: false,
                      // initialValue: _grupRute.destination ?? "",
                      decoration: InputDecoration(
                        labelText: "Tujuan",
                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      validator: (value) {
                        return FormValidation.textRequired(
                          value: value,
                          label: "Tujuan",
                        );
                      },
                    ),
                  ),
                  (_lihatFasePerjalananProvider.loadingScreen)
                      ? Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(),
                            ),
                            CardLoading(loadingMessage: "Mendapatkan rute..."),
                          ],
                        )
                      : Container(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
