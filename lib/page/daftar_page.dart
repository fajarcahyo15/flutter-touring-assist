import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/helper/form_validation.dart';
import 'package:new_touring_assist_flutter/provider/daftar_provider.dart';
import 'package:new_touring_assist_flutter/widget/alert.dart';
import 'package:new_touring_assist_flutter/widget/button_circular.dart';
import 'package:new_touring_assist_flutter/widget/indicator_loading_button.dart';
import 'package:new_touring_assist_flutter/widget/tff_password.dart';
import 'package:provider/provider.dart';

class DaftarPage extends StatefulWidget {
  @override
  _DaftarPageState createState() => _DaftarPageState();
}

class _DaftarPageState extends State<DaftarPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormDaftar = GlobalKey<FormState>();

  final _txtNoHP = TextEditingController();
  final _txtNamaLengkap = TextEditingController();
  final _txtEmail = TextEditingController();
  final _txtPassword = TextEditingController();
  final _txtKonfPassword = TextEditingController();

  DaftarProvider _daftarProvider;

  bool _autoValidate = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _daftarProvider.clearData();
    });
  }

  @override
  void dispose() {
    _txtNoHP.dispose();
    _txtNamaLengkap.dispose();
    _txtEmail.dispose();
    _txtPassword.dispose();
    _txtKonfPassword.dispose();
    super.dispose();
  }

  void _onBtnDaftarPressed() async {
    setState(() => _autoValidate = true);
    if (_keyFormDaftar.currentState.validate()) {
      Map<String, dynamic> input = {
        'no_hp': _txtNoHP.text.trim(),
        'nama_lengkap': _txtNamaLengkap.text.trim(),
        'email': _txtEmail.text.trim(),
        'password': _txtPassword.text.trim(),
        'konf_password': _txtKonfPassword.text.trim(),
      };

      bool success = await _daftarProvider.daftarAkun(input);

      if (success) {
        _keyFormDaftar.currentState.reset();

        Alert.inform(
          context: context,
          content: _daftarProvider.toastMessage,
          onYes: () {
            Navigator.pop(context);
            Navigator.pop(context);
          },
        );
      } else {
        Alert.inform(
          context: context,
          content: _daftarProvider.toastMessage,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    _daftarProvider = Provider.of<DaftarProvider>(context);

    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text("Daftar Akun"),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(32),
            child: Form(
              key: _keyFormDaftar,
              autovalidate: _autoValidate,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  TextFormField(
                    controller: _txtNoHP,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "Nomor HP",
                      contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                    validator: (value) {
                      return FormValidation.textBetween(
                        value: value,
                        min: 11,
                        max: 13,
                        label: "Nomor HP",
                      );
                    },
                  ),
                  SizedBox(height: 8),
                  TextFormField(
                    controller: _txtNamaLengkap,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: "Nama Lengkap",
                      contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                    validator: (value) {
                      return FormValidation.textMin(
                        value: value,
                        min: 3,
                        label: "Nama Lengkap",
                      );
                    },
                  ),
                  SizedBox(height: 8),
                  TextFormField(
                    controller: _txtEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: "Email",
                      contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                    validator: (value) {
                      return FormValidation.email(
                        value: value,
                        label: "Email",
                      );
                    },
                  ),
                  SizedBox(height: 8),
                  TFFPassword(
                    controller: _txtPassword,
                    labelText: "Password",
                    validator: (value) {
                      return FormValidation.textMin(
                        value: value,
                        min: 5,
                        label: "Password",
                      );
                    },
                  ),
                  SizedBox(height: 8),
                  TFFPassword(
                    controller: _txtKonfPassword,
                    labelText: "Konfirmasi Password",
                    validator: (value) {
                      String message = "Password yang dimasukkan tidak sama";
                      if (value == _txtPassword.text.trim()) {
                        message = null;
                      }
                      return message;
                    },
                  ),
                  SizedBox(height: 8),
                  (!_daftarProvider.reqDaftar)
                      ? ButtonCircular(
                          text: "Daftar",
                          onPressed: _onBtnDaftarPressed,
                        )
                      : IndicatorLoadingButton(
                          loadingText: "Mengirim data...",
                        ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
