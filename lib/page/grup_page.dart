import 'package:flutter/material.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/page/asisten_perjalanan_page.dart';
import 'package:new_touring_assist_flutter/page/settings_grup_page.dart';

class GrupPage extends StatefulWidget {
  final Grup grup;

  const GrupPage({
    Key key,
    @required this.grup,
  }) : super(key: key);

  _GrupPageState createState() => _GrupPageState();
}

class _GrupPageState extends State<GrupPage> {
  final _keyScaffold = GlobalKey<ScaffoldState>();
  final _keyFormPesan = GlobalKey<ScaffoldState>();

  final _txtPesan = TextEditingController();

  @override
  void dispose() {
    _txtPesan.dispose();
    super.dispose();
  }

  void _onSettingsPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SettingsGrupPage(
          grup: widget.grup,
        ),
      ),
    );
  }

  void _onAssitancePressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AsistenPerjalananPage(
          idGrup: widget.grup.idGrup ?? "-",
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        title: Text(widget.grup.namaGrup),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.motorcycle),
            onPressed: () => _onAssitancePressed(context),
          ),
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => _onSettingsPressed(context),
          ),
        ],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                reverse: true,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: Container(),
                        ),
                        Flexible(
                          flex: 10,
                          child: Card(
                            elevation: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Nama Pengirim",
                                    style: TextStyle(
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8),
                                    child: Text("Vestibulum ante ipsum primis in faucibus orci."),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 8),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Icon(
                                          Icons.timer,
                                          size: 14,
                                          color: Colors.grey,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 2.0),
                                          child: Text(
                                            "Waktu kirim pesan",
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontStyle: FontStyle.italic,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          flex: 10,
                          child: Card(
                            elevation: 1,
                            color: Colors.green[50],
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Nama Pengirim",
                                    style: TextStyle(
                                      color: Colors.orange[900],
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8),
                                    child: Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec."),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 8),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Icon(
                                          Icons.timer,
                                          size: 14,
                                          color: Colors.grey,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 2.0),
                                          child: Text(
                                            "Waktu kirim pesan",
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontStyle: FontStyle.italic,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: Container(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Form(
              key: _keyFormPesan,
              child: Padding(
                padding: EdgeInsets.all(8),
                child: TextFormField(
                  controller: _txtPesan,
                  keyboardType: TextInputType.multiline,
                  decoration: InputDecoration(
                    labelText: "Pesan",
                    contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () {},
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
