import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:new_touring_assist_flutter/helper/notification_helper.dart';

class FcmSingleton {
  static final FcmSingleton _fcmService = new FcmSingleton._internal();
  FcmSingleton._internal();
  static FcmSingleton get instance => _fcmService;

  FirebaseMessaging _firebaseMessaging;

  FirebaseMessaging get firebaseMessaging {
    if (_firebaseMessaging != null) {
      return _firebaseMessaging;
    }

    _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage");
        print(message);
        NotificationHelper.show(message['notification']['title'], message['notification']['body']);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume");
        print(message);
        NotificationHelper.show(message['notification']['title'], message['notification']['body']);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch");
        print(message);
        NotificationHelper.show(message['notification']['title'], message['notification']['body']);
      },
    );

    return _firebaseMessaging;
  }
}
