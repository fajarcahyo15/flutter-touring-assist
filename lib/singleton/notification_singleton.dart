import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationSingleton {
  static final NotificationSingleton _notificationSingleton = new NotificationSingleton._internal();
  NotificationSingleton._internal();
  static NotificationSingleton get instance => _notificationSingleton;

  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;

  FlutterLocalNotificationsPlugin get flutterNotificationPlugin {
    if (_flutterLocalNotificationsPlugin != null) {
      return _flutterLocalNotificationsPlugin;
    }

    _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
    _flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: (String payload) async {
      print(payload);
    });

    return _flutterLocalNotificationsPlugin;
  }
}
