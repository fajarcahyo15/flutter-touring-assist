import 'dart:convert';
import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:new_touring_assist_flutter/helper/api_key.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';

import 'package:http/http.dart' as http;

class DarkSkyRepo extends Repo {
  Future<Map<String, dynamic>> forecast(LatLng location) async {
    Map<String, dynamic> map = {
      'message': null,
    };

    try {
      var res = await http.get("https://api.darksky.net/forecast/${ApiKey.darkSky}/${location.latitude},${location.longitude}?exclude=currently&lang=id");

      if (res.statusCode == 200) {
        var result = jsonDecode(res.body);
        map = result;
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Dark Sky -> forecast $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Dark Sky -> forecast $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
