import 'dart:io';

import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/model/response/anggota_grup/res_list_anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_buat_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_detail_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_edit_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_list_grup.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';
import 'package:http/http.dart' as http;

class GrupRepo extends Repo {
  Future<Map<String, dynamic>> create(Grup val) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post(
        "$baseUrl/grup/create",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'nama_grup': val.namaGrup,
          'tgl_berangkat': val.tglBerangkat,
          'jarak_maksimal': val.jarakMaksimal.toString(),
          'interval_lokasi': val.intervalLokasi.toString(),
          'deskripsi': val.deskripsi,
        },
      );

      if (res.statusCode == 200) {
        ResBuatGrup resBuatGrup = ResBuatGrup.fromJson(res.body);

        if (resBuatGrup.status == true) {
          map['data'] = resBuatGrup;
        } else {
          map['message'] = errMessage;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup -> create $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup -> create $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getByIdMember() async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/grup/read/", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResListGrup resListGrup = ResListGrup.fromJson(res.body);

        if (resListGrup.status == true) {
          map['data'] = resListGrup;
        } else {
          map['message'] = errMessage;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup -> getByIdMember $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup -> getByIdMember $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getById(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get(
        "$baseUrl/grup/detail?id_grup=$idGrup",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
      );

      if (res.statusCode == 200) {
        ResDetailGrup resDetailGrup = ResDetailGrup.fromJson(res.body);

        if (resDetailGrup.status == true) {
          map['data'] = resDetailGrup;
        } else {
          map['message'] = errMessage;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup -> getById $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup -> getById $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getListMember(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/grup/member?id_grup=$idGrup", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResListAnggotaGrup resListAnggotaGrup = ResListAnggotaGrup.fromJson(res.body);

        if (resListAnggotaGrup.status) {
          map['data'] = resListAnggotaGrup;
        } else {
          map['message'] = resListAnggotaGrup.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup -> getListMember $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup -> getListMember $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> update(Grup val) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post(
        "$baseUrl/grup/update",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'id_grup': val.idGrup,
          'nama_grup': val.namaGrup,
          'tgl_berangkat': val.tglBerangkat,
          'jarak_maksimal': val.jarakMaksimal.toString(),
          'interval_lokasi': val.intervalLokasi.toString(),
          'deskripsi': val.deskripsi,
        },
      );

      if (res.statusCode == 200) {
        ResEditGrup resEditGrup = ResEditGrup.fromJson(res.body);

        if (resEditGrup.status == true) {
          map['data'] = resEditGrup;
        } else {
          map['message'] = errMessage;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup -> update $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup -> update $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> updateFoto(String idGrup, String foto) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.put(
        "$baseUrl/grup/updatefoto",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'id_grup': idGrup,
          'foto_grup': foto,
        },
      );

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);

        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup -> updateFoto $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup -> updateFoto $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> delete(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post("$baseUrl/grup/delete", headers: {
        HttpHeaders.authorizationHeader: token,
      }, body: {
        'id_grup': idGrup,
      });

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);

        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup -> delete $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup -> delete $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
