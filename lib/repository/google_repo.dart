import 'dart:convert';
import 'dart:io';

import 'package:new_touring_assist_flutter/helper/api_key.dart';
import 'package:new_touring_assist_flutter/model/response/direction/direction.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';

import 'package:http/http.dart' as http;

class GoogleRepo extends Repo {
  Future<Map<String, dynamic>> direction(String origin, String destination) async {
    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http
          .get("https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination&key=${ApiKey.google}&language=id-ID&avoid=tolls&mode=driving&alternatives =true");

      if (res.statusCode == 200) {
        var result = jsonDecode(res.body);

        if (result['status'] == "OK")
          map['data'] = Direction.fromJson(res.body);
        else
          map['message'] = result['error_message'];
      } else
        map['message'] = errMessage;
    } on SocketException catch (e) {
      print("Error Repo Google -> direction $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Goole -> direction $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
