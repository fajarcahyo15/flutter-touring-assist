import 'dart:convert';
import 'dart:io';

import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';

import 'repo.dart';
import 'package:http/http.dart' as http;

class AuthRepo extends Repo {
  Future<Map<String, dynamic>> login(username, password) async {
    Map<String, dynamic> map = {
      'status': false,
    };

    try {
      var res = await http.post("${baseUrl}auth/login", headers: {
        HttpHeaders.contentTypeHeader: "application/x-www-form-urlencoded",
      }, body: {
        'username': username,
        'password': password,
      });

      if (res.statusCode == 200) {
        var result = jsonDecode(res.body);
        map['status'] = result['status'];

        if (map['status']) {
          map['token'] = result['data']['token'];
        } else {
          map['message'] = result['message'];
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print(e);
      map['message'] = errConnectionMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> logout() async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post("${baseUrl}akun/logout", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);
        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo AuthRepo -> logout $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo AuthRepo -> logout $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
