import 'dart:io';

import 'package:new_touring_assist_flutter/model/anggota.dart';
import 'package:new_touring_assist_flutter/model/response/anggota/res_detail_anggota.dart';
import 'package:new_touring_assist_flutter/model/response/anggota_grup/res_list_anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';
import 'package:http/http.dart' as http;

class AnggotaRepo extends Repo {
  Future<Map<String, dynamic>> cariTambahAnggota(String idGrup, String cari) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/anggota/caritambahanggota?id_grup=$idGrup&cari=$cari", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResListAnggotaGrup resListAnggotaGrup = ResListAnggotaGrup.fromJson(res.body);

        if (resListAnggotaGrup.status) {
          map['data'] = resListAnggotaGrup;
        } else {
          map['message'] = resListAnggotaGrup.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error AnggotaRepo -> cariTambahAnggota $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error AnggotaRepo -> cariTambahAnggota $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getProfil() async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/anggota/profil", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResDetailAnggota resDetailAnggota = ResDetailAnggota.fromJson(res.body);

        if (resDetailAnggota.status) {
          map['data'] = resDetailAnggota;
        } else {
          map['message'] = resDetailAnggota.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error AnggotaRepo -> getProfil $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error AnggotaRepo -> getProfil $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> updateProfil(Anggota profil) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.put(
        "$baseUrl/anggota/profil",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'no_hp': profil.noHp,
          'alamat_lengkap': profil.alamatLengkap,
        },
      );

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);

        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error AnggotaRepo -> updateProfil $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error AnggotaRepo -> updateProfil $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> updateFoto(String foto) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.put(
        "$baseUrl/anggota/foto",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'foto': foto,
        },
      );

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);

        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error AnggotaRepo -> updateFoto $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error AnggotaRepo -> updateFoto $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
