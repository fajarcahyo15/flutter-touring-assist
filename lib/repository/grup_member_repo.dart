import 'dart:io';

import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/res_detail_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/res_list_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';
import 'package:http/http.dart' as http;

class GrupMemberRepo extends Repo {
  Future<Map<String, dynamic>> getByIdGrup(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/grup_member/byidgrup?id_grup=$idGrup", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResListGrupMember resListGrupMember = ResListGrupMember.fromJson(res.body);

        if (resListGrupMember.status) {
          map['data'] = resListGrupMember;
        } else {
          map['message'] = resListGrupMember.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Member -> getByIdGrup $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Member -> getByIdGrup $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getAssistActive(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/grup_member/asssitactive?id_grup=$idGrup", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResListGrupMember resListGrupMember = ResListGrupMember.fromJson(res.body);

        if (resListGrupMember.status) {
          map['data'] = resListGrupMember;
        } else {
          map['message'] = resListGrupMember.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Member -> getAssistActive $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Member -> getAssistActive $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getDetailByIdGrup(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/grup_member/detailbyidgrupanggota?id_grup=$idGrup", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResDetailGrupMember resDetailGrupMember = ResDetailGrupMember.fromJson(res.body);

        if (resDetailGrupMember.status) {
          map['data'] = resDetailGrupMember;
        } else {
          map['message'] = resDetailGrupMember.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Member -> updateAssist $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Member -> updateAssist $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> addMember(String idGrup, String idAnggota) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post(
        "$baseUrl/grup_member/add",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'id_grup': idGrup,
          'id_anggota': idAnggota,
        },
      );

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);

        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Member -> addMember $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Member -> addMember $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> updateAssist(GrupRute grupRute) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post("$baseUrl/grup_member/assist", headers: {
        HttpHeaders.authorizationHeader: token,
      }, body: {
        'id_grup_rute': grupRute.idGrupRute.toString(),
        'assist_member': grupRute.assistRute.toString(),
      });

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);
        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Member -> updateAssist $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Member -> updateAssist $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> keluarGrup(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post("$baseUrl/grup_member/keluar", headers: {
        HttpHeaders.authorizationHeader: token,
      }, body: {
        'id_grup': idGrup,
      });

      print(res.body);

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);

        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Member -> keluar $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Member -> keluar $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
