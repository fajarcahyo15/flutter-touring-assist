import 'dart:io';

import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';
import 'package:http/http.dart' as http;

class AkunRepo extends Repo {
  Future<Map<String, dynamic>> daftarAkun(Map<String, dynamic> input) async {
    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post("${baseUrl}akun/daftar", body: {
        'no_hp': input['no_hp'],
        'nama_lengkap': input['nama_lengkap'],
        'email': input['email'],
        'password': input['password'],
        'konf_password': input['konf_password'],
      });

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);
        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo AkunRepo -> daftarAkun $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo AkunRepo -> daftarAkun $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> lupaPassword(String email) async {
    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post("${baseUrl}akun/lupa", body: {
        'email': email,
      });

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);
        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo AkunRepo -> lupaPassword $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo AkunRepo -> lupaPassword $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> updateFcmToken(String fcmToken) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.put(
        "${baseUrl}akun/fcm",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'fcm_token': fcmToken,
        },
      );

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);
        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo AkunRepo -> updateFcmToken $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo AkunRepo -> updateFcmToken $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> updatePassword(String passwordLama, String passwordBaru) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.put(
        "${baseUrl}akun/updatepassword",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'password_lama': passwordLama,
          'password_baru': passwordBaru,
        },
      );

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);
        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo AkunRepo -> updatePassword $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo AkunRepo -> updatePassword $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
