import 'dart:io';

import 'package:new_touring_assist_flutter/model/response/weather/res_bad_weather.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';
import 'package:http/http.dart' as http;

class WeatherRepo extends Repo {
  Future<Map<String, dynamic>> getListBadWeather() async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get(
        "$baseUrl/weather/badweather",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
      );

      if (res.statusCode == 200) {
        ResBadWeather resBadWeather = ResBadWeather.fromJson(res.body);

        if (resBadWeather.status == true) {
          map['data'] = resBadWeather;
        } else {
          map['message'] = errMessage;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Weather -> getListBadWeather $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Weather -> getListBadWeather $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
