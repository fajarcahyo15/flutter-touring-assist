import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:new_touring_assist_flutter/helper/api_key.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/forecast.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';
import 'package:http/http.dart' as http;

class OwmRepo extends Repo {
  Future<Map<String, dynamic>> forecast5d3h(LatLng location) async {
    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("https://api.openweathermap.org/data/2.5/forecast?lat=${location.latitude}&lon=${location.longitude}&appid=${ApiKey.owm}");

      if (res.statusCode == 200) {
        Forecast forecast = Forecast.fromJson(res.body);
        map['data'] = forecast;
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Owm -> forecast5d3h $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Owm -> forecast5d3h $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
