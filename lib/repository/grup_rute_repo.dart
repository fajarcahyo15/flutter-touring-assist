import 'dart:convert';
import 'dart:io';

import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_buat_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_detail_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_last_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_list_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/repository/local/session.dart';
import 'package:new_touring_assist_flutter/repository/repo.dart';
import 'package:http/http.dart' as http;

class GrupRuteRepo extends Repo {
  Future<Map<String, dynamic>> getByIdGrup(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get(
        "$baseUrl/grup_rute/list?id_grup=$idGrup",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
      );

      if (res.statusCode == 200) {
        ResListGrupRute resListGrupRute = ResListGrupRute.fromJson(res.body);

        if (resListGrupRute.status == true) {
          map['data'] = resListGrupRute;
        } else {
          map['message'] = resListGrupRute.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Rute -> getByIdGrup $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Rute -> getByIdGrup $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getByIdGrupRute(int idGrupRute) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/grup_rute/detail?id_grup_rute=$idGrupRute", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResDetailGrupRute resDetailGrupRute = ResDetailGrupRute.fromJson(res.body);
        if (resDetailGrupRute.status == true) {
          map['data'] = resDetailGrupRute;
        } else {
          map['message'] = resDetailGrupRute.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Rute -> getByIdGrupRute $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Rute -> getByIdGrupRute $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getLastRouteByIdGrup(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get(
        "$baseUrl/grup_rute/last?id_grup=$idGrup",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
      );

      if (res.statusCode == 200) {
        ResLastGrupRute resLastGrupRute = ResLastGrupRute.fromJson(res.body);
        if (resLastGrupRute.status == true) {
          map['data'] = resLastGrupRute;
        } else {
          map['message'] = resLastGrupRute.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Rute -> getLastRouteByIdGrup $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Rute -> getLastRouteByIdGrup $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getOriDesByIdGrup(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get(
        "$baseUrl/grup_rute/orides?id_grup=$idGrup",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
      );

      if (res.statusCode == 200) {
        var response = jsonDecode(res.body);
        map['data'] = response['data']['perjalanan'];
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Rute -> getOriDesByIdGrup $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Rute -> getOriDesByIdGrup $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> getAssistActive(String idGrup) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.get("$baseUrl/grup_rute/active?id_grup=$idGrup", headers: {
        HttpHeaders.authorizationHeader: token,
      });

      if (res.statusCode == 200) {
        ResDetailGrupRute resDetailGrupRute = ResDetailGrupRute.fromJson(res.body);
        if (resDetailGrupRute.status) {
          map['data'] = resDetailGrupRute;
        } else {
          map['message'] = resDetailGrupRute.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Rute -> getAssistActive $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Rute -> getAssistActive $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> create(GrupRute val) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post(
        "$baseUrl/grup_rute/create",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'origin_address': val.origin,
          'destination_address': val.destination,
          'polyline': val.polyline,
          'waktu_tempuh': val.waktuTempuh.toString(),
          'jarak': val.jarak.toString(),
          'id_grup': val.idGrup,
        },
      );

      if (res.statusCode == 200) {
        ResBuatGrupRute resBuatGrupRute = ResBuatGrupRute.fromJson(res.body);
        if (resBuatGrupRute.status == true) {
          map['data'] = resBuatGrupRute;
        } else {
          map['message'] = resBuatGrupRute.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Rute -> create $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Rute -> create $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> updateAssist(GrupRute grupRute) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post("$baseUrl/grup_rute/assist", headers: {
        HttpHeaders.authorizationHeader: token,
      }, body: {
        'id_grup_rute': grupRute.idGrupRute.toString(),
        'assist_rute': grupRute.assistRute.toString(),
      });

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);

        if (resBase.status) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Rute -> updateAssist $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Rute -> updateAssist $e");
      map['message'] = errMessage;
    }

    return map;
  }

  Future<Map<String, dynamic>> delete(String idGrupRute) async {
    Session session = Session();
    String token = await session.getToken();

    Map<String, dynamic> map = {
      'data': null,
      'message': null,
    };

    try {
      var res = await http.post(
        "$baseUrl/grup_rute/delete",
        headers: {
          HttpHeaders.authorizationHeader: token,
        },
        body: {
          'id_grup_rute': idGrupRute,
        },
      );

      if (res.statusCode == 200) {
        ResBase resBase = ResBase.fromJson(res.body);

        if (resBase.status == true) {
          map['data'] = resBase;
        } else {
          map['message'] = resBase.message;
        }
      } else {
        map['message'] = errMessage;
      }
    } on SocketException catch (e) {
      print("Error Repo Grup Rute -> delete $e");
      map['message'] = errConnectionMessage;
    } catch (e) {
      print("Error Repo Grup Rute -> delete $e");
      map['message'] = errMessage;
    }

    return map;
  }
}
