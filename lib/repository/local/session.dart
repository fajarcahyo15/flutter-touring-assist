import 'package:shared_preferences/shared_preferences.dart';

class Session {
  SharedPreferences _sharedPreferences;

  Future setToken(String token) async {
    _sharedPreferences = await SharedPreferences.getInstance();
    await _sharedPreferences.setString("token", token);
  }

  Future<String> getToken() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    String token = _sharedPreferences.getString("token");
    return token;
  }
}
