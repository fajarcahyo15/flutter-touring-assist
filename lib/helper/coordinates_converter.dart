import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CoordinatesConverter {
  static List<LatLng> listPointLatLngToListLatLng(List<PointLatLng> val) {
    List<LatLng> data = List();

    val.forEach((value) {
      data.add(LatLng(value.latitude, value.longitude));
    });

    return data;
  }
}
