import 'package:flutter/material.dart';

class FormValidation {
  static String textRequired({
    @required String value,
    String label = "ini",
  }) {
    if (value.trim().length <= 0) {
      return "Kolom $label harus diisi";
    } else {
      return null;
    }
  }

  static String textMin({
    @required String value,
    @required int min,
    String label = "ini",
  }) {
    if (value.trim().length < min) {
      return "Kolom $label harus memiliki minimal $min karakter";
    } else {
      return null;
    }
  }

  static String textMax({
    @required String value,
    @required int max,
    String label = "ini",
  }) {
    if (value.trim().length > max) {
      return "Kolom $label harus memiliki maksimal $max karakter";
    } else {
      return null;
    }
  }

  static String textBetween({
    @required String value,
    @required int min,
    @required int max,
    String label = "ini",
  }) {
    if ((value.trim().length > max) || (value.trim().length < min)) {
      return "Kolom $label harus diantara $min sampai $max karakter";
    } else {
      return null;
    }
  }

  static String numberRequired({
    @required String value,
    String label = "ini",
  }) {
    String error = "Kolom $label harus diisi";

    if (value.trim().length <= 0) {
      return error;
    }

    try {
      if (int.parse(value) <= 0) {
        return error;
      }
    } catch (e) {
      return "Kolom $label harus diisi angka";
    }

    return null;
  }

  static String numberBetween({
    @required String value,
    @required int min,
    @required int max,
    String label = "ini",
  }) {
    String error = "Kolom $label harus diantara $min sampai $max";

    try {
      if ((int.parse(value.trim()) < min) || (int.parse(value.trim()) > max)) {
        return error;
      }
    } catch (e) {
      return "Kolom $label harus diisi angka";
    }

    return null;
  }

  static String email({
    @required String value,
    String label = "ini",
  }) {
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);

    if (!regex.hasMatch(value)) {
      return "Kolom $label harus berisi email";
    } else {
      return null;
    }
  }
}
