import 'package:flutter/foundation.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

enum DateFormatEnum {
  yyyyMMdd,
  yyyyMMddHHmm,
  ddMMMMyyyy,
  HHmm,
}

class StringDateFormatter {
  static String format({
    @required DateFormatEnum from,
    @required DateFormatEnum to,
    @required String date,
  }) {
    String output = date;

    if (date != null) {
      initializeDateFormatting();

      if ((from == DateFormatEnum.yyyyMMdd) && (to == DateFormatEnum.ddMMMMyyyy)) {
        DateFormat dateFormat = DateFormat("yyyy-MM-dd");
        DateTime dateTime = dateFormat.parse(date);
        DateFormat transform = DateFormat("dd MMMM yyyy", "id");
        output = transform.format(dateTime);
      } else if ((from == DateFormatEnum.yyyyMMddHHmm) && (to == DateFormatEnum.ddMMMMyyyy)) {
        DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm");
        DateTime dateTime = dateFormat.parse(date);
        DateFormat transform = DateFormat("dd MMMM yyyy", "id");
        output = transform.format(dateTime);
      } else if ((from == DateFormatEnum.yyyyMMddHHmm) && (to == DateFormatEnum.HHmm)) {
        DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm");
        DateTime dateTime = dateFormat.parse(date);
        DateFormat transform = DateFormat("HH:mm", "id");
        output = transform.format(dateTime);
      } else if ((from == DateFormatEnum.yyyyMMddHHmm) && (to == DateFormatEnum.yyyyMMdd)) {
        DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm");
        DateTime dateTime = dateFormat.parse(date);
        DateFormat transform = DateFormat("yyyy-MM-dd", "id");
        output = transform.format(dateTime);
      }
    }

    return output;
  }
}
