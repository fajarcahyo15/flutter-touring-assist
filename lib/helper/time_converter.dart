class TimeConverter {
  static int minutesToSeconds(int minutes) {
    int seconds;
    if (minutes != null) {
      seconds = minutes * 60;
    }
    return seconds;
  }

  static int secondsToMinutes(int seconds) {
    int minutes;
    if (seconds != null) {
      double val = seconds / 60;
      minutes = val.round();
    }
    return minutes;
  }
}
