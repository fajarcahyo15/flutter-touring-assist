class ModelParser {
  static int parseInt(val) {
    int parsedValue;
    try {
      parsedValue = int.parse(val);
    } catch (e) {}

    return parsedValue;
  }

  static bool parseBool(val) {
    bool parsedValue;
    try {
      if ((val == '0') || (val == 'false') || (val == false)) {
        parsedValue = false;
      } else if ((val == '1') || (val == 'true') || (val == true)) {
        parsedValue = true;
      }
    } catch (e) {}

    return parsedValue;
  }
}
