import 'dart:typed_data';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:new_touring_assist_flutter/singleton/notification_singleton.dart';

class NotificationHelper {
  static void show(String title, String message) async {
    var vibrationPattern = new Int64List(4);
    vibrationPattern[0] = 0;
    vibrationPattern[1] = 1000;
    vibrationPattern[2] = 5000;
    vibrationPattern[3] = 2000;

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'NOTIF_ASSIST',
      'NOTIF ASSIST',
      'CHANNEL_NOTIF_ASSIST',
      importance: Importance.Max,
      priority: Priority.High,
      ticker: 'ticker',
      enableVibration: true,
      vibrationPattern: vibrationPattern,
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await NotificationSingleton.instance.flutterNotificationPlugin.show(0, title, message, platformChannelSpecifics, payload: 'item x');
  }
}
