// GENERATED CODE - DO NOT MODIFY BY HAND

part of anggota;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Anggota> _$anggotaSerializer = new _$AnggotaSerializer();

class _$AnggotaSerializer implements StructuredSerializer<Anggota> {
  @override
  final Iterable<Type> types = const [Anggota, _$Anggota];
  @override
  final String wireName = 'Anggota';

  @override
  Iterable<Object> serialize(Serializers serializers, Anggota object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idAnggota != null) {
      result
        ..add('id_anggota')
        ..add(serializers.serialize(object.idAnggota,
            specifiedType: const FullType(String)));
    }
    if (object.namaLengkap != null) {
      result
        ..add('nama_lengkap')
        ..add(serializers.serialize(object.namaLengkap,
            specifiedType: const FullType(String)));
    }
    if (object.jenisKelamin != null) {
      result
        ..add('jenis_kelamin')
        ..add(serializers.serialize(object.jenisKelamin,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.noSim != null) {
      result
        ..add('no_sim')
        ..add(serializers.serialize(object.noSim,
            specifiedType: const FullType(String)));
    }
    if (object.noHp != null) {
      result
        ..add('no_hp')
        ..add(serializers.serialize(object.noHp,
            specifiedType: const FullType(String)));
    }
    if (object.tempatLahir != null) {
      result
        ..add('tempat_lahir')
        ..add(serializers.serialize(object.tempatLahir,
            specifiedType: const FullType(String)));
    }
    if (object.tglLahir != null) {
      result
        ..add('tgl_lahir')
        ..add(serializers.serialize(object.tglLahir,
            specifiedType: const FullType(String)));
    }
    if (object.alamatLengkap != null) {
      result
        ..add('alamat_lengkap')
        ..add(serializers.serialize(object.alamatLengkap,
            specifiedType: const FullType(String)));
    }
    if (object.foto != null) {
      result
        ..add('foto')
        ..add(serializers.serialize(object.foto,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Anggota deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AnggotaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_anggota':
          result.idAnggota = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_lengkap':
          result.namaLengkap = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jenis_kelamin':
          result.jenisKelamin = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'no_sim':
          result.noSim = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'no_hp':
          result.noHp = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tempat_lahir':
          result.tempatLahir = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tgl_lahir':
          result.tglLahir = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'alamat_lengkap':
          result.alamatLengkap = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'foto':
          result.foto = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Anggota extends Anggota {
  @override
  final String idAnggota;
  @override
  final String namaLengkap;
  @override
  final String jenisKelamin;
  @override
  final String email;
  @override
  final String noSim;
  @override
  final String noHp;
  @override
  final String tempatLahir;
  @override
  final String tglLahir;
  @override
  final String alamatLengkap;
  @override
  final String foto;

  factory _$Anggota([void Function(AnggotaBuilder) updates]) =>
      (new AnggotaBuilder()..update(updates)).build();

  _$Anggota._(
      {this.idAnggota,
      this.namaLengkap,
      this.jenisKelamin,
      this.email,
      this.noSim,
      this.noHp,
      this.tempatLahir,
      this.tglLahir,
      this.alamatLengkap,
      this.foto})
      : super._();

  @override
  Anggota rebuild(void Function(AnggotaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AnggotaBuilder toBuilder() => new AnggotaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Anggota &&
        idAnggota == other.idAnggota &&
        namaLengkap == other.namaLengkap &&
        jenisKelamin == other.jenisKelamin &&
        email == other.email &&
        noSim == other.noSim &&
        noHp == other.noHp &&
        tempatLahir == other.tempatLahir &&
        tglLahir == other.tglLahir &&
        alamatLengkap == other.alamatLengkap &&
        foto == other.foto;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc(0, idAnggota.hashCode),
                                        namaLengkap.hashCode),
                                    jenisKelamin.hashCode),
                                email.hashCode),
                            noSim.hashCode),
                        noHp.hashCode),
                    tempatLahir.hashCode),
                tglLahir.hashCode),
            alamatLengkap.hashCode),
        foto.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Anggota')
          ..add('idAnggota', idAnggota)
          ..add('namaLengkap', namaLengkap)
          ..add('jenisKelamin', jenisKelamin)
          ..add('email', email)
          ..add('noSim', noSim)
          ..add('noHp', noHp)
          ..add('tempatLahir', tempatLahir)
          ..add('tglLahir', tglLahir)
          ..add('alamatLengkap', alamatLengkap)
          ..add('foto', foto))
        .toString();
  }
}

class AnggotaBuilder implements Builder<Anggota, AnggotaBuilder> {
  _$Anggota _$v;

  String _idAnggota;
  String get idAnggota => _$this._idAnggota;
  set idAnggota(String idAnggota) => _$this._idAnggota = idAnggota;

  String _namaLengkap;
  String get namaLengkap => _$this._namaLengkap;
  set namaLengkap(String namaLengkap) => _$this._namaLengkap = namaLengkap;

  String _jenisKelamin;
  String get jenisKelamin => _$this._jenisKelamin;
  set jenisKelamin(String jenisKelamin) => _$this._jenisKelamin = jenisKelamin;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _noSim;
  String get noSim => _$this._noSim;
  set noSim(String noSim) => _$this._noSim = noSim;

  String _noHp;
  String get noHp => _$this._noHp;
  set noHp(String noHp) => _$this._noHp = noHp;

  String _tempatLahir;
  String get tempatLahir => _$this._tempatLahir;
  set tempatLahir(String tempatLahir) => _$this._tempatLahir = tempatLahir;

  String _tglLahir;
  String get tglLahir => _$this._tglLahir;
  set tglLahir(String tglLahir) => _$this._tglLahir = tglLahir;

  String _alamatLengkap;
  String get alamatLengkap => _$this._alamatLengkap;
  set alamatLengkap(String alamatLengkap) =>
      _$this._alamatLengkap = alamatLengkap;

  String _foto;
  String get foto => _$this._foto;
  set foto(String foto) => _$this._foto = foto;

  AnggotaBuilder();

  AnggotaBuilder get _$this {
    if (_$v != null) {
      _idAnggota = _$v.idAnggota;
      _namaLengkap = _$v.namaLengkap;
      _jenisKelamin = _$v.jenisKelamin;
      _email = _$v.email;
      _noSim = _$v.noSim;
      _noHp = _$v.noHp;
      _tempatLahir = _$v.tempatLahir;
      _tglLahir = _$v.tglLahir;
      _alamatLengkap = _$v.alamatLengkap;
      _foto = _$v.foto;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Anggota other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Anggota;
  }

  @override
  void update(void Function(AnggotaBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Anggota build() {
    final _$result = _$v ??
        new _$Anggota._(
            idAnggota: idAnggota,
            namaLengkap: namaLengkap,
            jenisKelamin: jenisKelamin,
            email: email,
            noSim: noSim,
            noHp: noHp,
            tempatLahir: tempatLahir,
            tglLahir: tglLahir,
            alamatLengkap: alamatLengkap,
            foto: foto);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
