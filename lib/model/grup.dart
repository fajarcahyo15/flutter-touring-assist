library grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'grup.g.dart';

abstract class Grup implements Built<Grup, GrupBuilder> {
  Grup._();

  factory Grup([updates(GrupBuilder b)]) = _$Grup;

  @nullable
  @BuiltValueField(wireName: 'id_grup')
  String get idGrup;

  @nullable
  @BuiltValueField(wireName: 'nama_grup')
  String get namaGrup;

  @nullable
  @BuiltValueField(wireName: 'deskripsi')
  String get deskripsi;

  @nullable
  @BuiltValueField(wireName: 'foto_grup')
  String get fotoGrup;

  @nullable
  @BuiltValueField(wireName: 'tgl_berangkat')
  String get tglBerangkat;

  @nullable
  @BuiltValueField(wireName: 'jarak_maksimal')
  String get jarakMaksimal;

  @nullable
  @BuiltValueField(wireName: 'interval_lokasi')
  String get intervalLokasi;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Grup.serializer, this));
  }

  static Grup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Grup.serializer, json.decode(jsonString));
  }

  static Serializer<Grup> get serializer => _$grupSerializer;
}
