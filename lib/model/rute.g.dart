// GENERATED CODE - DO NOT MODIFY BY HAND

part of rute;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Rute> _$ruteSerializer = new _$RuteSerializer();

class _$RuteSerializer implements StructuredSerializer<Rute> {
  @override
  final Iterable<Type> types = const [Rute, _$Rute];
  @override
  final String wireName = 'Rute';

  @override
  Iterable<Object> serialize(Serializers serializers, Rute object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idGrupRute != null) {
      result
        ..add('id_grup_rute')
        ..add(serializers.serialize(object.idGrupRute,
            specifiedType: const FullType(int)));
    }
    if (object.originLatlng != null) {
      result
        ..add('origin_latlng')
        ..add(serializers.serialize(object.originLatlng,
            specifiedType: const FullType(String)));
    }
    if (object.destinationLatlng != null) {
      result
        ..add('destination_latlng')
        ..add(serializers.serialize(object.destinationLatlng,
            specifiedType: const FullType(String)));
    }
    if (object.polyline != null) {
      result
        ..add('polyline')
        ..add(serializers.serialize(object.polyline,
            specifiedType: const FullType(String)));
    }
    if (object.waktuTempuh != null) {
      result
        ..add('waktu_tempuh')
        ..add(serializers.serialize(object.waktuTempuh,
            specifiedType: const FullType(int)));
    }
    if (object.jarak != null) {
      result
        ..add('jarak')
        ..add(serializers.serialize(object.jarak,
            specifiedType: const FullType(int)));
    }
    if (object.idGrup != null) {
      result
        ..add('id_grup')
        ..add(serializers.serialize(object.idGrup,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Rute deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RuteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_grup_rute':
          result.idGrupRute = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'origin_latlng':
          result.originLatlng = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'destination_latlng':
          result.destinationLatlng = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'polyline':
          result.polyline = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'waktu_tempuh':
          result.waktuTempuh = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'jarak':
          result.jarak = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'id_grup':
          result.idGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Rute extends Rute {
  @override
  final int idGrupRute;
  @override
  final String originLatlng;
  @override
  final String destinationLatlng;
  @override
  final String polyline;
  @override
  final int waktuTempuh;
  @override
  final int jarak;
  @override
  final String idGrup;

  factory _$Rute([void Function(RuteBuilder) updates]) =>
      (new RuteBuilder()..update(updates)).build();

  _$Rute._(
      {this.idGrupRute,
      this.originLatlng,
      this.destinationLatlng,
      this.polyline,
      this.waktuTempuh,
      this.jarak,
      this.idGrup})
      : super._();

  @override
  Rute rebuild(void Function(RuteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RuteBuilder toBuilder() => new RuteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Rute &&
        idGrupRute == other.idGrupRute &&
        originLatlng == other.originLatlng &&
        destinationLatlng == other.destinationLatlng &&
        polyline == other.polyline &&
        waktuTempuh == other.waktuTempuh &&
        jarak == other.jarak &&
        idGrup == other.idGrup;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, idGrupRute.hashCode), originLatlng.hashCode),
                        destinationLatlng.hashCode),
                    polyline.hashCode),
                waktuTempuh.hashCode),
            jarak.hashCode),
        idGrup.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Rute')
          ..add('idGrupRute', idGrupRute)
          ..add('originLatlng', originLatlng)
          ..add('destinationLatlng', destinationLatlng)
          ..add('polyline', polyline)
          ..add('waktuTempuh', waktuTempuh)
          ..add('jarak', jarak)
          ..add('idGrup', idGrup))
        .toString();
  }
}

class RuteBuilder implements Builder<Rute, RuteBuilder> {
  _$Rute _$v;

  int _idGrupRute;
  int get idGrupRute => _$this._idGrupRute;
  set idGrupRute(int idGrupRute) => _$this._idGrupRute = idGrupRute;

  String _originLatlng;
  String get originLatlng => _$this._originLatlng;
  set originLatlng(String originLatlng) => _$this._originLatlng = originLatlng;

  String _destinationLatlng;
  String get destinationLatlng => _$this._destinationLatlng;
  set destinationLatlng(String destinationLatlng) =>
      _$this._destinationLatlng = destinationLatlng;

  String _polyline;
  String get polyline => _$this._polyline;
  set polyline(String polyline) => _$this._polyline = polyline;

  int _waktuTempuh;
  int get waktuTempuh => _$this._waktuTempuh;
  set waktuTempuh(int waktuTempuh) => _$this._waktuTempuh = waktuTempuh;

  int _jarak;
  int get jarak => _$this._jarak;
  set jarak(int jarak) => _$this._jarak = jarak;

  String _idGrup;
  String get idGrup => _$this._idGrup;
  set idGrup(String idGrup) => _$this._idGrup = idGrup;

  RuteBuilder();

  RuteBuilder get _$this {
    if (_$v != null) {
      _idGrupRute = _$v.idGrupRute;
      _originLatlng = _$v.originLatlng;
      _destinationLatlng = _$v.destinationLatlng;
      _polyline = _$v.polyline;
      _waktuTempuh = _$v.waktuTempuh;
      _jarak = _$v.jarak;
      _idGrup = _$v.idGrup;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Rute other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Rute;
  }

  @override
  void update(void Function(RuteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Rute build() {
    final _$result = _$v ??
        new _$Rute._(
            idGrupRute: idGrupRute,
            originLatlng: originLatlng,
            destinationLatlng: destinationLatlng,
            polyline: polyline,
            waktuTempuh: waktuTempuh,
            jarak: jarak,
            idGrup: idGrup);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
