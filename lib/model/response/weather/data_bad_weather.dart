library data_bad_weather;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/weather.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_bad_weather.g.dart';

abstract class DataBadWeather implements Built<DataBadWeather, DataBadWeatherBuilder> {
  DataBadWeather._();

  factory DataBadWeather([updates(DataBadWeatherBuilder b)]) = _$DataBadWeather;

  @nullable
  @BuiltValueField(wireName: 'list_bad_weather')
  BuiltList<Weather> get listBadWeather;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataBadWeather.serializer, this));
  }

  static DataBadWeather fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataBadWeather.serializer, json.decode(jsonString));
  }

  static Serializer<DataBadWeather> get serializer => _$dataBadWeatherSerializer;
}
