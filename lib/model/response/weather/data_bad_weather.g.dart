// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_bad_weather;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataBadWeather> _$dataBadWeatherSerializer =
    new _$DataBadWeatherSerializer();

class _$DataBadWeatherSerializer
    implements StructuredSerializer<DataBadWeather> {
  @override
  final Iterable<Type> types = const [DataBadWeather, _$DataBadWeather];
  @override
  final String wireName = 'DataBadWeather';

  @override
  Iterable<Object> serialize(Serializers serializers, DataBadWeather object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.listBadWeather != null) {
      result
        ..add('list_bad_weather')
        ..add(serializers.serialize(object.listBadWeather,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Weather)])));
    }
    return result;
  }

  @override
  DataBadWeather deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataBadWeatherBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'list_bad_weather':
          result.listBadWeather.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Weather)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$DataBadWeather extends DataBadWeather {
  @override
  final BuiltList<Weather> listBadWeather;

  factory _$DataBadWeather([void Function(DataBadWeatherBuilder) updates]) =>
      (new DataBadWeatherBuilder()..update(updates)).build();

  _$DataBadWeather._({this.listBadWeather}) : super._();

  @override
  DataBadWeather rebuild(void Function(DataBadWeatherBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataBadWeatherBuilder toBuilder() =>
      new DataBadWeatherBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataBadWeather && listBadWeather == other.listBadWeather;
  }

  @override
  int get hashCode {
    return $jf($jc(0, listBadWeather.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataBadWeather')
          ..add('listBadWeather', listBadWeather))
        .toString();
  }
}

class DataBadWeatherBuilder
    implements Builder<DataBadWeather, DataBadWeatherBuilder> {
  _$DataBadWeather _$v;

  ListBuilder<Weather> _listBadWeather;
  ListBuilder<Weather> get listBadWeather =>
      _$this._listBadWeather ??= new ListBuilder<Weather>();
  set listBadWeather(ListBuilder<Weather> listBadWeather) =>
      _$this._listBadWeather = listBadWeather;

  DataBadWeatherBuilder();

  DataBadWeatherBuilder get _$this {
    if (_$v != null) {
      _listBadWeather = _$v.listBadWeather?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataBadWeather other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataBadWeather;
  }

  @override
  void update(void Function(DataBadWeatherBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataBadWeather build() {
    _$DataBadWeather _$result;
    try {
      _$result = _$v ??
          new _$DataBadWeather._(listBadWeather: _listBadWeather?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'listBadWeather';
        _listBadWeather?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataBadWeather', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
