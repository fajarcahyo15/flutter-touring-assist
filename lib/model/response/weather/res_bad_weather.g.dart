// GENERATED CODE - DO NOT MODIFY BY HAND

part of res_bad_weather;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResBadWeather> _$resBadWeatherSerializer =
    new _$ResBadWeatherSerializer();

class _$ResBadWeatherSerializer implements StructuredSerializer<ResBadWeather> {
  @override
  final Iterable<Type> types = const [ResBadWeather, _$ResBadWeather];
  @override
  final String wireName = 'ResBadWeather';

  @override
  Iterable<Object> serialize(Serializers serializers, ResBadWeather object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(bool)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
      'data',
      serializers.serialize(object.data,
          specifiedType: const FullType(DataBadWeather)),
    ];

    return result;
  }

  @override
  ResBadWeather deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResBadWeatherBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data':
          result.data.replace(serializers.deserialize(value,
              specifiedType: const FullType(DataBadWeather)) as DataBadWeather);
          break;
      }
    }

    return result.build();
  }
}

class _$ResBadWeather extends ResBadWeather {
  @override
  final bool status;
  @override
  final String message;
  @override
  final DataBadWeather data;

  factory _$ResBadWeather([void Function(ResBadWeatherBuilder) updates]) =>
      (new ResBadWeatherBuilder()..update(updates)).build();

  _$ResBadWeather._({this.status, this.message, this.data}) : super._() {
    if (status == null) {
      throw new BuiltValueNullFieldError('ResBadWeather', 'status');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('ResBadWeather', 'message');
    }
    if (data == null) {
      throw new BuiltValueNullFieldError('ResBadWeather', 'data');
    }
  }

  @override
  ResBadWeather rebuild(void Function(ResBadWeatherBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResBadWeatherBuilder toBuilder() => new ResBadWeatherBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResBadWeather &&
        status == other.status &&
        message == other.message &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, status.hashCode), message.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResBadWeather')
          ..add('status', status)
          ..add('message', message)
          ..add('data', data))
        .toString();
  }
}

class ResBadWeatherBuilder
    implements Builder<ResBadWeather, ResBadWeatherBuilder> {
  _$ResBadWeather _$v;

  bool _status;
  bool get status => _$this._status;
  set status(bool status) => _$this._status = status;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  DataBadWeatherBuilder _data;
  DataBadWeatherBuilder get data =>
      _$this._data ??= new DataBadWeatherBuilder();
  set data(DataBadWeatherBuilder data) => _$this._data = data;

  ResBadWeatherBuilder();

  ResBadWeatherBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _message = _$v.message;
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResBadWeather other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResBadWeather;
  }

  @override
  void update(void Function(ResBadWeatherBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResBadWeather build() {
    _$ResBadWeather _$result;
    try {
      _$result = _$v ??
          new _$ResBadWeather._(
              status: status, message: message, data: data.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        data.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResBadWeather', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
