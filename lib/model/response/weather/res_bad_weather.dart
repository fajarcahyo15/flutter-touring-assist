library res_bad_weather;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/weather/data_bad_weather.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_bad_weather.g.dart';

abstract class ResBadWeather implements Built<ResBadWeather, ResBadWeatherBuilder> {
  ResBadWeather._();

  factory ResBadWeather([updates(ResBadWeatherBuilder b)]) = _$ResBadWeather;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @BuiltValueField(wireName: 'data')
  DataBadWeather get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResBadWeather.serializer, this));
  }

  static ResBadWeather fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResBadWeather.serializer, json.decode(jsonString));
  }

  static Serializer<ResBadWeather> get serializer => _$resBadWeatherSerializer;
}
