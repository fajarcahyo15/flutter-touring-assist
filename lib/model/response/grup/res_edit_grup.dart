library res_edit_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_detail_grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_edit_grup.g.dart';

abstract class ResEditGrup implements Built<ResEditGrup, ResEditGrupBuilder> {
  ResEditGrup._();

  factory ResEditGrup([updates(ResEditGrupBuilder b)]) = _$ResEditGrup;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataDetailGrup get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResEditGrup.serializer, this));
  }

  static ResEditGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResEditGrup.serializer, json.decode(jsonString));
  }

  static Serializer<ResEditGrup> get serializer => _$resEditGrupSerializer;
}
