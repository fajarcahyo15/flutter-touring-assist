library res_buat_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_buat_grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_buat_grup.g.dart';

abstract class ResBuatGrup implements Built<ResBuatGrup, ResBuatGrupBuilder> {
  ResBuatGrup._();

  factory ResBuatGrup([updates(ResBuatGrupBuilder b)]) = _$ResBuatGrup;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataBuatGrup get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResBuatGrup.serializer, this));
  }

  static ResBuatGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResBuatGrup.serializer, json.decode(jsonString));
  }

  static Serializer<ResBuatGrup> get serializer => _$resBuatGrupSerializer;
}
