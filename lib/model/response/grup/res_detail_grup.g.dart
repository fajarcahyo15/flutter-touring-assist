// GENERATED CODE - DO NOT MODIFY BY HAND

part of res_detail_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResDetailGrup> _$resDetailGrupSerializer =
    new _$ResDetailGrupSerializer();

class _$ResDetailGrupSerializer implements StructuredSerializer<ResDetailGrup> {
  @override
  final Iterable<Type> types = const [ResDetailGrup, _$ResDetailGrup];
  @override
  final String wireName = 'ResDetailGrup';

  @override
  Iterable<Object> serialize(Serializers serializers, ResDetailGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(bool)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];
    if (object.data != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.data,
            specifiedType: const FullType(DataDetailGrup)));
    }
    return result;
  }

  @override
  ResDetailGrup deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResDetailGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data':
          result.data.replace(serializers.deserialize(value,
              specifiedType: const FullType(DataDetailGrup)) as DataDetailGrup);
          break;
      }
    }

    return result.build();
  }
}

class _$ResDetailGrup extends ResDetailGrup {
  @override
  final bool status;
  @override
  final String message;
  @override
  final DataDetailGrup data;

  factory _$ResDetailGrup([void Function(ResDetailGrupBuilder) updates]) =>
      (new ResDetailGrupBuilder()..update(updates)).build();

  _$ResDetailGrup._({this.status, this.message, this.data}) : super._() {
    if (status == null) {
      throw new BuiltValueNullFieldError('ResDetailGrup', 'status');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('ResDetailGrup', 'message');
    }
  }

  @override
  ResDetailGrup rebuild(void Function(ResDetailGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResDetailGrupBuilder toBuilder() => new ResDetailGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResDetailGrup &&
        status == other.status &&
        message == other.message &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, status.hashCode), message.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResDetailGrup')
          ..add('status', status)
          ..add('message', message)
          ..add('data', data))
        .toString();
  }
}

class ResDetailGrupBuilder
    implements Builder<ResDetailGrup, ResDetailGrupBuilder> {
  _$ResDetailGrup _$v;

  bool _status;
  bool get status => _$this._status;
  set status(bool status) => _$this._status = status;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  DataDetailGrupBuilder _data;
  DataDetailGrupBuilder get data =>
      _$this._data ??= new DataDetailGrupBuilder();
  set data(DataDetailGrupBuilder data) => _$this._data = data;

  ResDetailGrupBuilder();

  ResDetailGrupBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _message = _$v.message;
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResDetailGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResDetailGrup;
  }

  @override
  void update(void Function(ResDetailGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResDetailGrup build() {
    _$ResDetailGrup _$result;
    try {
      _$result = _$v ??
          new _$ResDetailGrup._(
              status: status, message: message, data: _data?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        _data?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResDetailGrup', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
