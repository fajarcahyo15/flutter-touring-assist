library data_buat_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_buat_grup.g.dart';

abstract class DataBuatGrup implements Built<DataBuatGrup, DataBuatGrupBuilder> {
  DataBuatGrup._();

  factory DataBuatGrup([updates(DataBuatGrupBuilder b)]) = _$DataBuatGrup;

  @BuiltValueField(wireName: 'grup')
  Grup get grup;

  // @BuiltValueField(wireName: 'member')
  // Member get member;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataBuatGrup.serializer, this));
  }

  static DataBuatGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataBuatGrup.serializer, json.decode(jsonString));
  }

  static Serializer<DataBuatGrup> get serializer => _$dataBuatGrupSerializer;
}
