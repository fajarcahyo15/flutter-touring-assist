// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_detail_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataDetailGrup> _$dataDetailGrupSerializer =
    new _$DataDetailGrupSerializer();

class _$DataDetailGrupSerializer
    implements StructuredSerializer<DataDetailGrup> {
  @override
  final Iterable<Type> types = const [DataDetailGrup, _$DataDetailGrup];
  @override
  final String wireName = 'DataDetailGrup';

  @override
  Iterable<Object> serialize(Serializers serializers, DataDetailGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'grup',
      serializers.serialize(object.grup, specifiedType: const FullType(Grup)),
    ];

    return result;
  }

  @override
  DataDetailGrup deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataDetailGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'grup':
          result.grup.replace(serializers.deserialize(value,
              specifiedType: const FullType(Grup)) as Grup);
          break;
      }
    }

    return result.build();
  }
}

class _$DataDetailGrup extends DataDetailGrup {
  @override
  final Grup grup;

  factory _$DataDetailGrup([void Function(DataDetailGrupBuilder) updates]) =>
      (new DataDetailGrupBuilder()..update(updates)).build();

  _$DataDetailGrup._({this.grup}) : super._() {
    if (grup == null) {
      throw new BuiltValueNullFieldError('DataDetailGrup', 'grup');
    }
  }

  @override
  DataDetailGrup rebuild(void Function(DataDetailGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataDetailGrupBuilder toBuilder() =>
      new DataDetailGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataDetailGrup && grup == other.grup;
  }

  @override
  int get hashCode {
    return $jf($jc(0, grup.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataDetailGrup')..add('grup', grup))
        .toString();
  }
}

class DataDetailGrupBuilder
    implements Builder<DataDetailGrup, DataDetailGrupBuilder> {
  _$DataDetailGrup _$v;

  GrupBuilder _grup;
  GrupBuilder get grup => _$this._grup ??= new GrupBuilder();
  set grup(GrupBuilder grup) => _$this._grup = grup;

  DataDetailGrupBuilder();

  DataDetailGrupBuilder get _$this {
    if (_$v != null) {
      _grup = _$v.grup?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataDetailGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataDetailGrup;
  }

  @override
  void update(void Function(DataDetailGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataDetailGrup build() {
    _$DataDetailGrup _$result;
    try {
      _$result = _$v ?? new _$DataDetailGrup._(grup: grup.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'grup';
        grup.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataDetailGrup', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
