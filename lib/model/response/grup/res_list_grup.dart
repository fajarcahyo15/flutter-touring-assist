library res_list_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_list_grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_list_grup.g.dart';

abstract class ResListGrup implements Built<ResListGrup, ResListGrupBuilder> {
  ResListGrup._();

  factory ResListGrup([updates(ResListGrupBuilder b)]) = _$ResListGrup;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataListGrup get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResListGrup.serializer, this));
  }

  static ResListGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResListGrup.serializer, json.decode(jsonString));
  }

  static Serializer<ResListGrup> get serializer => _$resListGrupSerializer;
}
