// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_list_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataListGrup> _$dataListGrupSerializer =
    new _$DataListGrupSerializer();

class _$DataListGrupSerializer implements StructuredSerializer<DataListGrup> {
  @override
  final Iterable<Type> types = const [DataListGrup, _$DataListGrup];
  @override
  final String wireName = 'DataListGrup';

  @override
  Iterable<Object> serialize(Serializers serializers, DataListGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'list_grup',
      serializers.serialize(object.listGrup,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Grup)])),
    ];

    return result;
  }

  @override
  DataListGrup deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataListGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'list_grup':
          result.listGrup.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Grup)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$DataListGrup extends DataListGrup {
  @override
  final BuiltList<Grup> listGrup;

  factory _$DataListGrup([void Function(DataListGrupBuilder) updates]) =>
      (new DataListGrupBuilder()..update(updates)).build();

  _$DataListGrup._({this.listGrup}) : super._() {
    if (listGrup == null) {
      throw new BuiltValueNullFieldError('DataListGrup', 'listGrup');
    }
  }

  @override
  DataListGrup rebuild(void Function(DataListGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataListGrupBuilder toBuilder() => new DataListGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataListGrup && listGrup == other.listGrup;
  }

  @override
  int get hashCode {
    return $jf($jc(0, listGrup.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataListGrup')
          ..add('listGrup', listGrup))
        .toString();
  }
}

class DataListGrupBuilder
    implements Builder<DataListGrup, DataListGrupBuilder> {
  _$DataListGrup _$v;

  ListBuilder<Grup> _listGrup;
  ListBuilder<Grup> get listGrup =>
      _$this._listGrup ??= new ListBuilder<Grup>();
  set listGrup(ListBuilder<Grup> listGrup) => _$this._listGrup = listGrup;

  DataListGrupBuilder();

  DataListGrupBuilder get _$this {
    if (_$v != null) {
      _listGrup = _$v.listGrup?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataListGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataListGrup;
  }

  @override
  void update(void Function(DataListGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataListGrup build() {
    _$DataListGrup _$result;
    try {
      _$result = _$v ?? new _$DataListGrup._(listGrup: listGrup.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'listGrup';
        listGrup.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataListGrup', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
