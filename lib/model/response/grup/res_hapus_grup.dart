library res_hapus_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_hapus_grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_hapus_grup.g.dart';

abstract class ResHapusGrup implements Built<ResHapusGrup, ResHapusGrupBuilder> {
  ResHapusGrup._();

  factory ResHapusGrup([updates(ResHapusGrupBuilder b)]) = _$ResHapusGrup;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataHapusGrup get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResHapusGrup.serializer, this));
  }

  static ResHapusGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResHapusGrup.serializer, json.decode(jsonString));
  }

  static Serializer<ResHapusGrup> get serializer => _$resHapusGrupSerializer;
}
