library data_hapus_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_hapus_grup.g.dart';

abstract class DataHapusGrup implements Built<DataHapusGrup, DataHapusGrupBuilder> {
  DataHapusGrup._();

  factory DataHapusGrup([updates(DataHapusGrupBuilder b)]) = _$DataHapusGrup;

  @BuiltValueField(wireName: 'id_grup')
  String get idGrup;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataHapusGrup.serializer, this));
  }

  static DataHapusGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataHapusGrup.serializer, json.decode(jsonString));
  }

  static Serializer<DataHapusGrup> get serializer => _$dataHapusGrupSerializer;
}
