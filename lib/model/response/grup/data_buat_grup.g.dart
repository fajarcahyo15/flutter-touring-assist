// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_buat_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataBuatGrup> _$dataBuatGrupSerializer =
    new _$DataBuatGrupSerializer();

class _$DataBuatGrupSerializer implements StructuredSerializer<DataBuatGrup> {
  @override
  final Iterable<Type> types = const [DataBuatGrup, _$DataBuatGrup];
  @override
  final String wireName = 'DataBuatGrup';

  @override
  Iterable<Object> serialize(Serializers serializers, DataBuatGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'grup',
      serializers.serialize(object.grup, specifiedType: const FullType(Grup)),
    ];

    return result;
  }

  @override
  DataBuatGrup deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataBuatGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'grup':
          result.grup.replace(serializers.deserialize(value,
              specifiedType: const FullType(Grup)) as Grup);
          break;
      }
    }

    return result.build();
  }
}

class _$DataBuatGrup extends DataBuatGrup {
  @override
  final Grup grup;

  factory _$DataBuatGrup([void Function(DataBuatGrupBuilder) updates]) =>
      (new DataBuatGrupBuilder()..update(updates)).build();

  _$DataBuatGrup._({this.grup}) : super._() {
    if (grup == null) {
      throw new BuiltValueNullFieldError('DataBuatGrup', 'grup');
    }
  }

  @override
  DataBuatGrup rebuild(void Function(DataBuatGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataBuatGrupBuilder toBuilder() => new DataBuatGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataBuatGrup && grup == other.grup;
  }

  @override
  int get hashCode {
    return $jf($jc(0, grup.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataBuatGrup')..add('grup', grup))
        .toString();
  }
}

class DataBuatGrupBuilder
    implements Builder<DataBuatGrup, DataBuatGrupBuilder> {
  _$DataBuatGrup _$v;

  GrupBuilder _grup;
  GrupBuilder get grup => _$this._grup ??= new GrupBuilder();
  set grup(GrupBuilder grup) => _$this._grup = grup;

  DataBuatGrupBuilder();

  DataBuatGrupBuilder get _$this {
    if (_$v != null) {
      _grup = _$v.grup?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataBuatGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataBuatGrup;
  }

  @override
  void update(void Function(DataBuatGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataBuatGrup build() {
    _$DataBuatGrup _$result;
    try {
      _$result = _$v ?? new _$DataBuatGrup._(grup: grup.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'grup';
        grup.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataBuatGrup', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
