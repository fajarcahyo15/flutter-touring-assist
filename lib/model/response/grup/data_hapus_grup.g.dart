// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_hapus_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataHapusGrup> _$dataHapusGrupSerializer =
    new _$DataHapusGrupSerializer();

class _$DataHapusGrupSerializer implements StructuredSerializer<DataHapusGrup> {
  @override
  final Iterable<Type> types = const [DataHapusGrup, _$DataHapusGrup];
  @override
  final String wireName = 'DataHapusGrup';

  @override
  Iterable<Object> serialize(Serializers serializers, DataHapusGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id_grup',
      serializers.serialize(object.idGrup,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  DataHapusGrup deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataHapusGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_grup':
          result.idGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$DataHapusGrup extends DataHapusGrup {
  @override
  final String idGrup;

  factory _$DataHapusGrup([void Function(DataHapusGrupBuilder) updates]) =>
      (new DataHapusGrupBuilder()..update(updates)).build();

  _$DataHapusGrup._({this.idGrup}) : super._() {
    if (idGrup == null) {
      throw new BuiltValueNullFieldError('DataHapusGrup', 'idGrup');
    }
  }

  @override
  DataHapusGrup rebuild(void Function(DataHapusGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataHapusGrupBuilder toBuilder() => new DataHapusGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataHapusGrup && idGrup == other.idGrup;
  }

  @override
  int get hashCode {
    return $jf($jc(0, idGrup.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataHapusGrup')..add('idGrup', idGrup))
        .toString();
  }
}

class DataHapusGrupBuilder
    implements Builder<DataHapusGrup, DataHapusGrupBuilder> {
  _$DataHapusGrup _$v;

  String _idGrup;
  String get idGrup => _$this._idGrup;
  set idGrup(String idGrup) => _$this._idGrup = idGrup;

  DataHapusGrupBuilder();

  DataHapusGrupBuilder get _$this {
    if (_$v != null) {
      _idGrup = _$v.idGrup;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataHapusGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataHapusGrup;
  }

  @override
  void update(void Function(DataHapusGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataHapusGrup build() {
    final _$result = _$v ?? new _$DataHapusGrup._(idGrup: idGrup);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
