library res_detail_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_detail_grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_detail_grup.g.dart';

abstract class ResDetailGrup implements Built<ResDetailGrup, ResDetailGrupBuilder> {
  ResDetailGrup._();

  factory ResDetailGrup([updates(ResDetailGrupBuilder b)]) = _$ResDetailGrup;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataDetailGrup get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResDetailGrup.serializer, this));
  }

  static ResDetailGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResDetailGrup.serializer, json.decode(jsonString));
  }

  static Serializer<ResDetailGrup> get serializer => _$resDetailGrupSerializer;
}
