library data_detail_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_detail_grup.g.dart';

abstract class DataDetailGrup implements Built<DataDetailGrup, DataDetailGrupBuilder> {
  DataDetailGrup._();

  factory DataDetailGrup([updates(DataDetailGrupBuilder b)]) = _$DataDetailGrup;

  @BuiltValueField(wireName: 'grup')
  Grup get grup;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataDetailGrup.serializer, this));
  }

  static DataDetailGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataDetailGrup.serializer, json.decode(jsonString));
  }

  static Serializer<DataDetailGrup> get serializer => _$dataDetailGrupSerializer;
}
