library data_list_grup;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_list_grup.g.dart';

abstract class DataListGrup implements Built<DataListGrup, DataListGrupBuilder> {
  DataListGrup._();

  factory DataListGrup([updates(DataListGrupBuilder b)]) = _$DataListGrup;

  @BuiltValueField(wireName: 'list_grup')
  BuiltList<Grup> get listGrup;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataListGrup.serializer, this));
  }

  static DataListGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataListGrup.serializer, json.decode(jsonString));
  }

  static Serializer<DataListGrup> get serializer => _$dataListGrupSerializer;
}
