// GENERATED CODE - DO NOT MODIFY BY HAND

part of res_hapus_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResHapusGrup> _$resHapusGrupSerializer =
    new _$ResHapusGrupSerializer();

class _$ResHapusGrupSerializer implements StructuredSerializer<ResHapusGrup> {
  @override
  final Iterable<Type> types = const [ResHapusGrup, _$ResHapusGrup];
  @override
  final String wireName = 'ResHapusGrup';

  @override
  Iterable<Object> serialize(Serializers serializers, ResHapusGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(bool)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];
    if (object.data != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.data,
            specifiedType: const FullType(DataHapusGrup)));
    }
    return result;
  }

  @override
  ResHapusGrup deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResHapusGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data':
          result.data.replace(serializers.deserialize(value,
              specifiedType: const FullType(DataHapusGrup)) as DataHapusGrup);
          break;
      }
    }

    return result.build();
  }
}

class _$ResHapusGrup extends ResHapusGrup {
  @override
  final bool status;
  @override
  final String message;
  @override
  final DataHapusGrup data;

  factory _$ResHapusGrup([void Function(ResHapusGrupBuilder) updates]) =>
      (new ResHapusGrupBuilder()..update(updates)).build();

  _$ResHapusGrup._({this.status, this.message, this.data}) : super._() {
    if (status == null) {
      throw new BuiltValueNullFieldError('ResHapusGrup', 'status');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('ResHapusGrup', 'message');
    }
  }

  @override
  ResHapusGrup rebuild(void Function(ResHapusGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResHapusGrupBuilder toBuilder() => new ResHapusGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResHapusGrup &&
        status == other.status &&
        message == other.message &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, status.hashCode), message.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResHapusGrup')
          ..add('status', status)
          ..add('message', message)
          ..add('data', data))
        .toString();
  }
}

class ResHapusGrupBuilder
    implements Builder<ResHapusGrup, ResHapusGrupBuilder> {
  _$ResHapusGrup _$v;

  bool _status;
  bool get status => _$this._status;
  set status(bool status) => _$this._status = status;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  DataHapusGrupBuilder _data;
  DataHapusGrupBuilder get data => _$this._data ??= new DataHapusGrupBuilder();
  set data(DataHapusGrupBuilder data) => _$this._data = data;

  ResHapusGrupBuilder();

  ResHapusGrupBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _message = _$v.message;
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResHapusGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResHapusGrup;
  }

  @override
  void update(void Function(ResHapusGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResHapusGrup build() {
    _$ResHapusGrup _$result;
    try {
      _$result = _$v ??
          new _$ResHapusGrup._(
              status: status, message: message, data: _data?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        _data?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResHapusGrup', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
