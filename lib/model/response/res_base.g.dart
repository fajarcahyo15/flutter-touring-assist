// GENERATED CODE - DO NOT MODIFY BY HAND

part of res_base;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResBase> _$resBaseSerializer = new _$ResBaseSerializer();

class _$ResBaseSerializer implements StructuredSerializer<ResBase> {
  @override
  final Iterable<Type> types = const [ResBase, _$ResBase];
  @override
  final String wireName = 'ResBase';

  @override
  Iterable<Object> serialize(Serializers serializers, ResBase object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(bool)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  ResBase deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResBaseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ResBase extends ResBase {
  @override
  final bool status;
  @override
  final String message;

  factory _$ResBase([void Function(ResBaseBuilder) updates]) =>
      (new ResBaseBuilder()..update(updates)).build();

  _$ResBase._({this.status, this.message}) : super._() {
    if (status == null) {
      throw new BuiltValueNullFieldError('ResBase', 'status');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('ResBase', 'message');
    }
  }

  @override
  ResBase rebuild(void Function(ResBaseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResBaseBuilder toBuilder() => new ResBaseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResBase &&
        status == other.status &&
        message == other.message;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, status.hashCode), message.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResBase')
          ..add('status', status)
          ..add('message', message))
        .toString();
  }
}

class ResBaseBuilder implements Builder<ResBase, ResBaseBuilder> {
  _$ResBase _$v;

  bool _status;
  bool get status => _$this._status;
  set status(bool status) => _$this._status = status;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  ResBaseBuilder();

  ResBaseBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _message = _$v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResBase other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResBase;
  }

  @override
  void update(void Function(ResBaseBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResBase build() {
    final _$result = _$v ?? new _$ResBase._(status: status, message: message);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
