// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_detail_anggota;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataDetailAnggota> _$dataDetailAnggotaSerializer =
    new _$DataDetailAnggotaSerializer();

class _$DataDetailAnggotaSerializer
    implements StructuredSerializer<DataDetailAnggota> {
  @override
  final Iterable<Type> types = const [DataDetailAnggota, _$DataDetailAnggota];
  @override
  final String wireName = 'DataDetailAnggota';

  @override
  Iterable<Object> serialize(Serializers serializers, DataDetailAnggota object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'anggota',
      serializers.serialize(object.anggota,
          specifiedType: const FullType(Anggota)),
    ];

    return result;
  }

  @override
  DataDetailAnggota deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataDetailAnggotaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'anggota':
          result.anggota.replace(serializers.deserialize(value,
              specifiedType: const FullType(Anggota)) as Anggota);
          break;
      }
    }

    return result.build();
  }
}

class _$DataDetailAnggota extends DataDetailAnggota {
  @override
  final Anggota anggota;

  factory _$DataDetailAnggota(
          [void Function(DataDetailAnggotaBuilder) updates]) =>
      (new DataDetailAnggotaBuilder()..update(updates)).build();

  _$DataDetailAnggota._({this.anggota}) : super._() {
    if (anggota == null) {
      throw new BuiltValueNullFieldError('DataDetailAnggota', 'anggota');
    }
  }

  @override
  DataDetailAnggota rebuild(void Function(DataDetailAnggotaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataDetailAnggotaBuilder toBuilder() =>
      new DataDetailAnggotaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataDetailAnggota && anggota == other.anggota;
  }

  @override
  int get hashCode {
    return $jf($jc(0, anggota.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataDetailAnggota')
          ..add('anggota', anggota))
        .toString();
  }
}

class DataDetailAnggotaBuilder
    implements Builder<DataDetailAnggota, DataDetailAnggotaBuilder> {
  _$DataDetailAnggota _$v;

  AnggotaBuilder _anggota;
  AnggotaBuilder get anggota => _$this._anggota ??= new AnggotaBuilder();
  set anggota(AnggotaBuilder anggota) => _$this._anggota = anggota;

  DataDetailAnggotaBuilder();

  DataDetailAnggotaBuilder get _$this {
    if (_$v != null) {
      _anggota = _$v.anggota?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataDetailAnggota other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataDetailAnggota;
  }

  @override
  void update(void Function(DataDetailAnggotaBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataDetailAnggota build() {
    _$DataDetailAnggota _$result;
    try {
      _$result = _$v ?? new _$DataDetailAnggota._(anggota: anggota.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'anggota';
        anggota.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataDetailAnggota', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
