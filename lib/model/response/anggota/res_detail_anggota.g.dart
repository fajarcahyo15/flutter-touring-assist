// GENERATED CODE - DO NOT MODIFY BY HAND

part of res_detail_anggota;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResDetailAnggota> _$resDetailAnggotaSerializer =
    new _$ResDetailAnggotaSerializer();

class _$ResDetailAnggotaSerializer
    implements StructuredSerializer<ResDetailAnggota> {
  @override
  final Iterable<Type> types = const [ResDetailAnggota, _$ResDetailAnggota];
  @override
  final String wireName = 'ResDetailAnggota';

  @override
  Iterable<Object> serialize(Serializers serializers, ResDetailAnggota object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(bool)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];
    if (object.data != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.data,
            specifiedType: const FullType(DataDetailAnggota)));
    }
    return result;
  }

  @override
  ResDetailAnggota deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResDetailAnggotaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data':
          result.data.replace(serializers.deserialize(value,
                  specifiedType: const FullType(DataDetailAnggota))
              as DataDetailAnggota);
          break;
      }
    }

    return result.build();
  }
}

class _$ResDetailAnggota extends ResDetailAnggota {
  @override
  final bool status;
  @override
  final String message;
  @override
  final DataDetailAnggota data;

  factory _$ResDetailAnggota(
          [void Function(ResDetailAnggotaBuilder) updates]) =>
      (new ResDetailAnggotaBuilder()..update(updates)).build();

  _$ResDetailAnggota._({this.status, this.message, this.data}) : super._() {
    if (status == null) {
      throw new BuiltValueNullFieldError('ResDetailAnggota', 'status');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('ResDetailAnggota', 'message');
    }
  }

  @override
  ResDetailAnggota rebuild(void Function(ResDetailAnggotaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResDetailAnggotaBuilder toBuilder() =>
      new ResDetailAnggotaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResDetailAnggota &&
        status == other.status &&
        message == other.message &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, status.hashCode), message.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResDetailAnggota')
          ..add('status', status)
          ..add('message', message)
          ..add('data', data))
        .toString();
  }
}

class ResDetailAnggotaBuilder
    implements Builder<ResDetailAnggota, ResDetailAnggotaBuilder> {
  _$ResDetailAnggota _$v;

  bool _status;
  bool get status => _$this._status;
  set status(bool status) => _$this._status = status;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  DataDetailAnggotaBuilder _data;
  DataDetailAnggotaBuilder get data =>
      _$this._data ??= new DataDetailAnggotaBuilder();
  set data(DataDetailAnggotaBuilder data) => _$this._data = data;

  ResDetailAnggotaBuilder();

  ResDetailAnggotaBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _message = _$v.message;
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResDetailAnggota other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResDetailAnggota;
  }

  @override
  void update(void Function(ResDetailAnggotaBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResDetailAnggota build() {
    _$ResDetailAnggota _$result;
    try {
      _$result = _$v ??
          new _$ResDetailAnggota._(
              status: status, message: message, data: _data?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        _data?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResDetailAnggota', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
