library data_detail_anggota;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/anggota.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_detail_anggota.g.dart';

abstract class DataDetailAnggota implements Built<DataDetailAnggota, DataDetailAnggotaBuilder> {
  DataDetailAnggota._();

  factory DataDetailAnggota([updates(DataDetailAnggotaBuilder b)]) = _$DataDetailAnggota;

  @BuiltValueField(wireName: 'anggota')
  Anggota get anggota;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataDetailAnggota.serializer, this));
  }

  static DataDetailAnggota fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataDetailAnggota.serializer, json.decode(jsonString));
  }

  static Serializer<DataDetailAnggota> get serializer => _$dataDetailAnggotaSerializer;
}
