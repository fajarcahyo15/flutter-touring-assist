library res_detail_anggota;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/anggota/data_detail_anggota.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_detail_anggota.g.dart';

abstract class ResDetailAnggota implements Built<ResDetailAnggota, ResDetailAnggotaBuilder> {
  ResDetailAnggota._();

  factory ResDetailAnggota([updates(ResDetailAnggotaBuilder b)]) = _$ResDetailAnggota;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataDetailAnggota get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResDetailAnggota.serializer, this));
  }

  static ResDetailAnggota fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResDetailAnggota.serializer, json.decode(jsonString));
  }

  static Serializer<ResDetailAnggota> get serializer => _$resDetailAnggotaSerializer;
}
