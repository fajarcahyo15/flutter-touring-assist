// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_list_anggota_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataListAnggotaGrup> _$dataListAnggotaGrupSerializer =
    new _$DataListAnggotaGrupSerializer();

class _$DataListAnggotaGrupSerializer
    implements StructuredSerializer<DataListAnggotaGrup> {
  @override
  final Iterable<Type> types = const [
    DataListAnggotaGrup,
    _$DataListAnggotaGrup
  ];
  @override
  final String wireName = 'DataListAnggotaGrup';

  @override
  Iterable<Object> serialize(
      Serializers serializers, DataListAnggotaGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.listAnggotaGrup != null) {
      result
        ..add('list_anggota_grup')
        ..add(serializers.serialize(object.listAnggotaGrup,
            specifiedType: const FullType(
                BuiltList, const [const FullType(AnggotaGrup)])));
    }
    return result;
  }

  @override
  DataListAnggotaGrup deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataListAnggotaGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'list_anggota_grup':
          result.listAnggotaGrup.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(AnggotaGrup)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$DataListAnggotaGrup extends DataListAnggotaGrup {
  @override
  final BuiltList<AnggotaGrup> listAnggotaGrup;

  factory _$DataListAnggotaGrup(
          [void Function(DataListAnggotaGrupBuilder) updates]) =>
      (new DataListAnggotaGrupBuilder()..update(updates)).build();

  _$DataListAnggotaGrup._({this.listAnggotaGrup}) : super._();

  @override
  DataListAnggotaGrup rebuild(
          void Function(DataListAnggotaGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataListAnggotaGrupBuilder toBuilder() =>
      new DataListAnggotaGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataListAnggotaGrup &&
        listAnggotaGrup == other.listAnggotaGrup;
  }

  @override
  int get hashCode {
    return $jf($jc(0, listAnggotaGrup.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataListAnggotaGrup')
          ..add('listAnggotaGrup', listAnggotaGrup))
        .toString();
  }
}

class DataListAnggotaGrupBuilder
    implements Builder<DataListAnggotaGrup, DataListAnggotaGrupBuilder> {
  _$DataListAnggotaGrup _$v;

  ListBuilder<AnggotaGrup> _listAnggotaGrup;
  ListBuilder<AnggotaGrup> get listAnggotaGrup =>
      _$this._listAnggotaGrup ??= new ListBuilder<AnggotaGrup>();
  set listAnggotaGrup(ListBuilder<AnggotaGrup> listAnggotaGrup) =>
      _$this._listAnggotaGrup = listAnggotaGrup;

  DataListAnggotaGrupBuilder();

  DataListAnggotaGrupBuilder get _$this {
    if (_$v != null) {
      _listAnggotaGrup = _$v.listAnggotaGrup?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataListAnggotaGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataListAnggotaGrup;
  }

  @override
  void update(void Function(DataListAnggotaGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataListAnggotaGrup build() {
    _$DataListAnggotaGrup _$result;
    try {
      _$result = _$v ??
          new _$DataListAnggotaGrup._(
              listAnggotaGrup: _listAnggotaGrup?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'listAnggotaGrup';
        _listAnggotaGrup?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataListAnggotaGrup', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
