library res_list_anggota_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/anggota_grup/data_list_anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_list_anggota_grup.g.dart';

abstract class ResListAnggotaGrup implements Built<ResListAnggotaGrup, ResListAnggotaGrupBuilder> {
  ResListAnggotaGrup._();

  factory ResListAnggotaGrup([updates(ResListAnggotaGrupBuilder b)]) = _$ResListAnggotaGrup;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataListAnggotaGrup get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResListAnggotaGrup.serializer, this));
  }

  static ResListAnggotaGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResListAnggotaGrup.serializer, json.decode(jsonString));
  }

  static Serializer<ResListAnggotaGrup> get serializer => _$resListAnggotaGrupSerializer;
}
