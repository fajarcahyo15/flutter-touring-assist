// GENERATED CODE - DO NOT MODIFY BY HAND

part of res_list_anggota_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResListAnggotaGrup> _$resListAnggotaGrupSerializer =
    new _$ResListAnggotaGrupSerializer();

class _$ResListAnggotaGrupSerializer
    implements StructuredSerializer<ResListAnggotaGrup> {
  @override
  final Iterable<Type> types = const [ResListAnggotaGrup, _$ResListAnggotaGrup];
  @override
  final String wireName = 'ResListAnggotaGrup';

  @override
  Iterable<Object> serialize(Serializers serializers, ResListAnggotaGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(bool)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];
    if (object.data != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.data,
            specifiedType: const FullType(DataListAnggotaGrup)));
    }
    return result;
  }

  @override
  ResListAnggotaGrup deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResListAnggotaGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data':
          result.data.replace(serializers.deserialize(value,
                  specifiedType: const FullType(DataListAnggotaGrup))
              as DataListAnggotaGrup);
          break;
      }
    }

    return result.build();
  }
}

class _$ResListAnggotaGrup extends ResListAnggotaGrup {
  @override
  final bool status;
  @override
  final String message;
  @override
  final DataListAnggotaGrup data;

  factory _$ResListAnggotaGrup(
          [void Function(ResListAnggotaGrupBuilder) updates]) =>
      (new ResListAnggotaGrupBuilder()..update(updates)).build();

  _$ResListAnggotaGrup._({this.status, this.message, this.data}) : super._() {
    if (status == null) {
      throw new BuiltValueNullFieldError('ResListAnggotaGrup', 'status');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('ResListAnggotaGrup', 'message');
    }
  }

  @override
  ResListAnggotaGrup rebuild(
          void Function(ResListAnggotaGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResListAnggotaGrupBuilder toBuilder() =>
      new ResListAnggotaGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResListAnggotaGrup &&
        status == other.status &&
        message == other.message &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, status.hashCode), message.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResListAnggotaGrup')
          ..add('status', status)
          ..add('message', message)
          ..add('data', data))
        .toString();
  }
}

class ResListAnggotaGrupBuilder
    implements Builder<ResListAnggotaGrup, ResListAnggotaGrupBuilder> {
  _$ResListAnggotaGrup _$v;

  bool _status;
  bool get status => _$this._status;
  set status(bool status) => _$this._status = status;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  DataListAnggotaGrupBuilder _data;
  DataListAnggotaGrupBuilder get data =>
      _$this._data ??= new DataListAnggotaGrupBuilder();
  set data(DataListAnggotaGrupBuilder data) => _$this._data = data;

  ResListAnggotaGrupBuilder();

  ResListAnggotaGrupBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _message = _$v.message;
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResListAnggotaGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResListAnggotaGrup;
  }

  @override
  void update(void Function(ResListAnggotaGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResListAnggotaGrup build() {
    _$ResListAnggotaGrup _$result;
    try {
      _$result = _$v ??
          new _$ResListAnggotaGrup._(
              status: status, message: message, data: _data?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        _data?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResListAnggotaGrup', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
