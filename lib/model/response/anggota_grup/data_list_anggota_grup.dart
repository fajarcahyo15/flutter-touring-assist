library data_list_anggota_grup;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_list_anggota_grup.g.dart';

abstract class DataListAnggotaGrup implements Built<DataListAnggotaGrup, DataListAnggotaGrupBuilder> {
  DataListAnggotaGrup._();

  factory DataListAnggotaGrup([updates(DataListAnggotaGrupBuilder b)]) = _$DataListAnggotaGrup;

  @nullable
  @BuiltValueField(wireName: 'list_anggota_grup')
  BuiltList<AnggotaGrup> get listAnggotaGrup;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataListAnggotaGrup.serializer, this));
  }

  static DataListAnggotaGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataListAnggotaGrup.serializer, json.decode(jsonString));
  }

  static Serializer<DataListAnggotaGrup> get serializer => _$dataListAnggotaGrupSerializer;
}
