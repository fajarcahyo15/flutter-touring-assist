// GENERATED CODE - DO NOT MODIFY BY HAND

part of polyline;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Polyline> _$polylineSerializer = new _$PolylineSerializer();

class _$PolylineSerializer implements StructuredSerializer<Polyline> {
  @override
  final Iterable<Type> types = const [Polyline, _$Polyline];
  @override
  final String wireName = 'Polyline';

  @override
  Iterable<Object> serialize(Serializers serializers, Polyline object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'points',
      serializers.serialize(object.points,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Polyline deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PolylineBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'points':
          result.points = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Polyline extends Polyline {
  @override
  final String points;

  factory _$Polyline([void Function(PolylineBuilder) updates]) =>
      (new PolylineBuilder()..update(updates)).build();

  _$Polyline._({this.points}) : super._() {
    if (points == null) {
      throw new BuiltValueNullFieldError('Polyline', 'points');
    }
  }

  @override
  Polyline rebuild(void Function(PolylineBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PolylineBuilder toBuilder() => new PolylineBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Polyline && points == other.points;
  }

  @override
  int get hashCode {
    return $jf($jc(0, points.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Polyline')..add('points', points))
        .toString();
  }
}

class PolylineBuilder implements Builder<Polyline, PolylineBuilder> {
  _$Polyline _$v;

  String _points;
  String get points => _$this._points;
  set points(String points) => _$this._points = points;

  PolylineBuilder();

  PolylineBuilder get _$this {
    if (_$v != null) {
      _points = _$v.points;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Polyline other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Polyline;
  }

  @override
  void update(void Function(PolylineBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Polyline build() {
    final _$result = _$v ?? new _$Polyline._(points: points);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
