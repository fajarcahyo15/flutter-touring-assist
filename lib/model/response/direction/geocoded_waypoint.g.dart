// GENERATED CODE - DO NOT MODIFY BY HAND

part of geocoded_waypoint;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GeocodedWaypoint> _$geocodedWaypointSerializer =
    new _$GeocodedWaypointSerializer();

class _$GeocodedWaypointSerializer
    implements StructuredSerializer<GeocodedWaypoint> {
  @override
  final Iterable<Type> types = const [GeocodedWaypoint, _$GeocodedWaypoint];
  @override
  final String wireName = 'GeocodedWaypoint';

  @override
  Iterable<Object> serialize(Serializers serializers, GeocodedWaypoint object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'geocoder_status',
      serializers.serialize(object.geocoderStatus,
          specifiedType: const FullType(String)),
      'place_id',
      serializers.serialize(object.placeId,
          specifiedType: const FullType(String)),
      'types',
      serializers.serialize(object.types,
          specifiedType:
              const FullType(BuiltList, const [const FullType(String)])),
    ];

    return result;
  }

  @override
  GeocodedWaypoint deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GeocodedWaypointBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'geocoder_status':
          result.geocoderStatus = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'place_id':
          result.placeId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'types':
          result.types.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$GeocodedWaypoint extends GeocodedWaypoint {
  @override
  final String geocoderStatus;
  @override
  final String placeId;
  @override
  final BuiltList<String> types;

  factory _$GeocodedWaypoint(
          [void Function(GeocodedWaypointBuilder) updates]) =>
      (new GeocodedWaypointBuilder()..update(updates)).build();

  _$GeocodedWaypoint._({this.geocoderStatus, this.placeId, this.types})
      : super._() {
    if (geocoderStatus == null) {
      throw new BuiltValueNullFieldError('GeocodedWaypoint', 'geocoderStatus');
    }
    if (placeId == null) {
      throw new BuiltValueNullFieldError('GeocodedWaypoint', 'placeId');
    }
    if (types == null) {
      throw new BuiltValueNullFieldError('GeocodedWaypoint', 'types');
    }
  }

  @override
  GeocodedWaypoint rebuild(void Function(GeocodedWaypointBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GeocodedWaypointBuilder toBuilder() =>
      new GeocodedWaypointBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GeocodedWaypoint &&
        geocoderStatus == other.geocoderStatus &&
        placeId == other.placeId &&
        types == other.types;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, geocoderStatus.hashCode), placeId.hashCode),
        types.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GeocodedWaypoint')
          ..add('geocoderStatus', geocoderStatus)
          ..add('placeId', placeId)
          ..add('types', types))
        .toString();
  }
}

class GeocodedWaypointBuilder
    implements Builder<GeocodedWaypoint, GeocodedWaypointBuilder> {
  _$GeocodedWaypoint _$v;

  String _geocoderStatus;
  String get geocoderStatus => _$this._geocoderStatus;
  set geocoderStatus(String geocoderStatus) =>
      _$this._geocoderStatus = geocoderStatus;

  String _placeId;
  String get placeId => _$this._placeId;
  set placeId(String placeId) => _$this._placeId = placeId;

  ListBuilder<String> _types;
  ListBuilder<String> get types => _$this._types ??= new ListBuilder<String>();
  set types(ListBuilder<String> types) => _$this._types = types;

  GeocodedWaypointBuilder();

  GeocodedWaypointBuilder get _$this {
    if (_$v != null) {
      _geocoderStatus = _$v.geocoderStatus;
      _placeId = _$v.placeId;
      _types = _$v.types?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GeocodedWaypoint other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GeocodedWaypoint;
  }

  @override
  void update(void Function(GeocodedWaypointBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GeocodedWaypoint build() {
    _$GeocodedWaypoint _$result;
    try {
      _$result = _$v ??
          new _$GeocodedWaypoint._(
              geocoderStatus: geocoderStatus,
              placeId: placeId,
              types: types.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'types';
        types.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'GeocodedWaypoint', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
