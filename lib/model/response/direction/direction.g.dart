// GENERATED CODE - DO NOT MODIFY BY HAND

part of direction;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Direction> _$directionSerializer = new _$DirectionSerializer();

class _$DirectionSerializer implements StructuredSerializer<Direction> {
  @override
  final Iterable<Type> types = const [Direction, _$Direction];
  @override
  final String wireName = 'Direction';

  @override
  Iterable<Object> serialize(Serializers serializers, Direction object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'geocoded_waypoints',
      serializers.serialize(object.geocodedWaypoints,
          specifiedType: const FullType(
              BuiltList, const [const FullType(GeocodedWaypoint)])),
      'routes',
      serializers.serialize(object.routes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Route)])),
      'status',
      serializers.serialize(object.status,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Direction deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DirectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'geocoded_waypoints':
          result.geocodedWaypoints.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(GeocodedWaypoint)]))
              as BuiltList<dynamic>);
          break;
        case 'routes':
          result.routes.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Route)]))
              as BuiltList<dynamic>);
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Direction extends Direction {
  @override
  final BuiltList<GeocodedWaypoint> geocodedWaypoints;
  @override
  final BuiltList<Route> routes;
  @override
  final String status;

  factory _$Direction([void Function(DirectionBuilder) updates]) =>
      (new DirectionBuilder()..update(updates)).build();

  _$Direction._({this.geocodedWaypoints, this.routes, this.status})
      : super._() {
    if (geocodedWaypoints == null) {
      throw new BuiltValueNullFieldError('Direction', 'geocodedWaypoints');
    }
    if (routes == null) {
      throw new BuiltValueNullFieldError('Direction', 'routes');
    }
    if (status == null) {
      throw new BuiltValueNullFieldError('Direction', 'status');
    }
  }

  @override
  Direction rebuild(void Function(DirectionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DirectionBuilder toBuilder() => new DirectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Direction &&
        geocodedWaypoints == other.geocodedWaypoints &&
        routes == other.routes &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, geocodedWaypoints.hashCode), routes.hashCode),
        status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Direction')
          ..add('geocodedWaypoints', geocodedWaypoints)
          ..add('routes', routes)
          ..add('status', status))
        .toString();
  }
}

class DirectionBuilder implements Builder<Direction, DirectionBuilder> {
  _$Direction _$v;

  ListBuilder<GeocodedWaypoint> _geocodedWaypoints;
  ListBuilder<GeocodedWaypoint> get geocodedWaypoints =>
      _$this._geocodedWaypoints ??= new ListBuilder<GeocodedWaypoint>();
  set geocodedWaypoints(ListBuilder<GeocodedWaypoint> geocodedWaypoints) =>
      _$this._geocodedWaypoints = geocodedWaypoints;

  ListBuilder<Route> _routes;
  ListBuilder<Route> get routes => _$this._routes ??= new ListBuilder<Route>();
  set routes(ListBuilder<Route> routes) => _$this._routes = routes;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  DirectionBuilder();

  DirectionBuilder get _$this {
    if (_$v != null) {
      _geocodedWaypoints = _$v.geocodedWaypoints?.toBuilder();
      _routes = _$v.routes?.toBuilder();
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Direction other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Direction;
  }

  @override
  void update(void Function(DirectionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Direction build() {
    _$Direction _$result;
    try {
      _$result = _$v ??
          new _$Direction._(
              geocodedWaypoints: geocodedWaypoints.build(),
              routes: routes.build(),
              status: status);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'geocodedWaypoints';
        geocodedWaypoints.build();
        _$failedField = 'routes';
        routes.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Direction', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
