library leg;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/direction/distance.dart';
import 'package:new_touring_assist_flutter/model/response/direction/lat_lng.dart';
import 'package:new_touring_assist_flutter/model/response/direction/step.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'leg.g.dart';

abstract class Leg implements Built<Leg, LegBuilder> {
  Leg._();

  factory Leg([updates(LegBuilder b)]) = _$Leg;

  @BuiltValueField(wireName: 'distance')
  Distance get distance;

  @BuiltValueField(wireName: 'duration')
  Distance get duration;

  @BuiltValueField(wireName: 'end_address')
  String get endAddress;

  @BuiltValueField(wireName: 'end_location')
  LatLng get endLocation;

  @BuiltValueField(wireName: 'start_address')
  String get startAddress;

  @BuiltValueField(wireName: 'start_location')
  LatLng get startLocation;

  @BuiltValueField(wireName: 'steps')
  BuiltList<Step> get steps;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Leg.serializer, this));
  }

  static Leg fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Leg.serializer, json.decode(jsonString));
  }

  static Serializer<Leg> get serializer => _$legSerializer;
}
