library step;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/direction/distance.dart';
import 'package:new_touring_assist_flutter/model/response/direction/lat_lng.dart';
import 'package:new_touring_assist_flutter/model/response/direction/polyline.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'step.g.dart';

abstract class Step implements Built<Step, StepBuilder> {
  Step._();

  factory Step([updates(StepBuilder b)]) = _$Step;

  @BuiltValueField(wireName: 'distance')
  Distance get distance;

  @BuiltValueField(wireName: 'duration')
  Distance get duration;

  @BuiltValueField(wireName: 'end_location')
  LatLng get endLocation;

  @BuiltValueField(wireName: 'html_instructions')
  String get htmlInstructions;

  @BuiltValueField(wireName: 'polyline')
  Polyline get polyline;

  @BuiltValueField(wireName: 'start_location')
  LatLng get startLocation;

  @BuiltValueField(wireName: 'travel_mode')
  String get travelMode;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Step.serializer, this));
  }

  static Step fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Step.serializer, json.decode(jsonString));
  }

  static Serializer<Step> get serializer => _$stepSerializer;
}
