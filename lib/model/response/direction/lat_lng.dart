library lat_lng;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'lat_lng.g.dart';

abstract class LatLng implements Built<LatLng, LatLngBuilder> {
  LatLng._();

  factory LatLng([updates(LatLngBuilder b)]) = _$LatLng;

  @BuiltValueField(wireName: 'lat')
  double get lat;

  @nullable
  @BuiltValueField(wireName: 'lng')
  double get lng;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(LatLng.serializer, this));
  }

  static LatLng fromJson(String jsonString) {
    return standardSerializers.deserializeWith(LatLng.serializer, json.decode(jsonString));
  }

  static Serializer<LatLng> get serializer => _$latLngSerializer;
}
