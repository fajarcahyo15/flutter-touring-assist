library geocoded_waypoint;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'geocoded_waypoint.g.dart';

abstract class GeocodedWaypoint implements Built<GeocodedWaypoint, GeocodedWaypointBuilder> {
  GeocodedWaypoint._();

  factory GeocodedWaypoint([updates(GeocodedWaypointBuilder b)]) = _$GeocodedWaypoint;

  @BuiltValueField(wireName: 'geocoder_status')
  String get geocoderStatus;
  @BuiltValueField(wireName: 'place_id')
  String get placeId;
  @BuiltValueField(wireName: 'types')
  BuiltList<String> get types;
  String toJson() {
    return json.encode(standardSerializers.serializeWith(GeocodedWaypoint.serializer, this));
  }

  static GeocodedWaypoint fromJson(String jsonString) {
    return standardSerializers.deserializeWith(GeocodedWaypoint.serializer, json.decode(jsonString));
  }

  static Serializer<GeocodedWaypoint> get serializer => _$geocodedWaypointSerializer;
}
