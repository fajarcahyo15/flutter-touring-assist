library direction;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/direction/geocoded_waypoint.dart';
import 'package:new_touring_assist_flutter/model/response/direction/route.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'direction.g.dart';

abstract class Direction implements Built<Direction, DirectionBuilder> {
  Direction._();

  factory Direction([updates(DirectionBuilder b)]) = _$Direction;

  @BuiltValueField(wireName: 'geocoded_waypoints')
  BuiltList<GeocodedWaypoint> get geocodedWaypoints;

  @BuiltValueField(wireName: 'routes')
  BuiltList<Route> get routes;

  @BuiltValueField(wireName: 'status')
  String get status;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Direction.serializer, this));
  }

  static Direction fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Direction.serializer, json.decode(jsonString));
  }

  static Serializer<Direction> get serializer => _$directionSerializer;
}
