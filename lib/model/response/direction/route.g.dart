// GENERATED CODE - DO NOT MODIFY BY HAND

part of route;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Route> _$routeSerializer = new _$RouteSerializer();

class _$RouteSerializer implements StructuredSerializer<Route> {
  @override
  final Iterable<Type> types = const [Route, _$Route];
  @override
  final String wireName = 'Route';

  @override
  Iterable<Object> serialize(Serializers serializers, Route object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'bounds',
      serializers.serialize(object.bounds,
          specifiedType: const FullType(Bounds)),
      'copyrights',
      serializers.serialize(object.copyrights,
          specifiedType: const FullType(String)),
      'legs',
      serializers.serialize(object.legs,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Leg)])),
      'overview_polyline',
      serializers.serialize(object.overviewPolyline,
          specifiedType: const FullType(Polyline)),
      'summary',
      serializers.serialize(object.summary,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Route deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RouteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'bounds':
          result.bounds.replace(serializers.deserialize(value,
              specifiedType: const FullType(Bounds)) as Bounds);
          break;
        case 'copyrights':
          result.copyrights = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'legs':
          result.legs.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Leg)]))
              as BuiltList<dynamic>);
          break;
        case 'overview_polyline':
          result.overviewPolyline.replace(serializers.deserialize(value,
              specifiedType: const FullType(Polyline)) as Polyline);
          break;
        case 'summary':
          result.summary = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Route extends Route {
  @override
  final Bounds bounds;
  @override
  final String copyrights;
  @override
  final BuiltList<Leg> legs;
  @override
  final Polyline overviewPolyline;
  @override
  final String summary;

  factory _$Route([void Function(RouteBuilder) updates]) =>
      (new RouteBuilder()..update(updates)).build();

  _$Route._(
      {this.bounds,
      this.copyrights,
      this.legs,
      this.overviewPolyline,
      this.summary})
      : super._() {
    if (bounds == null) {
      throw new BuiltValueNullFieldError('Route', 'bounds');
    }
    if (copyrights == null) {
      throw new BuiltValueNullFieldError('Route', 'copyrights');
    }
    if (legs == null) {
      throw new BuiltValueNullFieldError('Route', 'legs');
    }
    if (overviewPolyline == null) {
      throw new BuiltValueNullFieldError('Route', 'overviewPolyline');
    }
    if (summary == null) {
      throw new BuiltValueNullFieldError('Route', 'summary');
    }
  }

  @override
  Route rebuild(void Function(RouteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RouteBuilder toBuilder() => new RouteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Route &&
        bounds == other.bounds &&
        copyrights == other.copyrights &&
        legs == other.legs &&
        overviewPolyline == other.overviewPolyline &&
        summary == other.summary;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, bounds.hashCode), copyrights.hashCode),
                legs.hashCode),
            overviewPolyline.hashCode),
        summary.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Route')
          ..add('bounds', bounds)
          ..add('copyrights', copyrights)
          ..add('legs', legs)
          ..add('overviewPolyline', overviewPolyline)
          ..add('summary', summary))
        .toString();
  }
}

class RouteBuilder implements Builder<Route, RouteBuilder> {
  _$Route _$v;

  BoundsBuilder _bounds;
  BoundsBuilder get bounds => _$this._bounds ??= new BoundsBuilder();
  set bounds(BoundsBuilder bounds) => _$this._bounds = bounds;

  String _copyrights;
  String get copyrights => _$this._copyrights;
  set copyrights(String copyrights) => _$this._copyrights = copyrights;

  ListBuilder<Leg> _legs;
  ListBuilder<Leg> get legs => _$this._legs ??= new ListBuilder<Leg>();
  set legs(ListBuilder<Leg> legs) => _$this._legs = legs;

  PolylineBuilder _overviewPolyline;
  PolylineBuilder get overviewPolyline =>
      _$this._overviewPolyline ??= new PolylineBuilder();
  set overviewPolyline(PolylineBuilder overviewPolyline) =>
      _$this._overviewPolyline = overviewPolyline;

  String _summary;
  String get summary => _$this._summary;
  set summary(String summary) => _$this._summary = summary;

  RouteBuilder();

  RouteBuilder get _$this {
    if (_$v != null) {
      _bounds = _$v.bounds?.toBuilder();
      _copyrights = _$v.copyrights;
      _legs = _$v.legs?.toBuilder();
      _overviewPolyline = _$v.overviewPolyline?.toBuilder();
      _summary = _$v.summary;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Route other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Route;
  }

  @override
  void update(void Function(RouteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Route build() {
    _$Route _$result;
    try {
      _$result = _$v ??
          new _$Route._(
              bounds: bounds.build(),
              copyrights: copyrights,
              legs: legs.build(),
              overviewPolyline: overviewPolyline.build(),
              summary: summary);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'bounds';
        bounds.build();

        _$failedField = 'legs';
        legs.build();
        _$failedField = 'overviewPolyline';
        overviewPolyline.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Route', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
