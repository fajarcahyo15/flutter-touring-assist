library bounds;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/direction/lat_lng.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'bounds.g.dart';

abstract class Bounds implements Built<Bounds, BoundsBuilder> {
  Bounds._();

  factory Bounds([updates(BoundsBuilder b)]) = _$Bounds;

  @BuiltValueField(wireName: 'northeast')
  LatLng get northeast;

  @BuiltValueField(wireName: 'southwest')
  LatLng get southwest;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Bounds.serializer, this));
  }

  static Bounds fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Bounds.serializer, json.decode(jsonString));
  }

  static Serializer<Bounds> get serializer => _$boundsSerializer;
}
