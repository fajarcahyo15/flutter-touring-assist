library distance;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'distance.g.dart';

abstract class Distance implements Built<Distance, DistanceBuilder> {
  Distance._();

  factory Distance([updates(DistanceBuilder b)]) = _$Distance;

  @BuiltValueField(wireName: 'text')
  String get text;

  @BuiltValueField(wireName: 'value')
  int get value;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Distance.serializer, this));
  }

  static Distance fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Distance.serializer, json.decode(jsonString));
  }

  static Serializer<Distance> get serializer => _$distanceSerializer;
}
