library route;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/direction/bounds.dart';
import 'package:new_touring_assist_flutter/model/response/direction/leg.dart';
import 'package:new_touring_assist_flutter/model/response/direction/polyline.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'route.g.dart';

abstract class Route implements Built<Route, RouteBuilder> {
  Route._();

  factory Route([updates(RouteBuilder b)]) = _$Route;

  @BuiltValueField(wireName: 'bounds')
  Bounds get bounds;

  @BuiltValueField(wireName: 'copyrights')
  String get copyrights;

  @BuiltValueField(wireName: 'legs')
  BuiltList<Leg> get legs;

  @BuiltValueField(wireName: 'overview_polyline')
  Polyline get overviewPolyline;

  @BuiltValueField(wireName: 'summary')
  String get summary;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Route.serializer, this));
  }

  static Route fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Route.serializer, json.decode(jsonString));
  }

  static Serializer<Route> get serializer => _$routeSerializer;
}
