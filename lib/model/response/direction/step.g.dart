// GENERATED CODE - DO NOT MODIFY BY HAND

part of step;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Step> _$stepSerializer = new _$StepSerializer();

class _$StepSerializer implements StructuredSerializer<Step> {
  @override
  final Iterable<Type> types = const [Step, _$Step];
  @override
  final String wireName = 'Step';

  @override
  Iterable<Object> serialize(Serializers serializers, Step object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'distance',
      serializers.serialize(object.distance,
          specifiedType: const FullType(Distance)),
      'duration',
      serializers.serialize(object.duration,
          specifiedType: const FullType(Distance)),
      'end_location',
      serializers.serialize(object.endLocation,
          specifiedType: const FullType(LatLng)),
      'html_instructions',
      serializers.serialize(object.htmlInstructions,
          specifiedType: const FullType(String)),
      'polyline',
      serializers.serialize(object.polyline,
          specifiedType: const FullType(Polyline)),
      'start_location',
      serializers.serialize(object.startLocation,
          specifiedType: const FullType(LatLng)),
      'travel_mode',
      serializers.serialize(object.travelMode,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Step deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new StepBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'distance':
          result.distance.replace(serializers.deserialize(value,
              specifiedType: const FullType(Distance)) as Distance);
          break;
        case 'duration':
          result.duration.replace(serializers.deserialize(value,
              specifiedType: const FullType(Distance)) as Distance);
          break;
        case 'end_location':
          result.endLocation.replace(serializers.deserialize(value,
              specifiedType: const FullType(LatLng)) as LatLng);
          break;
        case 'html_instructions':
          result.htmlInstructions = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'polyline':
          result.polyline.replace(serializers.deserialize(value,
              specifiedType: const FullType(Polyline)) as Polyline);
          break;
        case 'start_location':
          result.startLocation.replace(serializers.deserialize(value,
              specifiedType: const FullType(LatLng)) as LatLng);
          break;
        case 'travel_mode':
          result.travelMode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Step extends Step {
  @override
  final Distance distance;
  @override
  final Distance duration;
  @override
  final LatLng endLocation;
  @override
  final String htmlInstructions;
  @override
  final Polyline polyline;
  @override
  final LatLng startLocation;
  @override
  final String travelMode;

  factory _$Step([void Function(StepBuilder) updates]) =>
      (new StepBuilder()..update(updates)).build();

  _$Step._(
      {this.distance,
      this.duration,
      this.endLocation,
      this.htmlInstructions,
      this.polyline,
      this.startLocation,
      this.travelMode})
      : super._() {
    if (distance == null) {
      throw new BuiltValueNullFieldError('Step', 'distance');
    }
    if (duration == null) {
      throw new BuiltValueNullFieldError('Step', 'duration');
    }
    if (endLocation == null) {
      throw new BuiltValueNullFieldError('Step', 'endLocation');
    }
    if (htmlInstructions == null) {
      throw new BuiltValueNullFieldError('Step', 'htmlInstructions');
    }
    if (polyline == null) {
      throw new BuiltValueNullFieldError('Step', 'polyline');
    }
    if (startLocation == null) {
      throw new BuiltValueNullFieldError('Step', 'startLocation');
    }
    if (travelMode == null) {
      throw new BuiltValueNullFieldError('Step', 'travelMode');
    }
  }

  @override
  Step rebuild(void Function(StepBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StepBuilder toBuilder() => new StepBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Step &&
        distance == other.distance &&
        duration == other.duration &&
        endLocation == other.endLocation &&
        htmlInstructions == other.htmlInstructions &&
        polyline == other.polyline &&
        startLocation == other.startLocation &&
        travelMode == other.travelMode;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, distance.hashCode), duration.hashCode),
                        endLocation.hashCode),
                    htmlInstructions.hashCode),
                polyline.hashCode),
            startLocation.hashCode),
        travelMode.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Step')
          ..add('distance', distance)
          ..add('duration', duration)
          ..add('endLocation', endLocation)
          ..add('htmlInstructions', htmlInstructions)
          ..add('polyline', polyline)
          ..add('startLocation', startLocation)
          ..add('travelMode', travelMode))
        .toString();
  }
}

class StepBuilder implements Builder<Step, StepBuilder> {
  _$Step _$v;

  DistanceBuilder _distance;
  DistanceBuilder get distance => _$this._distance ??= new DistanceBuilder();
  set distance(DistanceBuilder distance) => _$this._distance = distance;

  DistanceBuilder _duration;
  DistanceBuilder get duration => _$this._duration ??= new DistanceBuilder();
  set duration(DistanceBuilder duration) => _$this._duration = duration;

  LatLngBuilder _endLocation;
  LatLngBuilder get endLocation => _$this._endLocation ??= new LatLngBuilder();
  set endLocation(LatLngBuilder endLocation) =>
      _$this._endLocation = endLocation;

  String _htmlInstructions;
  String get htmlInstructions => _$this._htmlInstructions;
  set htmlInstructions(String htmlInstructions) =>
      _$this._htmlInstructions = htmlInstructions;

  PolylineBuilder _polyline;
  PolylineBuilder get polyline => _$this._polyline ??= new PolylineBuilder();
  set polyline(PolylineBuilder polyline) => _$this._polyline = polyline;

  LatLngBuilder _startLocation;
  LatLngBuilder get startLocation =>
      _$this._startLocation ??= new LatLngBuilder();
  set startLocation(LatLngBuilder startLocation) =>
      _$this._startLocation = startLocation;

  String _travelMode;
  String get travelMode => _$this._travelMode;
  set travelMode(String travelMode) => _$this._travelMode = travelMode;

  StepBuilder();

  StepBuilder get _$this {
    if (_$v != null) {
      _distance = _$v.distance?.toBuilder();
      _duration = _$v.duration?.toBuilder();
      _endLocation = _$v.endLocation?.toBuilder();
      _htmlInstructions = _$v.htmlInstructions;
      _polyline = _$v.polyline?.toBuilder();
      _startLocation = _$v.startLocation?.toBuilder();
      _travelMode = _$v.travelMode;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Step other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Step;
  }

  @override
  void update(void Function(StepBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Step build() {
    _$Step _$result;
    try {
      _$result = _$v ??
          new _$Step._(
              distance: distance.build(),
              duration: duration.build(),
              endLocation: endLocation.build(),
              htmlInstructions: htmlInstructions,
              polyline: polyline.build(),
              startLocation: startLocation.build(),
              travelMode: travelMode);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'distance';
        distance.build();
        _$failedField = 'duration';
        duration.build();
        _$failedField = 'endLocation';
        endLocation.build();

        _$failedField = 'polyline';
        polyline.build();
        _$failedField = 'startLocation';
        startLocation.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Step', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
