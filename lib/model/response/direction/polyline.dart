library polyline;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'polyline.g.dart';

abstract class Polyline implements Built<Polyline, PolylineBuilder> {
  Polyline._();

  factory Polyline([updates(PolylineBuilder b)]) = _$Polyline;

  @BuiltValueField(wireName: 'points')
  String get points;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Polyline.serializer, this));
  }

  static Polyline fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Polyline.serializer, json.decode(jsonString));
  }

  static Serializer<Polyline> get serializer => _$polylineSerializer;
}
