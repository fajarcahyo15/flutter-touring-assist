// GENERATED CODE - DO NOT MODIFY BY HAND

part of leg;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Leg> _$legSerializer = new _$LegSerializer();

class _$LegSerializer implements StructuredSerializer<Leg> {
  @override
  final Iterable<Type> types = const [Leg, _$Leg];
  @override
  final String wireName = 'Leg';

  @override
  Iterable<Object> serialize(Serializers serializers, Leg object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'distance',
      serializers.serialize(object.distance,
          specifiedType: const FullType(Distance)),
      'duration',
      serializers.serialize(object.duration,
          specifiedType: const FullType(Distance)),
      'end_address',
      serializers.serialize(object.endAddress,
          specifiedType: const FullType(String)),
      'end_location',
      serializers.serialize(object.endLocation,
          specifiedType: const FullType(LatLng)),
      'start_address',
      serializers.serialize(object.startAddress,
          specifiedType: const FullType(String)),
      'start_location',
      serializers.serialize(object.startLocation,
          specifiedType: const FullType(LatLng)),
      'steps',
      serializers.serialize(object.steps,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Step)])),
    ];

    return result;
  }

  @override
  Leg deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LegBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'distance':
          result.distance.replace(serializers.deserialize(value,
              specifiedType: const FullType(Distance)) as Distance);
          break;
        case 'duration':
          result.duration.replace(serializers.deserialize(value,
              specifiedType: const FullType(Distance)) as Distance);
          break;
        case 'end_address':
          result.endAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'end_location':
          result.endLocation.replace(serializers.deserialize(value,
              specifiedType: const FullType(LatLng)) as LatLng);
          break;
        case 'start_address':
          result.startAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'start_location':
          result.startLocation.replace(serializers.deserialize(value,
              specifiedType: const FullType(LatLng)) as LatLng);
          break;
        case 'steps':
          result.steps.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Step)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$Leg extends Leg {
  @override
  final Distance distance;
  @override
  final Distance duration;
  @override
  final String endAddress;
  @override
  final LatLng endLocation;
  @override
  final String startAddress;
  @override
  final LatLng startLocation;
  @override
  final BuiltList<Step> steps;

  factory _$Leg([void Function(LegBuilder) updates]) =>
      (new LegBuilder()..update(updates)).build();

  _$Leg._(
      {this.distance,
      this.duration,
      this.endAddress,
      this.endLocation,
      this.startAddress,
      this.startLocation,
      this.steps})
      : super._() {
    if (distance == null) {
      throw new BuiltValueNullFieldError('Leg', 'distance');
    }
    if (duration == null) {
      throw new BuiltValueNullFieldError('Leg', 'duration');
    }
    if (endAddress == null) {
      throw new BuiltValueNullFieldError('Leg', 'endAddress');
    }
    if (endLocation == null) {
      throw new BuiltValueNullFieldError('Leg', 'endLocation');
    }
    if (startAddress == null) {
      throw new BuiltValueNullFieldError('Leg', 'startAddress');
    }
    if (startLocation == null) {
      throw new BuiltValueNullFieldError('Leg', 'startLocation');
    }
    if (steps == null) {
      throw new BuiltValueNullFieldError('Leg', 'steps');
    }
  }

  @override
  Leg rebuild(void Function(LegBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LegBuilder toBuilder() => new LegBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Leg &&
        distance == other.distance &&
        duration == other.duration &&
        endAddress == other.endAddress &&
        endLocation == other.endLocation &&
        startAddress == other.startAddress &&
        startLocation == other.startLocation &&
        steps == other.steps;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, distance.hashCode), duration.hashCode),
                        endAddress.hashCode),
                    endLocation.hashCode),
                startAddress.hashCode),
            startLocation.hashCode),
        steps.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Leg')
          ..add('distance', distance)
          ..add('duration', duration)
          ..add('endAddress', endAddress)
          ..add('endLocation', endLocation)
          ..add('startAddress', startAddress)
          ..add('startLocation', startLocation)
          ..add('steps', steps))
        .toString();
  }
}

class LegBuilder implements Builder<Leg, LegBuilder> {
  _$Leg _$v;

  DistanceBuilder _distance;
  DistanceBuilder get distance => _$this._distance ??= new DistanceBuilder();
  set distance(DistanceBuilder distance) => _$this._distance = distance;

  DistanceBuilder _duration;
  DistanceBuilder get duration => _$this._duration ??= new DistanceBuilder();
  set duration(DistanceBuilder duration) => _$this._duration = duration;

  String _endAddress;
  String get endAddress => _$this._endAddress;
  set endAddress(String endAddress) => _$this._endAddress = endAddress;

  LatLngBuilder _endLocation;
  LatLngBuilder get endLocation => _$this._endLocation ??= new LatLngBuilder();
  set endLocation(LatLngBuilder endLocation) =>
      _$this._endLocation = endLocation;

  String _startAddress;
  String get startAddress => _$this._startAddress;
  set startAddress(String startAddress) => _$this._startAddress = startAddress;

  LatLngBuilder _startLocation;
  LatLngBuilder get startLocation =>
      _$this._startLocation ??= new LatLngBuilder();
  set startLocation(LatLngBuilder startLocation) =>
      _$this._startLocation = startLocation;

  ListBuilder<Step> _steps;
  ListBuilder<Step> get steps => _$this._steps ??= new ListBuilder<Step>();
  set steps(ListBuilder<Step> steps) => _$this._steps = steps;

  LegBuilder();

  LegBuilder get _$this {
    if (_$v != null) {
      _distance = _$v.distance?.toBuilder();
      _duration = _$v.duration?.toBuilder();
      _endAddress = _$v.endAddress;
      _endLocation = _$v.endLocation?.toBuilder();
      _startAddress = _$v.startAddress;
      _startLocation = _$v.startLocation?.toBuilder();
      _steps = _$v.steps?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Leg other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Leg;
  }

  @override
  void update(void Function(LegBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Leg build() {
    _$Leg _$result;
    try {
      _$result = _$v ??
          new _$Leg._(
              distance: distance.build(),
              duration: duration.build(),
              endAddress: endAddress,
              endLocation: endLocation.build(),
              startAddress: startAddress,
              startLocation: startLocation.build(),
              steps: steps.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'distance';
        distance.build();
        _$failedField = 'duration';
        duration.build();

        _$failedField = 'endLocation';
        endLocation.build();

        _$failedField = 'startLocation';
        startLocation.build();
        _$failedField = 'steps';
        steps.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Leg', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
