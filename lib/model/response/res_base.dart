library res_base;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_base.g.dart';

abstract class ResBase implements Built<ResBase, ResBaseBuilder> {
  ResBase._();

  factory ResBase([updates(ResBaseBuilder b)]) = _$ResBase;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResBase.serializer, this));
  }

  static ResBase fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResBase.serializer, json.decode(jsonString));
  }

  static Serializer<ResBase> get serializer => _$resBaseSerializer;
}
