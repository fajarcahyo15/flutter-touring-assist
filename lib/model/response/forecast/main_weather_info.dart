library main_weather_info;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'main_weather_info.g.dart';

abstract class MainWeatherInfo implements Built<MainWeatherInfo, MainWeatherInfoBuilder> {
  MainWeatherInfo._();

  factory MainWeatherInfo([updates(MainWeatherInfoBuilder b)]) = _$MainWeatherInfo;

  @BuiltValueField(wireName: 'temp')
  double get temp;

  @BuiltValueField(wireName: 'feels_like')
  double get feelsLike;

  @BuiltValueField(wireName: 'temp_min')
  double get tempMin;

  @BuiltValueField(wireName: 'temp_max')
  double get tempMax;

  @BuiltValueField(wireName: 'pressure')
  int get pressure;

  @BuiltValueField(wireName: 'sea_level')
  int get seaLevel;

  @BuiltValueField(wireName: 'grnd_level')
  int get grndLevel;

  @BuiltValueField(wireName: 'humidity')
  int get humidity;

  @BuiltValueField(wireName: 'temp_kf')
  double get tempKf;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(MainWeatherInfo.serializer, this));
  }

  static MainWeatherInfo fromJson(String jsonString) {
    return standardSerializers.deserializeWith(MainWeatherInfo.serializer, json.decode(jsonString));
  }

  static Serializer<MainWeatherInfo> get serializer => _$mainWeatherInfoSerializer;
}
