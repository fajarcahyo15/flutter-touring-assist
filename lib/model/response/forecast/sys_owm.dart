library sys_owm;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'sys_owm.g.dart';

abstract class SysOwm implements Built<SysOwm, SysOwmBuilder> {
  SysOwm._();

  factory SysOwm([updates(SysOwmBuilder b)]) = _$SysOwm;

  @BuiltValueField(wireName: 'pod')
  String get pod;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(SysOwm.serializer, this));
  }

  static SysOwm fromJson(String jsonString) {
    return standardSerializers.deserializeWith(SysOwm.serializer, json.decode(jsonString));
  }

  static Serializer<SysOwm> get serializer => _$sysOwmSerializer;
}
