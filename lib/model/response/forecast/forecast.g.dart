// GENERATED CODE - DO NOT MODIFY BY HAND

part of forecast;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Forecast> _$forecastSerializer = new _$ForecastSerializer();

class _$ForecastSerializer implements StructuredSerializer<Forecast> {
  @override
  final Iterable<Type> types = const [Forecast, _$Forecast];
  @override
  final String wireName = 'Forecast';

  @override
  Iterable<Object> serialize(Serializers serializers, Forecast object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'cod',
      serializers.serialize(object.cod, specifiedType: const FullType(String)),
      'message',
      serializers.serialize(object.message, specifiedType: const FullType(int)),
      'cnt',
      serializers.serialize(object.cnt, specifiedType: const FullType(int)),
      'list',
      serializers.serialize(object.list,
          specifiedType: const FullType(
              BuiltList, const [const FullType(WeatherForecastOwm)])),
      'city',
      serializers.serialize(object.city,
          specifiedType: const FullType(CityOwm)),
    ];

    return result;
  }

  @override
  Forecast deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ForecastBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cod':
          result.cod = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'cnt':
          result.cnt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'list':
          result.list.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(WeatherForecastOwm)]))
              as BuiltList<dynamic>);
          break;
        case 'city':
          result.city.replace(serializers.deserialize(value,
              specifiedType: const FullType(CityOwm)) as CityOwm);
          break;
      }
    }

    return result.build();
  }
}

class _$Forecast extends Forecast {
  @override
  final String cod;
  @override
  final int message;
  @override
  final int cnt;
  @override
  final BuiltList<WeatherForecastOwm> list;
  @override
  final CityOwm city;

  factory _$Forecast([void Function(ForecastBuilder) updates]) =>
      (new ForecastBuilder()..update(updates)).build();

  _$Forecast._({this.cod, this.message, this.cnt, this.list, this.city})
      : super._() {
    if (cod == null) {
      throw new BuiltValueNullFieldError('Forecast', 'cod');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('Forecast', 'message');
    }
    if (cnt == null) {
      throw new BuiltValueNullFieldError('Forecast', 'cnt');
    }
    if (list == null) {
      throw new BuiltValueNullFieldError('Forecast', 'list');
    }
    if (city == null) {
      throw new BuiltValueNullFieldError('Forecast', 'city');
    }
  }

  @override
  Forecast rebuild(void Function(ForecastBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ForecastBuilder toBuilder() => new ForecastBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Forecast &&
        cod == other.cod &&
        message == other.message &&
        cnt == other.cnt &&
        list == other.list &&
        city == other.city;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, cod.hashCode), message.hashCode), cnt.hashCode),
            list.hashCode),
        city.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Forecast')
          ..add('cod', cod)
          ..add('message', message)
          ..add('cnt', cnt)
          ..add('list', list)
          ..add('city', city))
        .toString();
  }
}

class ForecastBuilder implements Builder<Forecast, ForecastBuilder> {
  _$Forecast _$v;

  String _cod;
  String get cod => _$this._cod;
  set cod(String cod) => _$this._cod = cod;

  int _message;
  int get message => _$this._message;
  set message(int message) => _$this._message = message;

  int _cnt;
  int get cnt => _$this._cnt;
  set cnt(int cnt) => _$this._cnt = cnt;

  ListBuilder<WeatherForecastOwm> _list;
  ListBuilder<WeatherForecastOwm> get list =>
      _$this._list ??= new ListBuilder<WeatherForecastOwm>();
  set list(ListBuilder<WeatherForecastOwm> list) => _$this._list = list;

  CityOwmBuilder _city;
  CityOwmBuilder get city => _$this._city ??= new CityOwmBuilder();
  set city(CityOwmBuilder city) => _$this._city = city;

  ForecastBuilder();

  ForecastBuilder get _$this {
    if (_$v != null) {
      _cod = _$v.cod;
      _message = _$v.message;
      _cnt = _$v.cnt;
      _list = _$v.list?.toBuilder();
      _city = _$v.city?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Forecast other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Forecast;
  }

  @override
  void update(void Function(ForecastBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Forecast build() {
    _$Forecast _$result;
    try {
      _$result = _$v ??
          new _$Forecast._(
              cod: cod,
              message: message,
              cnt: cnt,
              list: list.build(),
              city: city.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'list';
        list.build();
        _$failedField = 'city';
        city.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Forecast', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
