library clouds_owm;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'clouds_owm.g.dart';

abstract class CloudsOwm implements Built<CloudsOwm, CloudsOwmBuilder> {
  CloudsOwm._();

  factory CloudsOwm([updates(CloudsOwmBuilder b)]) = _$CloudsOwm;

  @BuiltValueField(wireName: 'all')
  int get all;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(CloudsOwm.serializer, this));
  }

  static CloudsOwm fromJson(String jsonString) {
    return standardSerializers.deserializeWith(CloudsOwm.serializer, json.decode(jsonString));
  }

  static Serializer<CloudsOwm> get serializer => _$cloudsOwmSerializer;
}
