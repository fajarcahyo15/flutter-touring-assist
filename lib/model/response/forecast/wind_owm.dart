library wind_owm;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'wind_owm.g.dart';

abstract class WindOwm implements Built<WindOwm, WindOwmBuilder> {
  WindOwm._();

  factory WindOwm([updates(WindOwmBuilder b)]) = _$WindOwm;

  @BuiltValueField(wireName: 'speed')
  double get speed;

  @BuiltValueField(wireName: 'deg')
  int get deg;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(WindOwm.serializer, this));
  }

  static WindOwm fromJson(String jsonString) {
    return standardSerializers.deserializeWith(WindOwm.serializer, json.decode(jsonString));
  }

  static Serializer<WindOwm> get serializer => _$windOwmSerializer;
}
