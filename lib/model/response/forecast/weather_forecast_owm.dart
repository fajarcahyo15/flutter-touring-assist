library weather_forecast_owm;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/clouds_owm.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/main_weather_info.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/sys_owm.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/weather.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/wind_owm.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'weather_forecast_owm.g.dart';

abstract class WeatherForecastOwm implements Built<WeatherForecastOwm, WeatherForecastOwmBuilder> {
  WeatherForecastOwm._();

  factory WeatherForecastOwm([updates(WeatherForecastOwmBuilder b)]) = _$WeatherForecastOwm;

  @BuiltValueField(wireName: 'dt')
  int get dt;

  @BuiltValueField(wireName: 'main')
  MainWeatherInfo get main;

  @BuiltValueField(wireName: 'weather')
  BuiltList<Weather> get weather;

  @BuiltValueField(wireName: 'clouds')
  CloudsOwm get clouds;

  @BuiltValueField(wireName: 'wind')
  WindOwm get wind;

  @BuiltValueField(wireName: 'sys')
  SysOwm get sys;

  @BuiltValueField(wireName: 'dt_txt')
  String get dtTxt;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(WeatherForecastOwm.serializer, this));
  }

  static WeatherForecastOwm fromJson(String jsonString) {
    return standardSerializers.deserializeWith(WeatherForecastOwm.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherForecastOwm> get serializer => _$weatherForecastOwmSerializer;
}
