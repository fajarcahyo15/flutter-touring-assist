// GENERATED CODE - DO NOT MODIFY BY HAND

part of main_weather_info;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MainWeatherInfo> _$mainWeatherInfoSerializer =
    new _$MainWeatherInfoSerializer();

class _$MainWeatherInfoSerializer
    implements StructuredSerializer<MainWeatherInfo> {
  @override
  final Iterable<Type> types = const [MainWeatherInfo, _$MainWeatherInfo];
  @override
  final String wireName = 'MainWeatherInfo';

  @override
  Iterable<Object> serialize(Serializers serializers, MainWeatherInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'temp',
      serializers.serialize(object.temp, specifiedType: const FullType(double)),
      'feels_like',
      serializers.serialize(object.feelsLike,
          specifiedType: const FullType(double)),
      'temp_min',
      serializers.serialize(object.tempMin,
          specifiedType: const FullType(double)),
      'temp_max',
      serializers.serialize(object.tempMax,
          specifiedType: const FullType(double)),
      'pressure',
      serializers.serialize(object.pressure,
          specifiedType: const FullType(int)),
      'sea_level',
      serializers.serialize(object.seaLevel,
          specifiedType: const FullType(int)),
      'grnd_level',
      serializers.serialize(object.grndLevel,
          specifiedType: const FullType(int)),
      'humidity',
      serializers.serialize(object.humidity,
          specifiedType: const FullType(int)),
      'temp_kf',
      serializers.serialize(object.tempKf,
          specifiedType: const FullType(double)),
    ];

    return result;
  }

  @override
  MainWeatherInfo deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MainWeatherInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'temp':
          result.temp = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'feels_like':
          result.feelsLike = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'temp_min':
          result.tempMin = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'temp_max':
          result.tempMax = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'pressure':
          result.pressure = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'sea_level':
          result.seaLevel = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'grnd_level':
          result.grndLevel = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'humidity':
          result.humidity = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'temp_kf':
          result.tempKf = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$MainWeatherInfo extends MainWeatherInfo {
  @override
  final double temp;
  @override
  final double feelsLike;
  @override
  final double tempMin;
  @override
  final double tempMax;
  @override
  final int pressure;
  @override
  final int seaLevel;
  @override
  final int grndLevel;
  @override
  final int humidity;
  @override
  final double tempKf;

  factory _$MainWeatherInfo([void Function(MainWeatherInfoBuilder) updates]) =>
      (new MainWeatherInfoBuilder()..update(updates)).build();

  _$MainWeatherInfo._(
      {this.temp,
      this.feelsLike,
      this.tempMin,
      this.tempMax,
      this.pressure,
      this.seaLevel,
      this.grndLevel,
      this.humidity,
      this.tempKf})
      : super._() {
    if (temp == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'temp');
    }
    if (feelsLike == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'feelsLike');
    }
    if (tempMin == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'tempMin');
    }
    if (tempMax == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'tempMax');
    }
    if (pressure == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'pressure');
    }
    if (seaLevel == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'seaLevel');
    }
    if (grndLevel == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'grndLevel');
    }
    if (humidity == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'humidity');
    }
    if (tempKf == null) {
      throw new BuiltValueNullFieldError('MainWeatherInfo', 'tempKf');
    }
  }

  @override
  MainWeatherInfo rebuild(void Function(MainWeatherInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MainWeatherInfoBuilder toBuilder() =>
      new MainWeatherInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MainWeatherInfo &&
        temp == other.temp &&
        feelsLike == other.feelsLike &&
        tempMin == other.tempMin &&
        tempMax == other.tempMax &&
        pressure == other.pressure &&
        seaLevel == other.seaLevel &&
        grndLevel == other.grndLevel &&
        humidity == other.humidity &&
        tempKf == other.tempKf;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, temp.hashCode), feelsLike.hashCode),
                                tempMin.hashCode),
                            tempMax.hashCode),
                        pressure.hashCode),
                    seaLevel.hashCode),
                grndLevel.hashCode),
            humidity.hashCode),
        tempKf.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MainWeatherInfo')
          ..add('temp', temp)
          ..add('feelsLike', feelsLike)
          ..add('tempMin', tempMin)
          ..add('tempMax', tempMax)
          ..add('pressure', pressure)
          ..add('seaLevel', seaLevel)
          ..add('grndLevel', grndLevel)
          ..add('humidity', humidity)
          ..add('tempKf', tempKf))
        .toString();
  }
}

class MainWeatherInfoBuilder
    implements Builder<MainWeatherInfo, MainWeatherInfoBuilder> {
  _$MainWeatherInfo _$v;

  double _temp;
  double get temp => _$this._temp;
  set temp(double temp) => _$this._temp = temp;

  double _feelsLike;
  double get feelsLike => _$this._feelsLike;
  set feelsLike(double feelsLike) => _$this._feelsLike = feelsLike;

  double _tempMin;
  double get tempMin => _$this._tempMin;
  set tempMin(double tempMin) => _$this._tempMin = tempMin;

  double _tempMax;
  double get tempMax => _$this._tempMax;
  set tempMax(double tempMax) => _$this._tempMax = tempMax;

  int _pressure;
  int get pressure => _$this._pressure;
  set pressure(int pressure) => _$this._pressure = pressure;

  int _seaLevel;
  int get seaLevel => _$this._seaLevel;
  set seaLevel(int seaLevel) => _$this._seaLevel = seaLevel;

  int _grndLevel;
  int get grndLevel => _$this._grndLevel;
  set grndLevel(int grndLevel) => _$this._grndLevel = grndLevel;

  int _humidity;
  int get humidity => _$this._humidity;
  set humidity(int humidity) => _$this._humidity = humidity;

  double _tempKf;
  double get tempKf => _$this._tempKf;
  set tempKf(double tempKf) => _$this._tempKf = tempKf;

  MainWeatherInfoBuilder();

  MainWeatherInfoBuilder get _$this {
    if (_$v != null) {
      _temp = _$v.temp;
      _feelsLike = _$v.feelsLike;
      _tempMin = _$v.tempMin;
      _tempMax = _$v.tempMax;
      _pressure = _$v.pressure;
      _seaLevel = _$v.seaLevel;
      _grndLevel = _$v.grndLevel;
      _humidity = _$v.humidity;
      _tempKf = _$v.tempKf;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MainWeatherInfo other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MainWeatherInfo;
  }

  @override
  void update(void Function(MainWeatherInfoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MainWeatherInfo build() {
    final _$result = _$v ??
        new _$MainWeatherInfo._(
            temp: temp,
            feelsLike: feelsLike,
            tempMin: tempMin,
            tempMax: tempMax,
            pressure: pressure,
            seaLevel: seaLevel,
            grndLevel: grndLevel,
            humidity: humidity,
            tempKf: tempKf);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
