// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_forecast_owm;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherForecastOwm> _$weatherForecastOwmSerializer =
    new _$WeatherForecastOwmSerializer();

class _$WeatherForecastOwmSerializer
    implements StructuredSerializer<WeatherForecastOwm> {
  @override
  final Iterable<Type> types = const [WeatherForecastOwm, _$WeatherForecastOwm];
  @override
  final String wireName = 'WeatherForecastOwm';

  @override
  Iterable<Object> serialize(Serializers serializers, WeatherForecastOwm object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'dt',
      serializers.serialize(object.dt, specifiedType: const FullType(int)),
      'main',
      serializers.serialize(object.main,
          specifiedType: const FullType(MainWeatherInfo)),
      'weather',
      serializers.serialize(object.weather,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Weather)])),
      'clouds',
      serializers.serialize(object.clouds,
          specifiedType: const FullType(CloudsOwm)),
      'wind',
      serializers.serialize(object.wind,
          specifiedType: const FullType(WindOwm)),
      'sys',
      serializers.serialize(object.sys, specifiedType: const FullType(SysOwm)),
      'dt_txt',
      serializers.serialize(object.dtTxt,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  WeatherForecastOwm deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherForecastOwmBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'dt':
          result.dt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'main':
          result.main.replace(serializers.deserialize(value,
                  specifiedType: const FullType(MainWeatherInfo))
              as MainWeatherInfo);
          break;
        case 'weather':
          result.weather.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Weather)]))
              as BuiltList<dynamic>);
          break;
        case 'clouds':
          result.clouds.replace(serializers.deserialize(value,
              specifiedType: const FullType(CloudsOwm)) as CloudsOwm);
          break;
        case 'wind':
          result.wind.replace(serializers.deserialize(value,
              specifiedType: const FullType(WindOwm)) as WindOwm);
          break;
        case 'sys':
          result.sys.replace(serializers.deserialize(value,
              specifiedType: const FullType(SysOwm)) as SysOwm);
          break;
        case 'dt_txt':
          result.dtTxt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherForecastOwm extends WeatherForecastOwm {
  @override
  final int dt;
  @override
  final MainWeatherInfo main;
  @override
  final BuiltList<Weather> weather;
  @override
  final CloudsOwm clouds;
  @override
  final WindOwm wind;
  @override
  final SysOwm sys;
  @override
  final String dtTxt;

  factory _$WeatherForecastOwm(
          [void Function(WeatherForecastOwmBuilder) updates]) =>
      (new WeatherForecastOwmBuilder()..update(updates)).build();

  _$WeatherForecastOwm._(
      {this.dt,
      this.main,
      this.weather,
      this.clouds,
      this.wind,
      this.sys,
      this.dtTxt})
      : super._() {
    if (dt == null) {
      throw new BuiltValueNullFieldError('WeatherForecastOwm', 'dt');
    }
    if (main == null) {
      throw new BuiltValueNullFieldError('WeatherForecastOwm', 'main');
    }
    if (weather == null) {
      throw new BuiltValueNullFieldError('WeatherForecastOwm', 'weather');
    }
    if (clouds == null) {
      throw new BuiltValueNullFieldError('WeatherForecastOwm', 'clouds');
    }
    if (wind == null) {
      throw new BuiltValueNullFieldError('WeatherForecastOwm', 'wind');
    }
    if (sys == null) {
      throw new BuiltValueNullFieldError('WeatherForecastOwm', 'sys');
    }
    if (dtTxt == null) {
      throw new BuiltValueNullFieldError('WeatherForecastOwm', 'dtTxt');
    }
  }

  @override
  WeatherForecastOwm rebuild(
          void Function(WeatherForecastOwmBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherForecastOwmBuilder toBuilder() =>
      new WeatherForecastOwmBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherForecastOwm &&
        dt == other.dt &&
        main == other.main &&
        weather == other.weather &&
        clouds == other.clouds &&
        wind == other.wind &&
        sys == other.sys &&
        dtTxt == other.dtTxt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, dt.hashCode), main.hashCode),
                        weather.hashCode),
                    clouds.hashCode),
                wind.hashCode),
            sys.hashCode),
        dtTxt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherForecastOwm')
          ..add('dt', dt)
          ..add('main', main)
          ..add('weather', weather)
          ..add('clouds', clouds)
          ..add('wind', wind)
          ..add('sys', sys)
          ..add('dtTxt', dtTxt))
        .toString();
  }
}

class WeatherForecastOwmBuilder
    implements Builder<WeatherForecastOwm, WeatherForecastOwmBuilder> {
  _$WeatherForecastOwm _$v;

  int _dt;
  int get dt => _$this._dt;
  set dt(int dt) => _$this._dt = dt;

  MainWeatherInfoBuilder _main;
  MainWeatherInfoBuilder get main =>
      _$this._main ??= new MainWeatherInfoBuilder();
  set main(MainWeatherInfoBuilder main) => _$this._main = main;

  ListBuilder<Weather> _weather;
  ListBuilder<Weather> get weather =>
      _$this._weather ??= new ListBuilder<Weather>();
  set weather(ListBuilder<Weather> weather) => _$this._weather = weather;

  CloudsOwmBuilder _clouds;
  CloudsOwmBuilder get clouds => _$this._clouds ??= new CloudsOwmBuilder();
  set clouds(CloudsOwmBuilder clouds) => _$this._clouds = clouds;

  WindOwmBuilder _wind;
  WindOwmBuilder get wind => _$this._wind ??= new WindOwmBuilder();
  set wind(WindOwmBuilder wind) => _$this._wind = wind;

  SysOwmBuilder _sys;
  SysOwmBuilder get sys => _$this._sys ??= new SysOwmBuilder();
  set sys(SysOwmBuilder sys) => _$this._sys = sys;

  String _dtTxt;
  String get dtTxt => _$this._dtTxt;
  set dtTxt(String dtTxt) => _$this._dtTxt = dtTxt;

  WeatherForecastOwmBuilder();

  WeatherForecastOwmBuilder get _$this {
    if (_$v != null) {
      _dt = _$v.dt;
      _main = _$v.main?.toBuilder();
      _weather = _$v.weather?.toBuilder();
      _clouds = _$v.clouds?.toBuilder();
      _wind = _$v.wind?.toBuilder();
      _sys = _$v.sys?.toBuilder();
      _dtTxt = _$v.dtTxt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherForecastOwm other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherForecastOwm;
  }

  @override
  void update(void Function(WeatherForecastOwmBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherForecastOwm build() {
    _$WeatherForecastOwm _$result;
    try {
      _$result = _$v ??
          new _$WeatherForecastOwm._(
              dt: dt,
              main: main.build(),
              weather: weather.build(),
              clouds: clouds.build(),
              wind: wind.build(),
              sys: sys.build(),
              dtTxt: dtTxt);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'main';
        main.build();
        _$failedField = 'weather';
        weather.build();
        _$failedField = 'clouds';
        clouds.build();
        _$failedField = 'wind';
        wind.build();
        _$failedField = 'sys';
        sys.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'WeatherForecastOwm', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
