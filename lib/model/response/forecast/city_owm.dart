library city_owm;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/coord.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'city_owm.g.dart';

abstract class CityOwm implements Built<CityOwm, CityOwmBuilder> {
  CityOwm._();

  factory CityOwm([updates(CityOwmBuilder b)]) = _$CityOwm;

  @BuiltValueField(wireName: 'id')
  int get id;

  @BuiltValueField(wireName: 'name')
  String get name;

  @BuiltValueField(wireName: 'coord')
  Coord get coord;

  @BuiltValueField(wireName: 'country')
  String get country;

  @BuiltValueField(wireName: 'timezone')
  int get timezone;

  @BuiltValueField(wireName: 'sunrise')
  int get sunrise;

  @BuiltValueField(wireName: 'sunset')
  int get sunset;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(CityOwm.serializer, this));
  }

  static CityOwm fromJson(String jsonString) {
    return standardSerializers.deserializeWith(CityOwm.serializer, json.decode(jsonString));
  }

  static Serializer<CityOwm> get serializer => _$cityOwmSerializer;
}
