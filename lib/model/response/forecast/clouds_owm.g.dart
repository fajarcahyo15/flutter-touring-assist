// GENERATED CODE - DO NOT MODIFY BY HAND

part of clouds_owm;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CloudsOwm> _$cloudsOwmSerializer = new _$CloudsOwmSerializer();

class _$CloudsOwmSerializer implements StructuredSerializer<CloudsOwm> {
  @override
  final Iterable<Type> types = const [CloudsOwm, _$CloudsOwm];
  @override
  final String wireName = 'CloudsOwm';

  @override
  Iterable<Object> serialize(Serializers serializers, CloudsOwm object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'all',
      serializers.serialize(object.all, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  CloudsOwm deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CloudsOwmBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'all':
          result.all = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$CloudsOwm extends CloudsOwm {
  @override
  final int all;

  factory _$CloudsOwm([void Function(CloudsOwmBuilder) updates]) =>
      (new CloudsOwmBuilder()..update(updates)).build();

  _$CloudsOwm._({this.all}) : super._() {
    if (all == null) {
      throw new BuiltValueNullFieldError('CloudsOwm', 'all');
    }
  }

  @override
  CloudsOwm rebuild(void Function(CloudsOwmBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CloudsOwmBuilder toBuilder() => new CloudsOwmBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CloudsOwm && all == other.all;
  }

  @override
  int get hashCode {
    return $jf($jc(0, all.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CloudsOwm')..add('all', all))
        .toString();
  }
}

class CloudsOwmBuilder implements Builder<CloudsOwm, CloudsOwmBuilder> {
  _$CloudsOwm _$v;

  int _all;
  int get all => _$this._all;
  set all(int all) => _$this._all = all;

  CloudsOwmBuilder();

  CloudsOwmBuilder get _$this {
    if (_$v != null) {
      _all = _$v.all;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CloudsOwm other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CloudsOwm;
  }

  @override
  void update(void Function(CloudsOwmBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CloudsOwm build() {
    final _$result = _$v ?? new _$CloudsOwm._(all: all);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
