// GENERATED CODE - DO NOT MODIFY BY HAND

part of city_owm;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CityOwm> _$cityOwmSerializer = new _$CityOwmSerializer();

class _$CityOwmSerializer implements StructuredSerializer<CityOwm> {
  @override
  final Iterable<Type> types = const [CityOwm, _$CityOwm];
  @override
  final String wireName = 'CityOwm';

  @override
  Iterable<Object> serialize(Serializers serializers, CityOwm object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'coord',
      serializers.serialize(object.coord, specifiedType: const FullType(Coord)),
      'country',
      serializers.serialize(object.country,
          specifiedType: const FullType(String)),
      'timezone',
      serializers.serialize(object.timezone,
          specifiedType: const FullType(int)),
      'sunrise',
      serializers.serialize(object.sunrise, specifiedType: const FullType(int)),
      'sunset',
      serializers.serialize(object.sunset, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  CityOwm deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CityOwmBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'coord':
          result.coord.replace(serializers.deserialize(value,
              specifiedType: const FullType(Coord)) as Coord);
          break;
        case 'country':
          result.country = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'timezone':
          result.timezone = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'sunrise':
          result.sunrise = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'sunset':
          result.sunset = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$CityOwm extends CityOwm {
  @override
  final int id;
  @override
  final String name;
  @override
  final Coord coord;
  @override
  final String country;
  @override
  final int timezone;
  @override
  final int sunrise;
  @override
  final int sunset;

  factory _$CityOwm([void Function(CityOwmBuilder) updates]) =>
      (new CityOwmBuilder()..update(updates)).build();

  _$CityOwm._(
      {this.id,
      this.name,
      this.coord,
      this.country,
      this.timezone,
      this.sunrise,
      this.sunset})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('CityOwm', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('CityOwm', 'name');
    }
    if (coord == null) {
      throw new BuiltValueNullFieldError('CityOwm', 'coord');
    }
    if (country == null) {
      throw new BuiltValueNullFieldError('CityOwm', 'country');
    }
    if (timezone == null) {
      throw new BuiltValueNullFieldError('CityOwm', 'timezone');
    }
    if (sunrise == null) {
      throw new BuiltValueNullFieldError('CityOwm', 'sunrise');
    }
    if (sunset == null) {
      throw new BuiltValueNullFieldError('CityOwm', 'sunset');
    }
  }

  @override
  CityOwm rebuild(void Function(CityOwmBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CityOwmBuilder toBuilder() => new CityOwmBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CityOwm &&
        id == other.id &&
        name == other.name &&
        coord == other.coord &&
        country == other.country &&
        timezone == other.timezone &&
        sunrise == other.sunrise &&
        sunset == other.sunset;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), name.hashCode),
                        coord.hashCode),
                    country.hashCode),
                timezone.hashCode),
            sunrise.hashCode),
        sunset.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CityOwm')
          ..add('id', id)
          ..add('name', name)
          ..add('coord', coord)
          ..add('country', country)
          ..add('timezone', timezone)
          ..add('sunrise', sunrise)
          ..add('sunset', sunset))
        .toString();
  }
}

class CityOwmBuilder implements Builder<CityOwm, CityOwmBuilder> {
  _$CityOwm _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  CoordBuilder _coord;
  CoordBuilder get coord => _$this._coord ??= new CoordBuilder();
  set coord(CoordBuilder coord) => _$this._coord = coord;

  String _country;
  String get country => _$this._country;
  set country(String country) => _$this._country = country;

  int _timezone;
  int get timezone => _$this._timezone;
  set timezone(int timezone) => _$this._timezone = timezone;

  int _sunrise;
  int get sunrise => _$this._sunrise;
  set sunrise(int sunrise) => _$this._sunrise = sunrise;

  int _sunset;
  int get sunset => _$this._sunset;
  set sunset(int sunset) => _$this._sunset = sunset;

  CityOwmBuilder();

  CityOwmBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _coord = _$v.coord?.toBuilder();
      _country = _$v.country;
      _timezone = _$v.timezone;
      _sunrise = _$v.sunrise;
      _sunset = _$v.sunset;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CityOwm other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CityOwm;
  }

  @override
  void update(void Function(CityOwmBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CityOwm build() {
    _$CityOwm _$result;
    try {
      _$result = _$v ??
          new _$CityOwm._(
              id: id,
              name: name,
              coord: coord.build(),
              country: country,
              timezone: timezone,
              sunrise: sunrise,
              sunset: sunset);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'coord';
        coord.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CityOwm', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
