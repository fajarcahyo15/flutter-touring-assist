// GENERATED CODE - DO NOT MODIFY BY HAND

part of wind_owm;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WindOwm> _$windOwmSerializer = new _$WindOwmSerializer();

class _$WindOwmSerializer implements StructuredSerializer<WindOwm> {
  @override
  final Iterable<Type> types = const [WindOwm, _$WindOwm];
  @override
  final String wireName = 'WindOwm';

  @override
  Iterable<Object> serialize(Serializers serializers, WindOwm object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'speed',
      serializers.serialize(object.speed,
          specifiedType: const FullType(double)),
      'deg',
      serializers.serialize(object.deg, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  WindOwm deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WindOwmBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'speed':
          result.speed = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'deg':
          result.deg = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$WindOwm extends WindOwm {
  @override
  final double speed;
  @override
  final int deg;

  factory _$WindOwm([void Function(WindOwmBuilder) updates]) =>
      (new WindOwmBuilder()..update(updates)).build();

  _$WindOwm._({this.speed, this.deg}) : super._() {
    if (speed == null) {
      throw new BuiltValueNullFieldError('WindOwm', 'speed');
    }
    if (deg == null) {
      throw new BuiltValueNullFieldError('WindOwm', 'deg');
    }
  }

  @override
  WindOwm rebuild(void Function(WindOwmBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WindOwmBuilder toBuilder() => new WindOwmBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WindOwm && speed == other.speed && deg == other.deg;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, speed.hashCode), deg.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WindOwm')
          ..add('speed', speed)
          ..add('deg', deg))
        .toString();
  }
}

class WindOwmBuilder implements Builder<WindOwm, WindOwmBuilder> {
  _$WindOwm _$v;

  double _speed;
  double get speed => _$this._speed;
  set speed(double speed) => _$this._speed = speed;

  int _deg;
  int get deg => _$this._deg;
  set deg(int deg) => _$this._deg = deg;

  WindOwmBuilder();

  WindOwmBuilder get _$this {
    if (_$v != null) {
      _speed = _$v.speed;
      _deg = _$v.deg;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WindOwm other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WindOwm;
  }

  @override
  void update(void Function(WindOwmBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WindOwm build() {
    final _$result = _$v ?? new _$WindOwm._(speed: speed, deg: deg);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
