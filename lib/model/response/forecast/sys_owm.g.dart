// GENERATED CODE - DO NOT MODIFY BY HAND

part of sys_owm;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SysOwm> _$sysOwmSerializer = new _$SysOwmSerializer();

class _$SysOwmSerializer implements StructuredSerializer<SysOwm> {
  @override
  final Iterable<Type> types = const [SysOwm, _$SysOwm];
  @override
  final String wireName = 'SysOwm';

  @override
  Iterable<Object> serialize(Serializers serializers, SysOwm object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'pod',
      serializers.serialize(object.pod, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SysOwm deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SysOwmBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'pod':
          result.pod = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SysOwm extends SysOwm {
  @override
  final String pod;

  factory _$SysOwm([void Function(SysOwmBuilder) updates]) =>
      (new SysOwmBuilder()..update(updates)).build();

  _$SysOwm._({this.pod}) : super._() {
    if (pod == null) {
      throw new BuiltValueNullFieldError('SysOwm', 'pod');
    }
  }

  @override
  SysOwm rebuild(void Function(SysOwmBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SysOwmBuilder toBuilder() => new SysOwmBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SysOwm && pod == other.pod;
  }

  @override
  int get hashCode {
    return $jf($jc(0, pod.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SysOwm')..add('pod', pod)).toString();
  }
}

class SysOwmBuilder implements Builder<SysOwm, SysOwmBuilder> {
  _$SysOwm _$v;

  String _pod;
  String get pod => _$this._pod;
  set pod(String pod) => _$this._pod = pod;

  SysOwmBuilder();

  SysOwmBuilder get _$this {
    if (_$v != null) {
      _pod = _$v.pod;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SysOwm other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SysOwm;
  }

  @override
  void update(void Function(SysOwmBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SysOwm build() {
    final _$result = _$v ?? new _$SysOwm._(pod: pod);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
