library forecast;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/city_owm.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/weather_forecast_owm.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'forecast.g.dart';

abstract class Forecast implements Built<Forecast, ForecastBuilder> {
  Forecast._();

  factory Forecast([updates(ForecastBuilder b)]) = _$Forecast;

  @BuiltValueField(wireName: 'cod')
  String get cod;

  @BuiltValueField(wireName: 'message')
  int get message;

  @BuiltValueField(wireName: 'cnt')
  int get cnt;

  @BuiltValueField(wireName: 'list')
  BuiltList<WeatherForecastOwm> get list;

  @BuiltValueField(wireName: 'city')
  CityOwm get city;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Forecast.serializer, this));
  }

  static Forecast fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Forecast.serializer, json.decode(jsonString));
  }

  static Serializer<Forecast> get serializer => _$forecastSerializer;
}
