library coord;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'coord.g.dart';

abstract class Coord implements Built<Coord, CoordBuilder> {
  Coord._();

  factory Coord([updates(CoordBuilder b)]) = _$Coord;

  @BuiltValueField(wireName: 'lat')
  double get lat;

  @BuiltValueField(wireName: 'lon')
  double get lon;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Coord.serializer, this));
  }

  static Coord fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Coord.serializer, json.decode(jsonString));
  }

  static Serializer<Coord> get serializer => _$coordSerializer;
}
