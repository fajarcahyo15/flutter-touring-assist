library data_last_grup_rute;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_last_grup_rute.g.dart';

abstract class DataLastGrupRute implements Built<DataLastGrupRute, DataLastGrupRuteBuilder> {
  DataLastGrupRute._();

  factory DataLastGrupRute([updates(DataLastGrupRuteBuilder b)]) = _$DataLastGrupRute;

  @BuiltValueField(wireName: 'rute')
  GrupRute get grupRute;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataLastGrupRute.serializer, this));
  }

  static DataLastGrupRute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataLastGrupRute.serializer, json.decode(jsonString));
  }

  static Serializer<DataLastGrupRute> get serializer => _$dataLastGrupRuteSerializer;
}
