library res_detail_grup_rute;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/data_detail_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_detail_grup_rute.g.dart';

abstract class ResDetailGrupRute implements Built<ResDetailGrupRute, ResDetailGrupRuteBuilder> {
  ResDetailGrupRute._();

  factory ResDetailGrupRute([updates(ResDetailGrupRuteBuilder b)]) = _$ResDetailGrupRute;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataDetailGrupRute get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResDetailGrupRute.serializer, this));
  }

  static ResDetailGrupRute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResDetailGrupRute.serializer, json.decode(jsonString));
  }

  static Serializer<ResDetailGrupRute> get serializer => _$resDetailGrupRuteSerializer;
}
