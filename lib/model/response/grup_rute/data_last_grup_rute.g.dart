// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_last_grup_rute;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataLastGrupRute> _$dataLastGrupRuteSerializer =
    new _$DataLastGrupRuteSerializer();

class _$DataLastGrupRuteSerializer
    implements StructuredSerializer<DataLastGrupRute> {
  @override
  final Iterable<Type> types = const [DataLastGrupRute, _$DataLastGrupRute];
  @override
  final String wireName = 'DataLastGrupRute';

  @override
  Iterable<Object> serialize(Serializers serializers, DataLastGrupRute object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'rute',
      serializers.serialize(object.grupRute,
          specifiedType: const FullType(GrupRute)),
    ];

    return result;
  }

  @override
  DataLastGrupRute deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataLastGrupRuteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'rute':
          result.grupRute.replace(serializers.deserialize(value,
              specifiedType: const FullType(GrupRute)) as GrupRute);
          break;
      }
    }

    return result.build();
  }
}

class _$DataLastGrupRute extends DataLastGrupRute {
  @override
  final GrupRute grupRute;

  factory _$DataLastGrupRute(
          [void Function(DataLastGrupRuteBuilder) updates]) =>
      (new DataLastGrupRuteBuilder()..update(updates)).build();

  _$DataLastGrupRute._({this.grupRute}) : super._() {
    if (grupRute == null) {
      throw new BuiltValueNullFieldError('DataLastGrupRute', 'grupRute');
    }
  }

  @override
  DataLastGrupRute rebuild(void Function(DataLastGrupRuteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataLastGrupRuteBuilder toBuilder() =>
      new DataLastGrupRuteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataLastGrupRute && grupRute == other.grupRute;
  }

  @override
  int get hashCode {
    return $jf($jc(0, grupRute.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataLastGrupRute')
          ..add('grupRute', grupRute))
        .toString();
  }
}

class DataLastGrupRuteBuilder
    implements Builder<DataLastGrupRute, DataLastGrupRuteBuilder> {
  _$DataLastGrupRute _$v;

  GrupRuteBuilder _grupRute;
  GrupRuteBuilder get grupRute => _$this._grupRute ??= new GrupRuteBuilder();
  set grupRute(GrupRuteBuilder grupRute) => _$this._grupRute = grupRute;

  DataLastGrupRuteBuilder();

  DataLastGrupRuteBuilder get _$this {
    if (_$v != null) {
      _grupRute = _$v.grupRute?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataLastGrupRute other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataLastGrupRute;
  }

  @override
  void update(void Function(DataLastGrupRuteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataLastGrupRute build() {
    _$DataLastGrupRute _$result;
    try {
      _$result = _$v ?? new _$DataLastGrupRute._(grupRute: grupRute.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'grupRute';
        grupRute.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataLastGrupRute', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
