library res_buat_grup_rute;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_buat_grup_rute.g.dart';

abstract class ResBuatGrupRute implements Built<ResBuatGrupRute, ResBuatGrupRuteBuilder> {
  ResBuatGrupRute._();

  factory ResBuatGrupRute([updates(ResBuatGrupRuteBuilder b)]) = _$ResBuatGrupRute;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResBuatGrupRute.serializer, this));
  }

  static ResBuatGrupRute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResBuatGrupRute.serializer, json.decode(jsonString));
  }

  static Serializer<ResBuatGrupRute> get serializer => _$resBuatGrupRuteSerializer;
}
