library data_detail_grup_rute;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_detail_grup_rute.g.dart';

abstract class DataDetailGrupRute implements Built<DataDetailGrupRute, DataDetailGrupRuteBuilder> {
  DataDetailGrupRute._();

  factory DataDetailGrupRute([updates(DataDetailGrupRuteBuilder b)]) = _$DataDetailGrupRute;

  @BuiltValueField(wireName: 'grup_rute')
  GrupRute get grupRute;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataDetailGrupRute.serializer, this));
  }

  static DataDetailGrupRute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataDetailGrupRute.serializer, json.decode(jsonString));
  }

  static Serializer<DataDetailGrupRute> get serializer => _$dataDetailGrupRuteSerializer;
}
