// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_detail_grup_rute;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataDetailGrupRute> _$dataDetailGrupRuteSerializer =
    new _$DataDetailGrupRuteSerializer();

class _$DataDetailGrupRuteSerializer
    implements StructuredSerializer<DataDetailGrupRute> {
  @override
  final Iterable<Type> types = const [DataDetailGrupRute, _$DataDetailGrupRute];
  @override
  final String wireName = 'DataDetailGrupRute';

  @override
  Iterable<Object> serialize(Serializers serializers, DataDetailGrupRute object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'grup_rute',
      serializers.serialize(object.grupRute,
          specifiedType: const FullType(GrupRute)),
    ];

    return result;
  }

  @override
  DataDetailGrupRute deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataDetailGrupRuteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'grup_rute':
          result.grupRute.replace(serializers.deserialize(value,
              specifiedType: const FullType(GrupRute)) as GrupRute);
          break;
      }
    }

    return result.build();
  }
}

class _$DataDetailGrupRute extends DataDetailGrupRute {
  @override
  final GrupRute grupRute;

  factory _$DataDetailGrupRute(
          [void Function(DataDetailGrupRuteBuilder) updates]) =>
      (new DataDetailGrupRuteBuilder()..update(updates)).build();

  _$DataDetailGrupRute._({this.grupRute}) : super._() {
    if (grupRute == null) {
      throw new BuiltValueNullFieldError('DataDetailGrupRute', 'grupRute');
    }
  }

  @override
  DataDetailGrupRute rebuild(
          void Function(DataDetailGrupRuteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataDetailGrupRuteBuilder toBuilder() =>
      new DataDetailGrupRuteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataDetailGrupRute && grupRute == other.grupRute;
  }

  @override
  int get hashCode {
    return $jf($jc(0, grupRute.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataDetailGrupRute')
          ..add('grupRute', grupRute))
        .toString();
  }
}

class DataDetailGrupRuteBuilder
    implements Builder<DataDetailGrupRute, DataDetailGrupRuteBuilder> {
  _$DataDetailGrupRute _$v;

  GrupRuteBuilder _grupRute;
  GrupRuteBuilder get grupRute => _$this._grupRute ??= new GrupRuteBuilder();
  set grupRute(GrupRuteBuilder grupRute) => _$this._grupRute = grupRute;

  DataDetailGrupRuteBuilder();

  DataDetailGrupRuteBuilder get _$this {
    if (_$v != null) {
      _grupRute = _$v.grupRute?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataDetailGrupRute other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataDetailGrupRute;
  }

  @override
  void update(void Function(DataDetailGrupRuteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataDetailGrupRute build() {
    _$DataDetailGrupRute _$result;
    try {
      _$result = _$v ?? new _$DataDetailGrupRute._(grupRute: grupRute.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'grupRute';
        grupRute.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataDetailGrupRute', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
