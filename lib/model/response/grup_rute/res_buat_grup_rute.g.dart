// GENERATED CODE - DO NOT MODIFY BY HAND

part of res_buat_grup_rute;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResBuatGrupRute> _$resBuatGrupRuteSerializer =
    new _$ResBuatGrupRuteSerializer();

class _$ResBuatGrupRuteSerializer
    implements StructuredSerializer<ResBuatGrupRute> {
  @override
  final Iterable<Type> types = const [ResBuatGrupRute, _$ResBuatGrupRute];
  @override
  final String wireName = 'ResBuatGrupRute';

  @override
  Iterable<Object> serialize(Serializers serializers, ResBuatGrupRute object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(bool)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  ResBuatGrupRute deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResBuatGrupRuteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ResBuatGrupRute extends ResBuatGrupRute {
  @override
  final bool status;
  @override
  final String message;

  factory _$ResBuatGrupRute([void Function(ResBuatGrupRuteBuilder) updates]) =>
      (new ResBuatGrupRuteBuilder()..update(updates)).build();

  _$ResBuatGrupRute._({this.status, this.message}) : super._() {
    if (status == null) {
      throw new BuiltValueNullFieldError('ResBuatGrupRute', 'status');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('ResBuatGrupRute', 'message');
    }
  }

  @override
  ResBuatGrupRute rebuild(void Function(ResBuatGrupRuteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResBuatGrupRuteBuilder toBuilder() =>
      new ResBuatGrupRuteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResBuatGrupRute &&
        status == other.status &&
        message == other.message;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, status.hashCode), message.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResBuatGrupRute')
          ..add('status', status)
          ..add('message', message))
        .toString();
  }
}

class ResBuatGrupRuteBuilder
    implements Builder<ResBuatGrupRute, ResBuatGrupRuteBuilder> {
  _$ResBuatGrupRute _$v;

  bool _status;
  bool get status => _$this._status;
  set status(bool status) => _$this._status = status;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  ResBuatGrupRuteBuilder();

  ResBuatGrupRuteBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _message = _$v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResBuatGrupRute other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResBuatGrupRute;
  }

  @override
  void update(void Function(ResBuatGrupRuteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResBuatGrupRute build() {
    final _$result =
        _$v ?? new _$ResBuatGrupRute._(status: status, message: message);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
