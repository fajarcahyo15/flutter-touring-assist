library res_list_grup_rute;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/data_list_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_list_grup_rute.g.dart';

abstract class ResListGrupRute implements Built<ResListGrupRute, ResListGrupRuteBuilder> {
  ResListGrupRute._();

  factory ResListGrupRute([updates(ResListGrupRuteBuilder b)]) = _$ResListGrupRute;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataListGrupRute get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResListGrupRute.serializer, this));
  }

  static ResListGrupRute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResListGrupRute.serializer, json.decode(jsonString));
  }

  static Serializer<ResListGrupRute> get serializer => _$resListGrupRuteSerializer;
}
