// GENERATED CODE - DO NOT MODIFY BY HAND

part of res_last_grup_rute;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResLastGrupRute> _$resLastGrupRuteSerializer =
    new _$ResLastGrupRuteSerializer();

class _$ResLastGrupRuteSerializer
    implements StructuredSerializer<ResLastGrupRute> {
  @override
  final Iterable<Type> types = const [ResLastGrupRute, _$ResLastGrupRute];
  @override
  final String wireName = 'ResLastGrupRute';

  @override
  Iterable<Object> serialize(Serializers serializers, ResLastGrupRute object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'status',
      serializers.serialize(object.status, specifiedType: const FullType(bool)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];
    if (object.data != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.data,
            specifiedType: const FullType(DataLastGrupRute)));
    }
    return result;
  }

  @override
  ResLastGrupRute deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResLastGrupRuteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'data':
          result.data.replace(serializers.deserialize(value,
                  specifiedType: const FullType(DataLastGrupRute))
              as DataLastGrupRute);
          break;
      }
    }

    return result.build();
  }
}

class _$ResLastGrupRute extends ResLastGrupRute {
  @override
  final bool status;
  @override
  final String message;
  @override
  final DataLastGrupRute data;

  factory _$ResLastGrupRute([void Function(ResLastGrupRuteBuilder) updates]) =>
      (new ResLastGrupRuteBuilder()..update(updates)).build();

  _$ResLastGrupRute._({this.status, this.message, this.data}) : super._() {
    if (status == null) {
      throw new BuiltValueNullFieldError('ResLastGrupRute', 'status');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('ResLastGrupRute', 'message');
    }
  }

  @override
  ResLastGrupRute rebuild(void Function(ResLastGrupRuteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResLastGrupRuteBuilder toBuilder() =>
      new ResLastGrupRuteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResLastGrupRute &&
        status == other.status &&
        message == other.message &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, status.hashCode), message.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResLastGrupRute')
          ..add('status', status)
          ..add('message', message)
          ..add('data', data))
        .toString();
  }
}

class ResLastGrupRuteBuilder
    implements Builder<ResLastGrupRute, ResLastGrupRuteBuilder> {
  _$ResLastGrupRute _$v;

  bool _status;
  bool get status => _$this._status;
  set status(bool status) => _$this._status = status;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  DataLastGrupRuteBuilder _data;
  DataLastGrupRuteBuilder get data =>
      _$this._data ??= new DataLastGrupRuteBuilder();
  set data(DataLastGrupRuteBuilder data) => _$this._data = data;

  ResLastGrupRuteBuilder();

  ResLastGrupRuteBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _message = _$v.message;
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResLastGrupRute other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResLastGrupRute;
  }

  @override
  void update(void Function(ResLastGrupRuteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResLastGrupRute build() {
    _$ResLastGrupRute _$result;
    try {
      _$result = _$v ??
          new _$ResLastGrupRute._(
              status: status, message: message, data: _data?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        _data?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResLastGrupRute', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
