library res_last_grup_rute;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/data_last_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_last_grup_rute.g.dart';

abstract class ResLastGrupRute implements Built<ResLastGrupRute, ResLastGrupRuteBuilder> {
  ResLastGrupRute._();

  factory ResLastGrupRute([updates(ResLastGrupRuteBuilder b)]) = _$ResLastGrupRute;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataLastGrupRute get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResLastGrupRute.serializer, this));
  }

  static ResLastGrupRute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResLastGrupRute.serializer, json.decode(jsonString));
  }

  static Serializer<ResLastGrupRute> get serializer => _$resLastGrupRuteSerializer;
}
