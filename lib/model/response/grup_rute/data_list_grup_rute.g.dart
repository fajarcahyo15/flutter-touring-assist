// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_list_grup_rute;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataListGrupRute> _$dataListGrupRuteSerializer =
    new _$DataListGrupRuteSerializer();

class _$DataListGrupRuteSerializer
    implements StructuredSerializer<DataListGrupRute> {
  @override
  final Iterable<Type> types = const [DataListGrupRute, _$DataListGrupRute];
  @override
  final String wireName = 'DataListGrupRute';

  @override
  Iterable<Object> serialize(Serializers serializers, DataListGrupRute object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'list_rute',
      serializers.serialize(object.listRute,
          specifiedType:
              const FullType(BuiltList, const [const FullType(GrupRute)])),
    ];

    return result;
  }

  @override
  DataListGrupRute deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataListGrupRuteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'list_rute':
          result.listRute.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(GrupRute)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$DataListGrupRute extends DataListGrupRute {
  @override
  final BuiltList<GrupRute> listRute;

  factory _$DataListGrupRute(
          [void Function(DataListGrupRuteBuilder) updates]) =>
      (new DataListGrupRuteBuilder()..update(updates)).build();

  _$DataListGrupRute._({this.listRute}) : super._() {
    if (listRute == null) {
      throw new BuiltValueNullFieldError('DataListGrupRute', 'listRute');
    }
  }

  @override
  DataListGrupRute rebuild(void Function(DataListGrupRuteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataListGrupRuteBuilder toBuilder() =>
      new DataListGrupRuteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataListGrupRute && listRute == other.listRute;
  }

  @override
  int get hashCode {
    return $jf($jc(0, listRute.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataListGrupRute')
          ..add('listRute', listRute))
        .toString();
  }
}

class DataListGrupRuteBuilder
    implements Builder<DataListGrupRute, DataListGrupRuteBuilder> {
  _$DataListGrupRute _$v;

  ListBuilder<GrupRute> _listRute;
  ListBuilder<GrupRute> get listRute =>
      _$this._listRute ??= new ListBuilder<GrupRute>();
  set listRute(ListBuilder<GrupRute> listRute) => _$this._listRute = listRute;

  DataListGrupRuteBuilder();

  DataListGrupRuteBuilder get _$this {
    if (_$v != null) {
      _listRute = _$v.listRute?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataListGrupRute other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataListGrupRute;
  }

  @override
  void update(void Function(DataListGrupRuteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataListGrupRute build() {
    _$DataListGrupRute _$result;
    try {
      _$result = _$v ?? new _$DataListGrupRute._(listRute: listRute.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'listRute';
        listRute.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataListGrupRute', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
