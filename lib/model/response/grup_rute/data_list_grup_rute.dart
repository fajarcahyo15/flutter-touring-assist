library data_list_grup_rute;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_list_grup_rute.g.dart';

abstract class DataListGrupRute implements Built<DataListGrupRute, DataListGrupRuteBuilder> {
  DataListGrupRute._();

  factory DataListGrupRute([updates(DataListGrupRuteBuilder b)]) = _$DataListGrupRute;

  @BuiltValueField(wireName: 'list_rute')
  BuiltList<GrupRute> get listRute;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataListGrupRute.serializer, this));
  }

  static DataListGrupRute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataListGrupRute.serializer, json.decode(jsonString));
  }

  static Serializer<DataListGrupRute> get serializer => _$dataListGrupRuteSerializer;
}
