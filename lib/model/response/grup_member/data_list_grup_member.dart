library data_list_grup_member;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/anggota.dart';
import 'package:new_touring_assist_flutter/model/grup_member.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_list_grup_member.g.dart';

abstract class DataListGrupMember implements Built<DataListGrupMember, DataListGrupMemberBuilder> {
  DataListGrupMember._();

  factory DataListGrupMember([updates(DataListGrupMemberBuilder b)]) = _$DataListGrupMember;

  @nullable
  @BuiltValueField(wireName: 'anggota')
  Anggota get anggota;

  @nullable
  @BuiltValueField(wireName: 'grup_member')
  GrupMember get grupMember;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataListGrupMember.serializer, this));
  }

  static DataListGrupMember fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataListGrupMember.serializer, json.decode(jsonString));
  }

  static Serializer<DataListGrupMember> get serializer => _$dataListGrupMemberSerializer;
}
