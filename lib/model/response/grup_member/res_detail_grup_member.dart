library res_detail_grup_member;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/data_detail_grup_member.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_detail_grup_member.g.dart';

abstract class ResDetailGrupMember implements Built<ResDetailGrupMember, ResDetailGrupMemberBuilder> {
  ResDetailGrupMember._();

  factory ResDetailGrupMember([updates(ResDetailGrupMemberBuilder b)]) = _$ResDetailGrupMember;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  DataDetailGrupMember get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResDetailGrupMember.serializer, this));
  }

  static ResDetailGrupMember fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResDetailGrupMember.serializer, json.decode(jsonString));
  }

  static Serializer<ResDetailGrupMember> get serializer => _$resDetailGrupMemberSerializer;
}
