library res_list_grup_member;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/data_list_grup_member.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'res_list_grup_member.g.dart';

abstract class ResListGrupMember implements Built<ResListGrupMember, ResListGrupMemberBuilder> {
  ResListGrupMember._();

  factory ResListGrupMember([updates(ResListGrupMemberBuilder b)]) = _$ResListGrupMember;

  @BuiltValueField(wireName: 'status')
  bool get status;

  @BuiltValueField(wireName: 'message')
  String get message;

  @nullable
  @BuiltValueField(wireName: 'data')
  BuiltList<DataListGrupMember> get data;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(ResListGrupMember.serializer, this));
  }

  static ResListGrupMember fromJson(String jsonString) {
    return standardSerializers.deserializeWith(ResListGrupMember.serializer, json.decode(jsonString));
  }

  static Serializer<ResListGrupMember> get serializer => _$resListGrupMemberSerializer;
}
