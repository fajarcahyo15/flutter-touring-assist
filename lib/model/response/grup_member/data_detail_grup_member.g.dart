// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_detail_grup_member;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataDetailGrupMember> _$dataDetailGrupMemberSerializer =
    new _$DataDetailGrupMemberSerializer();

class _$DataDetailGrupMemberSerializer
    implements StructuredSerializer<DataDetailGrupMember> {
  @override
  final Iterable<Type> types = const [
    DataDetailGrupMember,
    _$DataDetailGrupMember
  ];
  @override
  final String wireName = 'DataDetailGrupMember';

  @override
  Iterable<Object> serialize(
      Serializers serializers, DataDetailGrupMember object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'grup_member',
      serializers.serialize(object.grupMember,
          specifiedType: const FullType(GrupMember)),
    ];

    return result;
  }

  @override
  DataDetailGrupMember deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataDetailGrupMemberBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'grup_member':
          result.grupMember.replace(serializers.deserialize(value,
              specifiedType: const FullType(GrupMember)) as GrupMember);
          break;
      }
    }

    return result.build();
  }
}

class _$DataDetailGrupMember extends DataDetailGrupMember {
  @override
  final GrupMember grupMember;

  factory _$DataDetailGrupMember(
          [void Function(DataDetailGrupMemberBuilder) updates]) =>
      (new DataDetailGrupMemberBuilder()..update(updates)).build();

  _$DataDetailGrupMember._({this.grupMember}) : super._() {
    if (grupMember == null) {
      throw new BuiltValueNullFieldError('DataDetailGrupMember', 'grupMember');
    }
  }

  @override
  DataDetailGrupMember rebuild(
          void Function(DataDetailGrupMemberBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataDetailGrupMemberBuilder toBuilder() =>
      new DataDetailGrupMemberBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataDetailGrupMember && grupMember == other.grupMember;
  }

  @override
  int get hashCode {
    return $jf($jc(0, grupMember.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataDetailGrupMember')
          ..add('grupMember', grupMember))
        .toString();
  }
}

class DataDetailGrupMemberBuilder
    implements Builder<DataDetailGrupMember, DataDetailGrupMemberBuilder> {
  _$DataDetailGrupMember _$v;

  GrupMemberBuilder _grupMember;
  GrupMemberBuilder get grupMember =>
      _$this._grupMember ??= new GrupMemberBuilder();
  set grupMember(GrupMemberBuilder grupMember) =>
      _$this._grupMember = grupMember;

  DataDetailGrupMemberBuilder();

  DataDetailGrupMemberBuilder get _$this {
    if (_$v != null) {
      _grupMember = _$v.grupMember?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataDetailGrupMember other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataDetailGrupMember;
  }

  @override
  void update(void Function(DataDetailGrupMemberBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataDetailGrupMember build() {
    _$DataDetailGrupMember _$result;
    try {
      _$result =
          _$v ?? new _$DataDetailGrupMember._(grupMember: grupMember.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'grupMember';
        grupMember.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataDetailGrupMember', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
