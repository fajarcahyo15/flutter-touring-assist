// GENERATED CODE - DO NOT MODIFY BY HAND

part of data_list_grup_member;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DataListGrupMember> _$dataListGrupMemberSerializer =
    new _$DataListGrupMemberSerializer();

class _$DataListGrupMemberSerializer
    implements StructuredSerializer<DataListGrupMember> {
  @override
  final Iterable<Type> types = const [DataListGrupMember, _$DataListGrupMember];
  @override
  final String wireName = 'DataListGrupMember';

  @override
  Iterable<Object> serialize(Serializers serializers, DataListGrupMember object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.anggota != null) {
      result
        ..add('anggota')
        ..add(serializers.serialize(object.anggota,
            specifiedType: const FullType(Anggota)));
    }
    if (object.grupMember != null) {
      result
        ..add('grup_member')
        ..add(serializers.serialize(object.grupMember,
            specifiedType: const FullType(GrupMember)));
    }
    return result;
  }

  @override
  DataListGrupMember deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DataListGrupMemberBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'anggota':
          result.anggota.replace(serializers.deserialize(value,
              specifiedType: const FullType(Anggota)) as Anggota);
          break;
        case 'grup_member':
          result.grupMember.replace(serializers.deserialize(value,
              specifiedType: const FullType(GrupMember)) as GrupMember);
          break;
      }
    }

    return result.build();
  }
}

class _$DataListGrupMember extends DataListGrupMember {
  @override
  final Anggota anggota;
  @override
  final GrupMember grupMember;

  factory _$DataListGrupMember(
          [void Function(DataListGrupMemberBuilder) updates]) =>
      (new DataListGrupMemberBuilder()..update(updates)).build();

  _$DataListGrupMember._({this.anggota, this.grupMember}) : super._();

  @override
  DataListGrupMember rebuild(
          void Function(DataListGrupMemberBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DataListGrupMemberBuilder toBuilder() =>
      new DataListGrupMemberBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DataListGrupMember &&
        anggota == other.anggota &&
        grupMember == other.grupMember;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, anggota.hashCode), grupMember.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DataListGrupMember')
          ..add('anggota', anggota)
          ..add('grupMember', grupMember))
        .toString();
  }
}

class DataListGrupMemberBuilder
    implements Builder<DataListGrupMember, DataListGrupMemberBuilder> {
  _$DataListGrupMember _$v;

  AnggotaBuilder _anggota;
  AnggotaBuilder get anggota => _$this._anggota ??= new AnggotaBuilder();
  set anggota(AnggotaBuilder anggota) => _$this._anggota = anggota;

  GrupMemberBuilder _grupMember;
  GrupMemberBuilder get grupMember =>
      _$this._grupMember ??= new GrupMemberBuilder();
  set grupMember(GrupMemberBuilder grupMember) =>
      _$this._grupMember = grupMember;

  DataListGrupMemberBuilder();

  DataListGrupMemberBuilder get _$this {
    if (_$v != null) {
      _anggota = _$v.anggota?.toBuilder();
      _grupMember = _$v.grupMember?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DataListGrupMember other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DataListGrupMember;
  }

  @override
  void update(void Function(DataListGrupMemberBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DataListGrupMember build() {
    _$DataListGrupMember _$result;
    try {
      _$result = _$v ??
          new _$DataListGrupMember._(
              anggota: _anggota?.build(), grupMember: _grupMember?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'anggota';
        _anggota?.build();
        _$failedField = 'grupMember';
        _grupMember?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DataListGrupMember', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
