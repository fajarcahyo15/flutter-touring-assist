library data_detail_grup_member;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/grup_member.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'data_detail_grup_member.g.dart';

abstract class DataDetailGrupMember implements Built<DataDetailGrupMember, DataDetailGrupMemberBuilder> {
  DataDetailGrupMember._();

  factory DataDetailGrupMember([updates(DataDetailGrupMemberBuilder b)]) = _$DataDetailGrupMember;

  @BuiltValueField(wireName: 'grup_member')
  GrupMember get grupMember;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(DataDetailGrupMember.serializer, this));
  }

  static DataDetailGrupMember fromJson(String jsonString) {
    return standardSerializers.deserializeWith(DataDetailGrupMember.serializer, json.decode(jsonString));
  }

  static Serializer<DataDetailGrupMember> get serializer => _$dataDetailGrupMemberSerializer;
}
