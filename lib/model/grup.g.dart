// GENERATED CODE - DO NOT MODIFY BY HAND

part of grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Grup> _$grupSerializer = new _$GrupSerializer();

class _$GrupSerializer implements StructuredSerializer<Grup> {
  @override
  final Iterable<Type> types = const [Grup, _$Grup];
  @override
  final String wireName = 'Grup';

  @override
  Iterable<Object> serialize(Serializers serializers, Grup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idGrup != null) {
      result
        ..add('id_grup')
        ..add(serializers.serialize(object.idGrup,
            specifiedType: const FullType(String)));
    }
    if (object.namaGrup != null) {
      result
        ..add('nama_grup')
        ..add(serializers.serialize(object.namaGrup,
            specifiedType: const FullType(String)));
    }
    if (object.deskripsi != null) {
      result
        ..add('deskripsi')
        ..add(serializers.serialize(object.deskripsi,
            specifiedType: const FullType(String)));
    }
    if (object.fotoGrup != null) {
      result
        ..add('foto_grup')
        ..add(serializers.serialize(object.fotoGrup,
            specifiedType: const FullType(String)));
    }
    if (object.tglBerangkat != null) {
      result
        ..add('tgl_berangkat')
        ..add(serializers.serialize(object.tglBerangkat,
            specifiedType: const FullType(String)));
    }
    if (object.jarakMaksimal != null) {
      result
        ..add('jarak_maksimal')
        ..add(serializers.serialize(object.jarakMaksimal,
            specifiedType: const FullType(String)));
    }
    if (object.intervalLokasi != null) {
      result
        ..add('interval_lokasi')
        ..add(serializers.serialize(object.intervalLokasi,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Grup deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_grup':
          result.idGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_grup':
          result.namaGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'deskripsi':
          result.deskripsi = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'foto_grup':
          result.fotoGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'tgl_berangkat':
          result.tglBerangkat = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jarak_maksimal':
          result.jarakMaksimal = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'interval_lokasi':
          result.intervalLokasi = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Grup extends Grup {
  @override
  final String idGrup;
  @override
  final String namaGrup;
  @override
  final String deskripsi;
  @override
  final String fotoGrup;
  @override
  final String tglBerangkat;
  @override
  final String jarakMaksimal;
  @override
  final String intervalLokasi;

  factory _$Grup([void Function(GrupBuilder) updates]) =>
      (new GrupBuilder()..update(updates)).build();

  _$Grup._(
      {this.idGrup,
      this.namaGrup,
      this.deskripsi,
      this.fotoGrup,
      this.tglBerangkat,
      this.jarakMaksimal,
      this.intervalLokasi})
      : super._();

  @override
  Grup rebuild(void Function(GrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GrupBuilder toBuilder() => new GrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Grup &&
        idGrup == other.idGrup &&
        namaGrup == other.namaGrup &&
        deskripsi == other.deskripsi &&
        fotoGrup == other.fotoGrup &&
        tglBerangkat == other.tglBerangkat &&
        jarakMaksimal == other.jarakMaksimal &&
        intervalLokasi == other.intervalLokasi;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, idGrup.hashCode), namaGrup.hashCode),
                        deskripsi.hashCode),
                    fotoGrup.hashCode),
                tglBerangkat.hashCode),
            jarakMaksimal.hashCode),
        intervalLokasi.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Grup')
          ..add('idGrup', idGrup)
          ..add('namaGrup', namaGrup)
          ..add('deskripsi', deskripsi)
          ..add('fotoGrup', fotoGrup)
          ..add('tglBerangkat', tglBerangkat)
          ..add('jarakMaksimal', jarakMaksimal)
          ..add('intervalLokasi', intervalLokasi))
        .toString();
  }
}

class GrupBuilder implements Builder<Grup, GrupBuilder> {
  _$Grup _$v;

  String _idGrup;
  String get idGrup => _$this._idGrup;
  set idGrup(String idGrup) => _$this._idGrup = idGrup;

  String _namaGrup;
  String get namaGrup => _$this._namaGrup;
  set namaGrup(String namaGrup) => _$this._namaGrup = namaGrup;

  String _deskripsi;
  String get deskripsi => _$this._deskripsi;
  set deskripsi(String deskripsi) => _$this._deskripsi = deskripsi;

  String _fotoGrup;
  String get fotoGrup => _$this._fotoGrup;
  set fotoGrup(String fotoGrup) => _$this._fotoGrup = fotoGrup;

  String _tglBerangkat;
  String get tglBerangkat => _$this._tglBerangkat;
  set tglBerangkat(String tglBerangkat) => _$this._tglBerangkat = tglBerangkat;

  String _jarakMaksimal;
  String get jarakMaksimal => _$this._jarakMaksimal;
  set jarakMaksimal(String jarakMaksimal) =>
      _$this._jarakMaksimal = jarakMaksimal;

  String _intervalLokasi;
  String get intervalLokasi => _$this._intervalLokasi;
  set intervalLokasi(String intervalLokasi) =>
      _$this._intervalLokasi = intervalLokasi;

  GrupBuilder();

  GrupBuilder get _$this {
    if (_$v != null) {
      _idGrup = _$v.idGrup;
      _namaGrup = _$v.namaGrup;
      _deskripsi = _$v.deskripsi;
      _fotoGrup = _$v.fotoGrup;
      _tglBerangkat = _$v.tglBerangkat;
      _jarakMaksimal = _$v.jarakMaksimal;
      _intervalLokasi = _$v.intervalLokasi;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Grup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Grup;
  }

  @override
  void update(void Function(GrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Grup build() {
    final _$result = _$v ??
        new _$Grup._(
            idGrup: idGrup,
            namaGrup: namaGrup,
            deskripsi: deskripsi,
            fotoGrup: fotoGrup,
            tglBerangkat: tglBerangkat,
            jarakMaksimal: jarakMaksimal,
            intervalLokasi: intervalLokasi);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
