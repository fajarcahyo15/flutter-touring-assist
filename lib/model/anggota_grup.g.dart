// GENERATED CODE - DO NOT MODIFY BY HAND

part of anggota_grup;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AnggotaGrup> _$anggotaGrupSerializer = new _$AnggotaGrupSerializer();

class _$AnggotaGrupSerializer implements StructuredSerializer<AnggotaGrup> {
  @override
  final Iterable<Type> types = const [AnggotaGrup, _$AnggotaGrup];
  @override
  final String wireName = 'AnggotaGrup';

  @override
  Iterable<Object> serialize(Serializers serializers, AnggotaGrup object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idGrup != null) {
      result
        ..add('id_grup')
        ..add(serializers.serialize(object.idGrup,
            specifiedType: const FullType(String)));
    }
    if (object.idGrupMember != null) {
      result
        ..add('id_grup_member')
        ..add(serializers.serialize(object.idGrupMember,
            specifiedType: const FullType(String)));
    }
    if (object.idAnggota != null) {
      result
        ..add('id_anggota')
        ..add(serializers.serialize(object.idAnggota,
            specifiedType: const FullType(String)));
    }
    if (object.namaLengkap != null) {
      result
        ..add('nama_lengkap')
        ..add(serializers.serialize(object.namaLengkap,
            specifiedType: const FullType(String)));
    }
    if (object.noHp != null) {
      result
        ..add('no_hp')
        ..add(serializers.serialize(object.noHp,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.foto != null) {
      result
        ..add('foto')
        ..add(serializers.serialize(object.foto,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AnggotaGrup deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AnggotaGrupBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_grup':
          result.idGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id_grup_member':
          result.idGrupMember = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id_anggota':
          result.idAnggota = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nama_lengkap':
          result.namaLengkap = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'no_hp':
          result.noHp = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'foto':
          result.foto = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AnggotaGrup extends AnggotaGrup {
  @override
  final String idGrup;
  @override
  final String idGrupMember;
  @override
  final String idAnggota;
  @override
  final String namaLengkap;
  @override
  final String noHp;
  @override
  final String email;
  @override
  final String foto;

  factory _$AnggotaGrup([void Function(AnggotaGrupBuilder) updates]) =>
      (new AnggotaGrupBuilder()..update(updates)).build();

  _$AnggotaGrup._(
      {this.idGrup,
      this.idGrupMember,
      this.idAnggota,
      this.namaLengkap,
      this.noHp,
      this.email,
      this.foto})
      : super._();

  @override
  AnggotaGrup rebuild(void Function(AnggotaGrupBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AnggotaGrupBuilder toBuilder() => new AnggotaGrupBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AnggotaGrup &&
        idGrup == other.idGrup &&
        idGrupMember == other.idGrupMember &&
        idAnggota == other.idAnggota &&
        namaLengkap == other.namaLengkap &&
        noHp == other.noHp &&
        email == other.email &&
        foto == other.foto;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, idGrup.hashCode), idGrupMember.hashCode),
                        idAnggota.hashCode),
                    namaLengkap.hashCode),
                noHp.hashCode),
            email.hashCode),
        foto.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AnggotaGrup')
          ..add('idGrup', idGrup)
          ..add('idGrupMember', idGrupMember)
          ..add('idAnggota', idAnggota)
          ..add('namaLengkap', namaLengkap)
          ..add('noHp', noHp)
          ..add('email', email)
          ..add('foto', foto))
        .toString();
  }
}

class AnggotaGrupBuilder implements Builder<AnggotaGrup, AnggotaGrupBuilder> {
  _$AnggotaGrup _$v;

  String _idGrup;
  String get idGrup => _$this._idGrup;
  set idGrup(String idGrup) => _$this._idGrup = idGrup;

  String _idGrupMember;
  String get idGrupMember => _$this._idGrupMember;
  set idGrupMember(String idGrupMember) => _$this._idGrupMember = idGrupMember;

  String _idAnggota;
  String get idAnggota => _$this._idAnggota;
  set idAnggota(String idAnggota) => _$this._idAnggota = idAnggota;

  String _namaLengkap;
  String get namaLengkap => _$this._namaLengkap;
  set namaLengkap(String namaLengkap) => _$this._namaLengkap = namaLengkap;

  String _noHp;
  String get noHp => _$this._noHp;
  set noHp(String noHp) => _$this._noHp = noHp;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _foto;
  String get foto => _$this._foto;
  set foto(String foto) => _$this._foto = foto;

  AnggotaGrupBuilder();

  AnggotaGrupBuilder get _$this {
    if (_$v != null) {
      _idGrup = _$v.idGrup;
      _idGrupMember = _$v.idGrupMember;
      _idAnggota = _$v.idAnggota;
      _namaLengkap = _$v.namaLengkap;
      _noHp = _$v.noHp;
      _email = _$v.email;
      _foto = _$v.foto;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AnggotaGrup other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AnggotaGrup;
  }

  @override
  void update(void Function(AnggotaGrupBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AnggotaGrup build() {
    final _$result = _$v ??
        new _$AnggotaGrup._(
            idGrup: idGrup,
            idGrupMember: idGrupMember,
            idAnggota: idAnggota,
            namaLengkap: namaLengkap,
            noHp: noHp,
            email: email,
            foto: foto);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
