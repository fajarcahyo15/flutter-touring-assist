library anggota_grup;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'anggota_grup.g.dart';

abstract class AnggotaGrup implements Built<AnggotaGrup, AnggotaGrupBuilder> {
  AnggotaGrup._();

  factory AnggotaGrup([updates(AnggotaGrupBuilder b)]) = _$AnggotaGrup;

  @nullable
  @BuiltValueField(wireName: 'id_grup')
  String get idGrup;

  @nullable
  @BuiltValueField(wireName: 'id_grup_member')
  String get idGrupMember;

  @nullable
  @BuiltValueField(wireName: 'id_anggota')
  String get idAnggota;

  @nullable
  @BuiltValueField(wireName: 'nama_lengkap')
  String get namaLengkap;

  @nullable
  @BuiltValueField(wireName: 'no_hp')
  String get noHp;

  @nullable
  @BuiltValueField(wireName: 'email')
  String get email;

  @nullable
  @BuiltValueField(wireName: 'foto')
  String get foto;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(AnggotaGrup.serializer, this));
  }

  static AnggotaGrup fromJson(String jsonString) {
    return standardSerializers.deserializeWith(AnggotaGrup.serializer, json.decode(jsonString));
  }

  static Serializer<AnggotaGrup> get serializer => _$anggotaGrupSerializer;
}
