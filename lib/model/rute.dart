library rute;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'rute.g.dart';

abstract class Rute implements Built<Rute, RuteBuilder> {
  Rute._();

  factory Rute([updates(RuteBuilder b)]) = _$Rute;

  @nullable
  @BuiltValueField(wireName: 'id_grup_rute')
  int get idGrupRute;

  @nullable
  @BuiltValueField(wireName: 'origin_latlng')
  String get originLatlng;

  @nullable
  @BuiltValueField(wireName: 'destination_latlng')
  String get destinationLatlng;

  @nullable
  @BuiltValueField(wireName: 'polyline')
  String get polyline;

  @nullable
  @BuiltValueField(wireName: 'waktu_tempuh')
  int get waktuTempuh;

  @nullable
  @BuiltValueField(wireName: 'jarak')
  int get jarak;

  @nullable
  @BuiltValueField(wireName: 'id_grup')
  String get idGrup;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Rute.serializer, this));
  }

  static Rute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Rute.serializer, json.decode(jsonString));
  }

  static Serializer<Rute> get serializer => _$ruteSerializer;
}
