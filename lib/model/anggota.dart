library anggota;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'anggota.g.dart';

abstract class Anggota implements Built<Anggota, AnggotaBuilder> {
  Anggota._();

  factory Anggota([updates(AnggotaBuilder b)]) = _$Anggota;

  @nullable
  @BuiltValueField(wireName: 'id_anggota')
  String get idAnggota;

  @nullable
  @BuiltValueField(wireName: 'nama_lengkap')
  String get namaLengkap;

  @nullable
  @BuiltValueField(wireName: 'jenis_kelamin')
  String get jenisKelamin;

  @nullable
  @BuiltValueField(wireName: 'email')
  String get email;

  @nullable
  @BuiltValueField(wireName: 'no_sim')
  String get noSim;

  @nullable
  @BuiltValueField(wireName: 'no_hp')
  String get noHp;

  @nullable
  @BuiltValueField(wireName: 'tempat_lahir')
  String get tempatLahir;

  @nullable
  @BuiltValueField(wireName: 'tgl_lahir')
  String get tglLahir;

  @nullable
  @BuiltValueField(wireName: 'alamat_lengkap')
  String get alamatLengkap;

  @nullable
  @BuiltValueField(wireName: 'foto')
  String get foto;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(Anggota.serializer, this));
  }

  static Anggota fromJson(String jsonString) {
    return standardSerializers.deserializeWith(Anggota.serializer, json.decode(jsonString));
  }

  static Serializer<Anggota> get serializer => _$anggotaSerializer;
}
