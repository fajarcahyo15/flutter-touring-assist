// GENERATED CODE - DO NOT MODIFY BY HAND

part of grup_rute;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GrupRute> _$grupRuteSerializer = new _$GrupRuteSerializer();

class _$GrupRuteSerializer implements StructuredSerializer<GrupRute> {
  @override
  final Iterable<Type> types = const [GrupRute, _$GrupRute];
  @override
  final String wireName = 'GrupRute';

  @override
  Iterable<Object> serialize(Serializers serializers, GrupRute object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idGrupRute != null) {
      result
        ..add('id_grup_rute')
        ..add(serializers.serialize(object.idGrupRute,
            specifiedType: const FullType(int)));
    }
    if (object.origin != null) {
      result
        ..add('origin_address')
        ..add(serializers.serialize(object.origin,
            specifiedType: const FullType(String)));
    }
    if (object.destination != null) {
      result
        ..add('destination_address')
        ..add(serializers.serialize(object.destination,
            specifiedType: const FullType(String)));
    }
    if (object.polyline != null) {
      result
        ..add('polyline')
        ..add(serializers.serialize(object.polyline,
            specifiedType: const FullType(String)));
    }
    if (object.waktuTempuh != null) {
      result
        ..add('waktu_tempuh')
        ..add(serializers.serialize(object.waktuTempuh,
            specifiedType: const FullType(int)));
    }
    if (object.jarak != null) {
      result
        ..add('jarak')
        ..add(serializers.serialize(object.jarak,
            specifiedType: const FullType(int)));
    }
    if (object.assistRute != null) {
      result
        ..add('assist_rute')
        ..add(serializers.serialize(object.assistRute,
            specifiedType: const FullType(bool)));
    }
    if (object.idGrup != null) {
      result
        ..add('id_grup')
        ..add(serializers.serialize(object.idGrup,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GrupRute deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GrupRuteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_grup_rute':
          result.idGrupRute = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'origin_address':
          result.origin = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'destination_address':
          result.destination = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'polyline':
          result.polyline = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'waktu_tempuh':
          result.waktuTempuh = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'jarak':
          result.jarak = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'assist_rute':
          result.assistRute = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'id_grup':
          result.idGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GrupRute extends GrupRute {
  @override
  final int idGrupRute;
  @override
  final String origin;
  @override
  final String destination;
  @override
  final String polyline;
  @override
  final int waktuTempuh;
  @override
  final int jarak;
  @override
  final bool assistRute;
  @override
  final String idGrup;

  factory _$GrupRute([void Function(GrupRuteBuilder) updates]) =>
      (new GrupRuteBuilder()..update(updates)).build();

  _$GrupRute._(
      {this.idGrupRute,
      this.origin,
      this.destination,
      this.polyline,
      this.waktuTempuh,
      this.jarak,
      this.assistRute,
      this.idGrup})
      : super._();

  @override
  GrupRute rebuild(void Function(GrupRuteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GrupRuteBuilder toBuilder() => new GrupRuteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GrupRute &&
        idGrupRute == other.idGrupRute &&
        origin == other.origin &&
        destination == other.destination &&
        polyline == other.polyline &&
        waktuTempuh == other.waktuTempuh &&
        jarak == other.jarak &&
        assistRute == other.assistRute &&
        idGrup == other.idGrup;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, idGrupRute.hashCode), origin.hashCode),
                            destination.hashCode),
                        polyline.hashCode),
                    waktuTempuh.hashCode),
                jarak.hashCode),
            assistRute.hashCode),
        idGrup.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GrupRute')
          ..add('idGrupRute', idGrupRute)
          ..add('origin', origin)
          ..add('destination', destination)
          ..add('polyline', polyline)
          ..add('waktuTempuh', waktuTempuh)
          ..add('jarak', jarak)
          ..add('assistRute', assistRute)
          ..add('idGrup', idGrup))
        .toString();
  }
}

class GrupRuteBuilder implements Builder<GrupRute, GrupRuteBuilder> {
  _$GrupRute _$v;

  int _idGrupRute;
  int get idGrupRute => _$this._idGrupRute;
  set idGrupRute(int idGrupRute) => _$this._idGrupRute = idGrupRute;

  String _origin;
  String get origin => _$this._origin;
  set origin(String origin) => _$this._origin = origin;

  String _destination;
  String get destination => _$this._destination;
  set destination(String destination) => _$this._destination = destination;

  String _polyline;
  String get polyline => _$this._polyline;
  set polyline(String polyline) => _$this._polyline = polyline;

  int _waktuTempuh;
  int get waktuTempuh => _$this._waktuTempuh;
  set waktuTempuh(int waktuTempuh) => _$this._waktuTempuh = waktuTempuh;

  int _jarak;
  int get jarak => _$this._jarak;
  set jarak(int jarak) => _$this._jarak = jarak;

  bool _assistRute;
  bool get assistRute => _$this._assistRute;
  set assistRute(bool assistRute) => _$this._assistRute = assistRute;

  String _idGrup;
  String get idGrup => _$this._idGrup;
  set idGrup(String idGrup) => _$this._idGrup = idGrup;

  GrupRuteBuilder();

  GrupRuteBuilder get _$this {
    if (_$v != null) {
      _idGrupRute = _$v.idGrupRute;
      _origin = _$v.origin;
      _destination = _$v.destination;
      _polyline = _$v.polyline;
      _waktuTempuh = _$v.waktuTempuh;
      _jarak = _$v.jarak;
      _assistRute = _$v.assistRute;
      _idGrup = _$v.idGrup;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GrupRute other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GrupRute;
  }

  @override
  void update(void Function(GrupRuteBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GrupRute build() {
    final _$result = _$v ??
        new _$GrupRute._(
            idGrupRute: idGrupRute,
            origin: origin,
            destination: destination,
            polyline: polyline,
            waktuTempuh: waktuTempuh,
            jarak: jarak,
            assistRute: assistRute,
            idGrup: idGrup);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
