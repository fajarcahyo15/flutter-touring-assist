library grup_rute;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'grup_rute.g.dart';

abstract class GrupRute implements Built<GrupRute, GrupRuteBuilder> {
  GrupRute._();

  factory GrupRute([updates(GrupRuteBuilder b)]) = _$GrupRute;

  @nullable
  @BuiltValueField(wireName: 'id_grup_rute')
  int get idGrupRute;

  @nullable
  @BuiltValueField(wireName: 'origin_address')
  String get origin;

  @nullable
  @BuiltValueField(wireName: 'destination_address')
  String get destination;

  @nullable
  @BuiltValueField(wireName: 'polyline')
  String get polyline;

  @nullable
  @BuiltValueField(wireName: 'waktu_tempuh')
  int get waktuTempuh;

  @nullable
  @BuiltValueField(wireName: 'jarak')
  int get jarak;

  @nullable
  @BuiltValueField(wireName: 'assist_rute')
  bool get assistRute;

  @nullable
  @BuiltValueField(wireName: 'id_grup')
  String get idGrup;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(GrupRute.serializer, this));
  }

  static GrupRute fromJson(String jsonString) {
    return standardSerializers.deserializeWith(GrupRute.serializer, json.decode(jsonString));
  }

  static Serializer<GrupRute> get serializer => _$grupRuteSerializer;
}
