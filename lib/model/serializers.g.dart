// GENERATED CODE - DO NOT MODIFY BY HAND

part of serializers;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(Anggota.serializer)
      ..add(AnggotaGrup.serializer)
      ..add(Bounds.serializer)
      ..add(CityOwm.serializer)
      ..add(CloudsOwm.serializer)
      ..add(Coord.serializer)
      ..add(DataBadWeather.serializer)
      ..add(DataBuatGrup.serializer)
      ..add(DataDetailAnggota.serializer)
      ..add(DataDetailGrup.serializer)
      ..add(DataDetailGrupMember.serializer)
      ..add(DataDetailGrupRute.serializer)
      ..add(DataHapusGrup.serializer)
      ..add(DataLastGrupRute.serializer)
      ..add(DataListAnggotaGrup.serializer)
      ..add(DataListGrup.serializer)
      ..add(DataListGrupMember.serializer)
      ..add(DataListGrupRute.serializer)
      ..add(Direction.serializer)
      ..add(Distance.serializer)
      ..add(Forecast.serializer)
      ..add(GeocodedWaypoint.serializer)
      ..add(Grup.serializer)
      ..add(GrupMember.serializer)
      ..add(GrupRute.serializer)
      ..add(LatLng.serializer)
      ..add(Leg.serializer)
      ..add(MainWeatherInfo.serializer)
      ..add(Polyline.serializer)
      ..add(ResBadWeather.serializer)
      ..add(ResBase.serializer)
      ..add(ResBuatGrup.serializer)
      ..add(ResBuatGrupRute.serializer)
      ..add(ResDetailAnggota.serializer)
      ..add(ResDetailGrup.serializer)
      ..add(ResDetailGrupMember.serializer)
      ..add(ResDetailGrupRute.serializer)
      ..add(ResEditGrup.serializer)
      ..add(ResHapusGrup.serializer)
      ..add(ResLastGrupRute.serializer)
      ..add(ResListAnggotaGrup.serializer)
      ..add(ResListGrup.serializer)
      ..add(ResListGrupMember.serializer)
      ..add(ResListGrupRute.serializer)
      ..add(Route.serializer)
      ..add(Rute.serializer)
      ..add(Step.serializer)
      ..add(SysOwm.serializer)
      ..add(Weather.serializer)
      ..add(WeatherForecastOwm.serializer)
      ..add(WindOwm.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(AnggotaGrup)]),
          () => new ListBuilder<AnggotaGrup>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(DataListGrupMember)]),
          () => new ListBuilder<DataListGrupMember>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(GeocodedWaypoint)]),
          () => new ListBuilder<GeocodedWaypoint>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Route)]),
          () => new ListBuilder<Route>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Grup)]),
          () => new ListBuilder<Grup>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(GrupRute)]),
          () => new ListBuilder<GrupRute>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Leg)]),
          () => new ListBuilder<Leg>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Step)]),
          () => new ListBuilder<Step>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Weather)]),
          () => new ListBuilder<Weather>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Weather)]),
          () => new ListBuilder<Weather>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(WeatherForecastOwm)]),
          () => new ListBuilder<WeatherForecastOwm>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
