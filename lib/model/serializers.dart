library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:new_touring_assist_flutter/model/anggota.dart';
import 'package:new_touring_assist_flutter/model/anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/grup.dart';
import 'package:new_touring_assist_flutter/model/grup_member.dart';
import 'package:new_touring_assist_flutter/model/grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/anggota/data_detail_anggota.dart';
import 'package:new_touring_assist_flutter/model/response/anggota/res_detail_anggota.dart';
import 'package:new_touring_assist_flutter/model/response/anggota_grup/data_list_anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/response/anggota_grup/res_list_anggota_grup.dart';
import 'package:new_touring_assist_flutter/model/response/direction/bounds.dart';
import 'package:new_touring_assist_flutter/model/response/direction/direction.dart';
import 'package:new_touring_assist_flutter/model/response/direction/distance.dart';
import 'package:new_touring_assist_flutter/model/response/direction/geocoded_waypoint.dart';
import 'package:new_touring_assist_flutter/model/response/direction/lat_lng.dart';
import 'package:new_touring_assist_flutter/model/response/direction/leg.dart';
import 'package:new_touring_assist_flutter/model/response/direction/polyline.dart';
import 'package:new_touring_assist_flutter/model/response/direction/route.dart';
import 'package:new_touring_assist_flutter/model/response/direction/step.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/city_owm.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/clouds_owm.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/coord.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/forecast.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/main_weather_info.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/sys_owm.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/weather.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/weather_forecast_owm.dart';
import 'package:new_touring_assist_flutter/model/response/forecast/wind_owm.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_buat_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_detail_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_hapus_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/data_list_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_buat_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_detail_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_edit_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_hapus_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup/res_list_grup.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/data_detail_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/data_list_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/res_detail_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_member/res_list_grup_member.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/data_detail_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/data_last_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/data_list_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_buat_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_detail_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_last_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/grup_rute/res_list_grup_rute.dart';
import 'package:new_touring_assist_flutter/model/response/res_base.dart';
import 'package:new_touring_assist_flutter/model/response/weather/data_bad_weather.dart';
import 'package:new_touring_assist_flutter/model/response/weather/res_bad_weather.dart';
import 'package:new_touring_assist_flutter/model/rute.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  Bounds,
  Direction,
  Distance,
  GeocodedWaypoint,
  LatLng,
  Leg,
  Polyline,
  Route,
  Step,
  CityOwm,
  CloudsOwm,
  Coord,
  Forecast,
  MainWeatherInfo,
  SysOwm,
  WeatherForecastOwm,
  Weather,
  WindOwm,
  DataDetailAnggota,
  ResDetailAnggota,
  DataListAnggotaGrup,
  ResListAnggotaGrup,
  DataBuatGrup,
  DataDetailGrup,
  DataHapusGrup,
  DataListGrup,
  ResBuatGrup,
  ResDetailGrup,
  ResEditGrup,
  ResHapusGrup,
  ResListGrup,
  DataDetailGrupMember,
  DataListGrupMember,
  ResDetailGrupMember,
  ResListGrupMember,
  DataDetailGrupRute,
  DataLastGrupRute,
  DataListGrupRute,
  ResBuatGrupRute,
  ResDetailGrupRute,
  ResLastGrupRute,
  ResListGrupRute,
  DataBadWeather,
  ResBadWeather,
  ResBase,
  AnggotaGrup,
  Anggota,
  GrupMember,
  GrupRute,
  Grup,
  Rute,
])
Serializers serializers = _$serializers;

Serializers standardSerializers = (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
