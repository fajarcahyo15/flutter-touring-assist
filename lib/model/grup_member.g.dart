// GENERATED CODE - DO NOT MODIFY BY HAND

part of grup_member;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<GrupMember> _$grupMemberSerializer = new _$GrupMemberSerializer();

class _$GrupMemberSerializer implements StructuredSerializer<GrupMember> {
  @override
  final Iterable<Type> types = const [GrupMember, _$GrupMember];
  @override
  final String wireName = 'GrupMember';

  @override
  Iterable<Object> serialize(Serializers serializers, GrupMember object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idGrupMember != null) {
      result
        ..add('id_grup_member')
        ..add(serializers.serialize(object.idGrupMember,
            specifiedType: const FullType(String)));
    }
    if (object.latitude != null) {
      result
        ..add('latitude')
        ..add(serializers.serialize(object.latitude,
            specifiedType: const FullType(String)));
    }
    if (object.longitude != null) {
      result
        ..add('longitude')
        ..add(serializers.serialize(object.longitude,
            specifiedType: const FullType(String)));
    }
    if (object.assistMember != null) {
      result
        ..add('assist_member')
        ..add(serializers.serialize(object.assistMember,
            specifiedType: const FullType(bool)));
    }
    if (object.idGrup != null) {
      result
        ..add('id_grup')
        ..add(serializers.serialize(object.idGrup,
            specifiedType: const FullType(String)));
    }
    if (object.idAnggota != null) {
      result
        ..add('id_anggota')
        ..add(serializers.serialize(object.idAnggota,
            specifiedType: const FullType(String)));
    }
    if (object.kodeLevelGrup != null) {
      result
        ..add('kode_level_grup')
        ..add(serializers.serialize(object.kodeLevelGrup,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  GrupMember deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new GrupMemberBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id_grup_member':
          result.idGrupMember = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'assist_member':
          result.assistMember = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'id_grup':
          result.idGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id_anggota':
          result.idAnggota = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'kode_level_grup':
          result.kodeLevelGrup = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$GrupMember extends GrupMember {
  @override
  final String idGrupMember;
  @override
  final String latitude;
  @override
  final String longitude;
  @override
  final bool assistMember;
  @override
  final String idGrup;
  @override
  final String idAnggota;
  @override
  final String kodeLevelGrup;

  factory _$GrupMember([void Function(GrupMemberBuilder) updates]) =>
      (new GrupMemberBuilder()..update(updates)).build();

  _$GrupMember._(
      {this.idGrupMember,
      this.latitude,
      this.longitude,
      this.assistMember,
      this.idGrup,
      this.idAnggota,
      this.kodeLevelGrup})
      : super._();

  @override
  GrupMember rebuild(void Function(GrupMemberBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GrupMemberBuilder toBuilder() => new GrupMemberBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GrupMember &&
        idGrupMember == other.idGrupMember &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        assistMember == other.assistMember &&
        idGrup == other.idGrup &&
        idAnggota == other.idAnggota &&
        kodeLevelGrup == other.kodeLevelGrup;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, idGrupMember.hashCode), latitude.hashCode),
                        longitude.hashCode),
                    assistMember.hashCode),
                idGrup.hashCode),
            idAnggota.hashCode),
        kodeLevelGrup.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GrupMember')
          ..add('idGrupMember', idGrupMember)
          ..add('latitude', latitude)
          ..add('longitude', longitude)
          ..add('assistMember', assistMember)
          ..add('idGrup', idGrup)
          ..add('idAnggota', idAnggota)
          ..add('kodeLevelGrup', kodeLevelGrup))
        .toString();
  }
}

class GrupMemberBuilder implements Builder<GrupMember, GrupMemberBuilder> {
  _$GrupMember _$v;

  String _idGrupMember;
  String get idGrupMember => _$this._idGrupMember;
  set idGrupMember(String idGrupMember) => _$this._idGrupMember = idGrupMember;

  String _latitude;
  String get latitude => _$this._latitude;
  set latitude(String latitude) => _$this._latitude = latitude;

  String _longitude;
  String get longitude => _$this._longitude;
  set longitude(String longitude) => _$this._longitude = longitude;

  bool _assistMember;
  bool get assistMember => _$this._assistMember;
  set assistMember(bool assistMember) => _$this._assistMember = assistMember;

  String _idGrup;
  String get idGrup => _$this._idGrup;
  set idGrup(String idGrup) => _$this._idGrup = idGrup;

  String _idAnggota;
  String get idAnggota => _$this._idAnggota;
  set idAnggota(String idAnggota) => _$this._idAnggota = idAnggota;

  String _kodeLevelGrup;
  String get kodeLevelGrup => _$this._kodeLevelGrup;
  set kodeLevelGrup(String kodeLevelGrup) =>
      _$this._kodeLevelGrup = kodeLevelGrup;

  GrupMemberBuilder();

  GrupMemberBuilder get _$this {
    if (_$v != null) {
      _idGrupMember = _$v.idGrupMember;
      _latitude = _$v.latitude;
      _longitude = _$v.longitude;
      _assistMember = _$v.assistMember;
      _idGrup = _$v.idGrup;
      _idAnggota = _$v.idAnggota;
      _kodeLevelGrup = _$v.kodeLevelGrup;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GrupMember other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GrupMember;
  }

  @override
  void update(void Function(GrupMemberBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GrupMember build() {
    final _$result = _$v ??
        new _$GrupMember._(
            idGrupMember: idGrupMember,
            latitude: latitude,
            longitude: longitude,
            assistMember: assistMember,
            idGrup: idGrup,
            idAnggota: idAnggota,
            kodeLevelGrup: kodeLevelGrup);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
