library grup_member;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:new_touring_assist_flutter/model/serializers.dart';

part 'grup_member.g.dart';

abstract class GrupMember implements Built<GrupMember, GrupMemberBuilder> {
  GrupMember._();

  factory GrupMember([updates(GrupMemberBuilder b)]) = _$GrupMember;

  @nullable
  @BuiltValueField(wireName: 'id_grup_member')
  String get idGrupMember;

  @nullable
  @BuiltValueField(wireName: 'latitude')
  String get latitude;

  @nullable
  @BuiltValueField(wireName: 'longitude')
  String get longitude;

  @nullable
  @BuiltValueField(wireName: 'assist_member')
  bool get assistMember;

  @nullable
  @BuiltValueField(wireName: 'id_grup')
  String get idGrup;

  @nullable
  @BuiltValueField(wireName: 'id_anggota')
  String get idAnggota;

  @nullable
  @BuiltValueField(wireName: 'kode_level_grup')
  String get kodeLevelGrup;

  String toJson() {
    return json.encode(standardSerializers.serializeWith(GrupMember.serializer, this));
  }

  static GrupMember fromJson(String jsonString) {
    return standardSerializers.deserializeWith(GrupMember.serializer, json.decode(jsonString));
  }

  static Serializer<GrupMember> get serializer => _$grupMemberSerializer;
}
