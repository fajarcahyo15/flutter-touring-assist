import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:new_touring_assist_flutter/page/login_page.dart';
import 'package:new_touring_assist_flutter/provider/asisten_perjalanan_provider.dart';
import 'package:new_touring_assist_flutter/provider/buat_grup_provider.dart';
import 'package:new_touring_assist_flutter/provider/daftar_provider.dart';
import 'package:new_touring_assist_flutter/provider/fase_perjalanan_aktif_provider.dart';
import 'package:new_touring_assist_flutter/provider/fase_perjalanan_provider.dart';
import 'package:new_touring_assist_flutter/provider/form_fase_perjalanan/lihat_fase_perjalanan_provider.dart';
import 'package:new_touring_assist_flutter/provider/grup_anggota_provider.dart';
import 'package:new_touring_assist_flutter/provider/grup_tambah_anggota_provider.dart';
import 'package:new_touring_assist_flutter/provider/login_provider.dart';
import 'package:new_touring_assist_flutter/provider/lupa_password_provider.dart';
import 'package:new_touring_assist_flutter/provider/main_provider.dart';
import 'package:new_touring_assist_flutter/provider/map_fase_perjalanan_provider.dart';
import 'package:new_touring_assist_flutter/provider/profil_provider.dart';
import 'package:new_touring_assist_flutter/provider/settings_grup_provider.dart';
import 'package:new_touring_assist_flutter/provider/form_fase_perjalanan/tambah_fase_perjalanan_provider.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginProvider>.value(
          value: LoginProvider(),
        ),
        ChangeNotifierProvider<DaftarProvider>.value(
          value: DaftarProvider(),
        ),
        ChangeNotifierProvider<LupaPasswordProvider>.value(
          value: LupaPasswordProvider(),
        ),
        ChangeNotifierProvider<MainProvider>.value(
          value: MainProvider(),
        ),
        ChangeNotifierProvider<ProfilProvider>.value(
          value: ProfilProvider(),
        ),
        ChangeNotifierProvider<BuatGrupProvider>.value(
          value: BuatGrupProvider(),
        ),
        ChangeNotifierProvider<AsistenPerjalananProvider>.value(
          value: AsistenPerjalananProvider(),
        ),
        ChangeNotifierProvider<FasePerjalananAktifProvider>.value(
          value: FasePerjalananAktifProvider(),
        ),
        ChangeNotifierProvider<FasePerjalananProvider>.value(
          value: FasePerjalananProvider(),
        ),
        ChangeNotifierProvider<MapFasePerjalananProvider>.value(
          value: MapFasePerjalananProvider(),
        ),
        ChangeNotifierProvider<TambahFasePerjalananProvider>.value(
          value: TambahFasePerjalananProvider(),
        ),
        ChangeNotifierProvider<LihatFasePerjalananProvider>.value(
          value: LihatFasePerjalananProvider(),
        ),
        ChangeNotifierProvider<SettingsGrupProvider>.value(
          value: SettingsGrupProvider(),
        ),
        ChangeNotifierProvider<GrupAnggotaProvider>.value(
          value: GrupAnggotaProvider(),
        ),
        ChangeNotifierProvider<GrupTambahAnggotaProvider>.value(
          value: GrupTambahAnggotaProvider(),
        ),
      ],
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'),
          const Locale('id'),
        ],
        title: "Bikers Touring Assist",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.indigo,
          fontFamily: "Ubuntu-Condensed",
          textTheme: TextTheme(
            title: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
            subtitle: TextStyle(
              color: Colors.grey,
            ),
            button: TextStyle(
              color: Colors.white,
            ),
          ),
          cardTheme: CardTheme(
            elevation: 3,
          ),
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          primaryIconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
        builder: (context, child) => MediaQuery(
          data: MediaQuery.of(context).copyWith(
            alwaysUse24HourFormat: true,
          ),
          child: child,
        ),
        home: LoginPage(),
      ),
    );
  }
}
