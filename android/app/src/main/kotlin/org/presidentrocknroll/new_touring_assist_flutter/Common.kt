package org.presidentrocknroll.new_touring_assist_flutter

import android.content.Context
import android.location.Location
import android.preference.PreferenceManager


class Common {
    companion object {
        val KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates"

        fun requestingLocationUpdates(context: Context?): Boolean {
            return PreferenceManager.getDefaultSharedPreferences(context)
                    .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false)
        }

        fun setRequestingLocationUpdates(context: Context?, requestingLocationUpdates: Boolean) {
            PreferenceManager.getDefaultSharedPreferences(context)
                    .edit()
                    .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                    .apply()
        }

        fun getLocationText(location: Location?): String? {
            return if (location == null) {
                "Gagal mendapatkan lokasi"
            } else {
                "Lokasi saat ini ${location.latitude},${location.longitude}"
            }
        }
    }
}