package org.presidentrocknroll.new_touring_assist_flutter

import android.app.*
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import org.greenrobot.eventbus.EventBus

class MyService : Service() {

    private val TAG = MyService::class.java.simpleName

    companion object {
        private val PACKAGE_NAME = "org.presidentrocknroll.new_touring_assist_flutter"
        private val CHANNEL_ID = "channel_location_service"
        private val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.started_from_notification"
        private val NOTIFICATION_ID = 1001
    }

    private val mBinder = LocalBinder()
    private var mNotificationManager: NotificationManager? = null
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private var mLocationCallback: LocationCallback? = null
    private var mServiceHandler: Handler? = null
    private var mLocation: Location? = null
    private var mLocationRequest: LocationRequest? = null

    private val notification: Notification
        get() {
            val intent = Intent(this, MyService::class.java)
            val text = Common.getLocationText(mLocation)
            intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true)

            val builder = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                    .setContentText(text)
                    .setContentTitle("Asisten Perjalanan Aktif")

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(CHANNEL_ID)
            }

            return builder.build()
        }

    inner class LocalBinder : Binder() {
        internal val service: MyService
            get() = this@MyService
    }

    override fun onCreate() {
        super.onCreate()

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(location: LocationResult?) {
                super.onLocationResult(location)
                onNewLocation(location!!.lastLocation)
            }
        }

        val handlerThread = HandlerThread(TAG)
        handlerThread.start()
        mServiceHandler = Handler(handlerThread.looper)
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = packageName
            val mChannel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)
            mNotificationManager!!.createNotificationChannel(mChannel)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val startedFromNotification = intent!!.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION, false)
        if (startedFromNotification) {
            removeLocationUpdate()
            stopSelf()
        }

        return START_NOT_STICKY
    }

    fun removeLocationUpdate() {
        try {
            mFusedLocationProviderClient!!.removeLocationUpdates(mLocationCallback)
            Common.setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (e: SecurityException) {
            Common.setRequestingLocationUpdates(this, true)
            Log.e(TAG, "Tidak memiliki izin mendapatkan lokasi: ${e.message}")
        }

        stopForeground(true)
    }

    fun getLastLocation() {
        try {
            mFusedLocationProviderClient!!.lastLocation.addOnCompleteListener { task ->
                if (task.isSuccessful && task.result != null) {
                    mLocation = task.result
                } else {
                    Log.e(TAG, "Gagal mendapatkan lokasi")
                }
            }
        } catch (e: SecurityException) {
            Log.e(TAG, "SecurityLocationError: ${e.message}")
        }
    }

    fun createLocationRequest(updateInterval: Long, fastedUpdateInterval: Long) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = updateInterval
        mLocationRequest!!.fastestInterval = fastedUpdateInterval
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    fun onNewLocation(location: Location?) {
        mLocation = location
        EventBus.getDefault().postSticky(BackgroundLocation(mLocation!!))

        if (serviceIsRunningInBackground(this)) {
            mNotificationManager!!.notify(NOTIFICATION_ID, notification)
        }
    }


    fun serviceIsRunningInBackground(context: Context): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (javaClass.name.equals(service.service.className)) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }

    override fun onBind(p0: Intent?): IBinder? {
        return mBinder
    }

    override fun onDestroy() {
        super.onDestroy()
        mServiceHandler!!.removeCallbacksAndMessages(null)
    }

    fun requestLocationUpdates(interval: Long) {
        Common.setRequestingLocationUpdates(this, true)

        createLocationRequest(interval, interval / 2)
        getLastLocation()

        val intent = Intent(applicationContext, MyService::class.java)
        startService(intent)

        try {
            mFusedLocationProviderClient!!.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())
        } catch (e: SecurityException) {
            Common.setRequestingLocationUpdates(this, false)
            Log.e(TAG, "Tidak memiliki izin mendapatkan lokasi: ${e.message}")
        }

        startForeground(NOTIFICATION_ID, notification)
    }
}