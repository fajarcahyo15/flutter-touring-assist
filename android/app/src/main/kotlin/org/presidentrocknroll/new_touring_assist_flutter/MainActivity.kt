package org.presidentrocknroll.new_touring_assist_flutter

import android.content.*
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.gsonparserfactory.GsonParserFactory
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.gson.GsonBuilder

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject

class MainActivity : FlutterActivity() {

    private var myService: MyService? = null
    private var mBound = false

    private var token: String? = null
    private var idGrup: String? = null
    private var idGrupRute: Int? = null

    var interval: Long = 10000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        val gson = GsonBuilder().setLenient().create()
        AndroidNetworking.initialize(getApplicationContext())
        AndroidNetworking.setParserFactory(GsonParserFactory(gson))

        val mServiceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(p0: ComponentName?) {
                myService = null
                mBound = false
            }

            override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
                val binder = p1 as MyService.LocalBinder
                myService = binder.service
                mBound = true
            }
        }

        val intentService = Intent(this@MainActivity, MyService::class.java)
        bindService(intentService, mServiceConnection, Context.BIND_AUTO_CREATE)

        MethodChannel(flutterView, "org.presidentrocknroll.new_touring_assist_flutter.assist")
                .setMethodCallHandler(object : MethodChannel.MethodCallHandler {
                    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {

                        if (call.method.equals("startService")) {
                            token = call.argument<String>("token")
                            idGrup = call.argument<String>("id_grup")
                            idGrupRute = call.argument<Int>("id_grup_rute")

                            try {
                                val strInterval = call.argument<String>("interval")
                                if (strInterval != null) {
                                    interval = strInterval.toLong() * 1000
                                }

                                EventBus.getDefault().register(this@MainActivity)
                                myService!!.requestLocationUpdates(interval)

                                Toast.makeText(this@MainActivity, "Asisten perjalanan telah diaktifkan", Toast.LENGTH_SHORT).show()
                                result.success("Asisten perjalanan telah diaktifkan")
                            } catch (e: Exception) {
                                Toast.makeText(this@MainActivity, e.toString(), Toast.LENGTH_LONG).show()
                                result.success("Gagal mengaktifkan asisten perjalanan")
                            }


                        } else if (call.method.equals("stopService")) {

                            EventBus.getDefault().unregister(this@MainActivity)
                            myService!!.removeLocationUpdate()

                            Toast.makeText(this@MainActivity, "Asisten perjalanan telah dinonaktifkan", Toast.LENGTH_SHORT).show()
                            result.success("Asisten perjalanan telah dinonaktifkan")
                        } else {

                            result.success("method unknown")
                        }
                    }
                })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onBackgroundLocationRetrieve(event: BackgroundLocation) {
        sendLocationToWebService(event.location)
    }

    fun sendLocationToWebService(location: Location) {
        // val BASE_URL = "https://api.presidentrocknroll.com"
        val BASE_URL = "http://10.0.2.2/api-touring/"

        AndroidNetworking.post("$BASE_URL/grup_member/updateposition")
                .addHeaders("Authorization", token)
                .addBodyParameter("latitude", location.latitude.toString())
                .addBodyParameter("longitude", location.longitude.toString())
                .addBodyParameter("id_grup_rute", idGrupRute.toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject?) {
                        Log.d("MyService", response.toString())
                    }

                    override fun onError(anError: ANError?) {
                        anError?.printStackTrace()
                        Toast.makeText(this@MainActivity, "Touring Assist:\nTerjadi kesalahan dalam mengirim lokasi", Toast.LENGTH_SHORT).show()
                    }

                })

    }
}
